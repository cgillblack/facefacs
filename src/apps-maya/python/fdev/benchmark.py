import os
import time
import datetime
from face import io
import fdev.utils.model as mod
import face.core as fcore
reload(io)
reload(fcore)
reload(mod)

def convert_bytes(num):
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def file_size(file_path):
    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return convert_bytes(file_info.st_size)

shape_count = 1
directory = "C:/face/tmp/"
file_name = 'format_bench'

time1 = time.time()

network = fcore.Network()
network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for i in range(shape_count):
    shape1 = network.add_shape('test{}'.format(i))
    shape1.get_vtx_array_from_maya_node('body_geo', method=1)
    
time2 = time.time()

filepath = '{}/{}.p'.format(directory, file_name)

io.save_pickle(filepath, network)

time3 = time.time()

new_network = io.open_network(filepath)

time4 = time.time()

new_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)
for name, shape in new_network.shapes.iteritems():
    shape.apply_vtx_array_to_maya_node('body_geo')

time5 = time.time()

print 'Pickle results:'
print '\tShape count:', shape_count
print '\tPoly count:', cmds.polyEvaluate('body_geo', f=True)
print '\tAdding shapes:', str(datetime.timedelta(seconds=time2-time1))
print '\tSaving file:', str(datetime.timedelta(seconds=time3-time2))
print '\tOpening file:', str(datetime.timedelta(seconds=time4-time3))
print '\tApplying shapes:', str(datetime.timedelta(seconds=time5-time4))
print '\tFile size:', file_size(filepath)
print '\n'

time1 = time.time()

network = fcore.Network()
network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for i in range(shape_count):
    shape1 = network.add_shape('test{}'.format(i))
    shape1.get_vtx_array_from_maya_node('body_geo', method=1)
    
time2 = time.time()

filepath = '{}/{}.json'.format(directory, file_name)

io.save_json(filepath, network)

time3 = time.time()

new_network = io.open_network(filepath)

time4 = time.time()

new_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)
for name, shape in new_network.shapes.iteritems():
    shape.apply_vtx_array_to_maya_node('body_geo')

time5 = time.time()

print 'Json results:'
print '\tShape count:', shape_count
print '\tPoly count:', cmds.polyEvaluate('body_geo', f=True)
print '\tAdding shapes:', str(datetime.timedelta(seconds=time2-time1))
print '\tSaving file:', str(datetime.timedelta(seconds=time3-time2))
print '\tOpening file:', str(datetime.timedelta(seconds=time4-time3))
print '\tApplying shapes:', str(datetime.timedelta(seconds=time5-time4))
print '\tFile size:', file_size(filepath)
print '\n'



time1 = time.time()

network = fcore.Network()
network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for i in range(shape_count):
    shape1 = network.add_shape('test{}'.format(i))
    shape1.get_vtx_array_from_maya_node('body_geo', method=1)
    
time2 = time.time()

filepath = '{}/{}.msgpack'.format(directory, file_name)

io.save_msgpack(filepath, network)

time3 = time.time()

new_network = io.open_network(filepath)

time4 = time.time()

new_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)
for name, shape in new_network.shapes.iteritems():
    shape.apply_vtx_array_to_maya_node('body_geo')

time5 = time.time()

print 'MessagePack results:'
print '\tShape count:', shape_count
print '\tPoly count:', cmds.polyEvaluate('body_geo', f=True)
print '\tAdding shapes:', str(datetime.timedelta(seconds=time2-time1))
print '\tSaving file:', str(datetime.timedelta(seconds=time3-time2))
print '\tOpening file:', str(datetime.timedelta(seconds=time4-time3))
print '\tApplying shapes:', str(datetime.timedelta(seconds=time5-time4))
print '\tFile size:', file_size(filepath)
print '\n'






















######################################################################################













import os
import time
import datetime
from face import io
import fdev.utils.model as mod
import face.core as fcore
reload(io)
reload(fcore)
reload(mod)

def convert_bytes(num):
    for x in ['bytes', 'KB', 'MB', 'GB', 'TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0


def file_size(file_path):
    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return convert_bytes(file_info.st_size)

shape_count = 1000
directory = "C:/face/tmp/"
file_name = 'format_bench'

time1 = time.time()

network = fcore.Network()
network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for i in range(shape_count):
    shape1 = network.add_shape('lipStretcher{}'.format(i))
    shape1.get_vtx_array_from_maya_node('lipStretcher', method=0)
    
"""
eyeClose = network.add_shape('eyeClose')
eyeClose.get_vtx_array_from_maya_node('eyeClose', method=0)
lipStretcher = network.add_shape('lipStretcher')
lipStretcher.get_vtx_array_from_maya_node('lipStretcher', method=0)
"""

time2 = time.time()

filepath = '{}/{}.p'.format(directory, file_name)

io.save_pickle(filepath, network, compress=True)

time3 = time.time()

new_network = io.open_network(filepath)

time4 = time.time()

new_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)


lMap = new_network.add_map('lHard')
lMap.map_array = proc.generate_hard_split_map(new_network.neutral_shape.vtx_array, 1)
rMap = new_network.add_map('rHard')
rMap.map_array = proc.generate_hard_split_map(new_network.neutral_shape.vtx_array, 0)

split_network = fcore.Network()
split_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for name, shape in new_network.shapes.iteritems():
    if name == 'neutral':
        continue
        
    lShape = split_network.add_shape('l{}{}'.format(name[0].upper(),name[1:]))
    lShape.vtx_array = proc.multiply_map_arrays(shape.vtx_array, lMap.map_array)

    rShape = split_network.add_shape('r{}{}'.format(name[0].upper(),name[1:]))
    rShape.vtx_array = proc.multiply_map_arrays(shape.vtx_array, rMap.map_array)
    
        
time5 = time.time()
for name, shape in split_network.shapes.iteritems():
    shape.apply_vtx_array_to_maya_node('result')

time6 = time.time()


print 'Pickle results:'
print '\tShape count:', shape_count
print '\tSplit shape count:', len(split_network.shapes.keys())
print '\tPoly count:', cmds.polyEvaluate('body_geo', f=True)
print '\tAdding shapes:', str(datetime.timedelta(seconds=time2-time1))
print '\tSaving file:', str(datetime.timedelta(seconds=time3-time2))
print '\tOpening file:', str(datetime.timedelta(seconds=time4-time3))
print '\tSplitting shapes:', str(datetime.timedelta(seconds=time5-time4))
print '\tApplying shapes:', str(datetime.timedelta(seconds=time6-time5))
print '\tFile size:', file_size(filepath)
print '\n'





"""
time1 = time.time()

network = fcore.Network()
network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for i in range(shape_count):
    shape1 = network.add_shape('test{}'.format(i))
    shape1.get_vtx_array_from_maya_node('eyeClose', method=0)

time2 = time.time()

filepath = '{}/{}.json'.format(directory, file_name)

io.save_json(filepath, network, compress=True)

time3 = time.time()

new_network = io.open_network(filepath)

time4 = time.time()
'''
new_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)
for name, shape in new_network.shapes.iteritems():
    shape.apply_vtx_array_to_maya_node('body_geo')
'''
time5 = time.time()

print 'Json results:'
print '\tShape count:', shape_count
print '\tPoly count:', cmds.polyEvaluate('body_geo', f=True)
print '\tAdding shapes:', str(datetime.timedelta(seconds=time2-time1))
print '\tSaving file:', str(datetime.timedelta(seconds=time3-time2))
print '\tOpening file:', str(datetime.timedelta(seconds=time4-time3))
print '\tApplying shapes:', str(datetime.timedelta(seconds=time5-time4))
print '\tFile size:', file_size(filepath)
print '\n'



time1 = time.time()

network = fcore.Network()
network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)

for i in range(shape_count):
    shape1 = network.add_shape('test{}'.format(i))
    shape1.get_vtx_array_from_maya_node('body_geo', method=0)
    
time2 = time.time()

filepath = '{}/{}.msgpack'.format(directory, file_name)

io.save_msgpack(filepath, network, compress=True)

time3 = time.time()

new_network = io.open_network(filepath)

time4 = time.time()
'''
new_network.neutral_shape.get_vtx_array_from_maya_node('body_geo', subtract_neutral=False)
for name, shape in new_network.shapes.iteritems():
    shape.apply_vtx_array_to_maya_node('body_geo')
'''
time5 = time.time()

print 'MessagePack results:'
print '\tShape count:', shape_count
print '\tPoly count:', cmds.polyEvaluate('body_geo', f=True)
print '\tAdding shapes:', str(datetime.timedelta(seconds=time2-time1))
print '\tSaving file:', str(datetime.timedelta(seconds=time3-time2))
print '\tOpening file:', str(datetime.timedelta(seconds=time4-time3))
print '\tApplying shapes:', str(datetime.timedelta(seconds=time5-time4))
print '\tFile size:', file_size(filepath)
print '\n'
"""

