"""Module docstring.

:author: Chris Gill

"""
import logging

from maya import cmds
import maya.api.OpenMaya as om

import numpy as np

LOG = logging.getLogger(__name__)
LOG.setLevel(40)


def get_map_array(node, attr):
    """Get a numpy array of the attr value of every vertex in a mesh.

    Notes:
    
        selectionList = om.MSelectionList()
        selectionList.add("testShape")
        nodeDagPath = selectionList.getDagPath(0)
        
        test = selectionList.getDependNode(0)
        
        om.MFnDependencyNode(test).findPlug("split", 0).asFloat()

    Args:
        node (str): Provided node for which to extract vertex attr values.
        attr (str): Name of attr to get.

    Returns:
        npArray: Numpy array of the per vertex attr values.
    """
    
    
    map_array = np.array(cmds.getAttr("{}.{}".format(node, attr)))
    
    return map_array
    

def create_paintable_attribute(node, attr):
    """Create an attribute that can be painted, for use in split painting.

    Args:
        node (str): Provided node for which to extract vertex attr values.
        attr (str): Name of the attribute to be added.

    """
    
    if not cmds.objectType(node) == "mesh":
        LOG.warning("%s is not of type 'mesh' searching for mesh child.", node)
        node_shape = cmds.listRelatives(node, type="mesh")

        if node_shape:
            node = node_shape[0]
            LOG.info("\tMesh child found, using %r.", node)
        else:
            LOG.info("Mesh child cannot be found for %r.", node)
            return
        
        
    cmds.addAttr(node, ln=attr, dt="doubleArray")
    cmds.makePaintable("mesh", "{}.{}".format(node, attr), attrType="doubleArray")




