"""Module containing functions for building/managing a blendshape node in maya.
 
:author: Chris Gill

TO DO:
Maybe write better blendshape construction function.

"""

import numpy as np

from maya import cmds

import fdev.utils.model as model
reload(model)


def create_blendshape_from_network(network, geometry, name=None, method=0):
    """Create a blendshape node ona geometry and add shapes from provided shape network.

    Args:
        network (obj): Network containing shapes to add to the blendshape target list.
        geometry (str): Name of geometry to which to apply blendShape deformer.
        name (str): Name to give blendShape deformer.
        method (int): Method to use to build blendshape deformer, cmds/api.

    Returns:
        str: Name of create blendshape node.
    """

    if not name:
        name = geometry + '_bls'

    if method == 0:
        #Crappy method just to get the ball rolling.
        
        deformer = cmds.blendShape(geometry, name=name)        
        cmds.namespace(add="TARGET_MESH")
        target_mesh = cmds.duplicate(geometry, name='TARGET_MESH:neutral')[0]
        i = 0
        for nm in network.shapes.keys():
            if nm == 'neutral_100':
                continue
            
            shp = network.shapes.get(nm)
            target_mesh = cmds.rename(target_mesh, "TARGET_MESH:"+nm)
            model.apply_shape_to_maya_node(shp, target_mesh)
            cmds.blendShape(deformer , edit=True, t=(geometry, i, target_mesh, 1.0))
            cmds.disconnectAttr(*cmds.listConnections("{}.worldMesh[0]".format(target_mesh), c=True, s=True, p=True, d=True))
            
            i += 1
        
        cmds.delete(target_mesh)
        cmds.namespace(rm='TARGET_MESH')

    return deformer[0]

def create_combo_drivers(network, deformer):
    """Create a combinationShape maya node for each combo shape and connect output of drivers to
    said node, and said node to combo inputs.

    Args:
        network (obj): Network containing shapes.
        deformer (str): Name of maya blendShape deformer to connect to.
    """
    
    combos = network.get_shapes(["combo"])
    
    for name, shp in combos.iteritems():
        combination_node = cmds.createNode('combinationShape', name=name+'_combo')
        
        cmds.setAttr("{}.combinationMethod".format(combination_node), 1)
        for i, driver in enumerate(shp.combination_of):
            cmds.connectAttr("{}.{}".format(deformer, driver.name), "{}.inputWeight[{}]".format(combination_node, i))
        
        cmds.connectAttr("{}.outputWeight".format(combination_node), "{}.{}".format(deformer, name))
            
    

def create_inbetween_drivers(network, deformer):
    """Create a valueRemap node for each inbetween shape and connect output of drivers to
    said node, and said node to inbetween inputs.

    Args:
        network (obj): Network containing shapes.
        deformer (str): Name of maya blendShape deformer to connect to.
    """    
    
    core_shapes = network.get_shapes(["core", "combo"])
    core_shapes_with_inbetweens = []
    
    for name, core in core_shapes.iteritems():
        if core.inbetweens.keys():
            core_shapes_with_inbetweens.append(core)
        
    for core in core_shapes_with_inbetweens:
        
        inbetweens = core.inbetweens
        
        percentages = inbetweens.keys()
        percentages = [0] + sorted(percentages) + [100]
        
        for i, percentage in enumerate(percentages):
            if i == 0 or i == len(percentages)-1:
                continue
            
            inbetween = inbetweens[percentage]
            
            remap_node = cmds.createNode('remapValue', name=inbetween.name+'_remap')
            
            #Start of curve
            cmds.setAttr("{}.value[0].value_Position".format(remap_node), percentages[i-1]/100.0)
            cmds.setAttr("{}.value[0].value_FloatValue".format(remap_node), 0)
            cmds.setAttr("{}.value[0].value_Interp".format(remap_node), 1)
            
            #Middle of curve
            cmds.setAttr("{}.value[1].value_Position".format(remap_node), percentage/100.0)
            cmds.setAttr("{}.value[1].value_FloatValue".format(remap_node), 1)
            cmds.setAttr("{}.value[1].value_Interp".format(remap_node), 1)
            
            #End of curve
            cmds.setAttr("{}.value[2].value_Position".format(remap_node), percentages[i+1]/100.0)
            cmds.setAttr("{}.value[2].value_FloatValue".format(remap_node), 0)
            cmds.setAttr("{}.value[2].value_Interp".format(remap_node), 1)
            
            cmds.connectAttr("{}.{}".format(deformer, core.name), "{}.inputValue".format(remap_node))
            cmds.connectAttr("{}.outValue".format(remap_node), "{}.{}".format(deformer, inbetween.name))
            

    
    
    
    
    
    
    
    
