"""Module for maya control creation.

:author: Chris Gill

TO DO:
Expand the control choices.
Add multi axis support for translate control.

"""
import logging
import sys
from ctypes import c_float
import re

from maya import cmds

import maya.api.OpenMaya as om
import maya.OpenMaya as om1

import numpy as np

from face import processing, core
from fdev.utils import transform
from __builtin__ import True
reload(transform)

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

NAMING_CONFIG = {
    "group":"{name}_GRP",
    "control":"{name}_CTRL",
    "shape":"{name}_CTRLShape",
    "locator":"{name}_LOC",
    "guide":"{name}_LOC",
    "mesh":"{name}_MSH"
    }

COLOR_CONFIG = {
    "C":[0,1,0],
    "L":[0,0,1],
    "R":[1,0,0],
    }


class TranslateControl(object):
    """Object containing maya control information.

    Attributes:
        name (str): Name of control.
        transform_matrix (npArray): transform matrix describing position and orientation of the control (normlized).
        scale (list): Scale of the control shape(Not the whole control transform, that comes from the transform matrix).
    """
    
    def __init__(self, name="default", axes=["y", "-y"], scale=[1,1,1]):
        self.name = name
        self.axes = axes
        self.scale = scale
        self.side = self.name.split("_")[0]
        self.root = None
        self.offset = None
        self.transform = None
        self.shapes = []
        
        self.output_attributes = {
            "tx":None,
            "ty":None,
            "tz":None,
            "-tx":None,
            "-ty":None,
            "-tz":None
            }
        
    
    def run(self):
        """Create control nodes in maya (group tree, shape, attributes).
        
        """
                
        if not self.axes:
            LOG.error("No axes given.")
            return 
        
        #Create transform hierarchy and create shape.
        self.root = cmds.createNode("transform", name="{}_GRP".format(self.name))
        self.offset = cmds.createNode("transform", name="{}Offset_GRP".format(self.name), parent=self.root)
        self.transform = cmds.createNode("transform", name="{}_CTRL".format(self.name), parent=self.offset)
        self.shape, self.indicator_inputs = self.create_shape(axes=self.axes, parent=self.transform, shape="teardrop", scale=self.scale, side=self.side)

        positive_limit = [0,0,0]
        negative_limit = [0,0,0]
        
        for axis in self.axes:
            attr_name = ""
            if axis.lower() in ["tx", "ty", "tz"]:
                
                positive_limit[["tx", "ty", "tz"].index(axis.lower())] = 1
                
                if not cmds.objExists("{}_positive_clamp".format(self.name)):
                    clamp = cmds.createNode("clamp", name="{}_positive_clamp".format(self.name))
                    cmds.connectAttr("{}.t".format(self.transform), "{}.input".format(clamp))

                else:
                    clamp = "{}_positive_clamp".format(self.name)                

                color = "RGB"[["tx", "ty", "tz"].index(axis.lower())]
                
                attr_name = "positive{}".format(axis.upper())
                cmds.setAttr("{}.min{}".format(clamp, color), 0)
                cmds.setAttr("{}.max{}".format(clamp, color), 1)
                
                output_attr = "{}.output{}".format(clamp, color)

                
            elif axis.lower() in ["-tx", "-ty", "-tz"]:
                negative_limit[["tx", "ty", "tz"].index(axis[1:].lower())] = -1
                
                if not cmds.objExists("{}_negative_clamp".format(self.name)):
                    clamp = cmds.createNode("clamp", name="{}_negative_clamp".format(self.name))
                    cmds.connectAttr("{}.t".format(self.transform), "{}.input".format(clamp))
                else:
                    clamp = "{}_negative_clamp".format(self.name)

                if not cmds.objExists("{}_negative_mult".format(self.name)):
                    mult = cmds.createNode("multiplyDivide", name="{}_negative_mult".format(self.name))
                    cmds.connectAttr("{}.output".format(clamp), "{}.input1".format(mult))
                    cmds.setAttr("{}.input2X".format(mult), -1)
                    cmds.setAttr("{}.input2Y".format(mult), -1)
                    cmds.setAttr("{}.input2Z".format(mult), -1)

                else:
                    mult = "{}_negative_mult".format(self.name)  

              

                color = "RGB"[["tx", "ty", "tz"].index(axis[1:].lower())]

                attr_name = "negative{}".format(axis[1:].upper())
                cmds.setAttr("{}.min{}".format(clamp, color), -1)
                cmds.setAttr("{}.max{}".format(clamp, color), 0)

                output_attr = "{}.output{}".format(mult, axis[-1].upper())
                
            else:
                LOG.error("%s is not a valid axis (tx, -tx, ty, -ty, tz, -tz).", axis)
                return
            
            #Attribute that will be the outward connection to drive a shape.
            cmds.addAttr(self.transform, ln=attr_name, at="float", dv=0, min=0, max=1, k=True)
            
            cmds.connectAttr(output_attr, "{}.{}".format(self.transform, attr_name))
            
            if self.indicator_inputs[axis]:
                cmds.connectAttr("{}.{}".format(self.transform, attr_name), self.indicator_inputs[axis])
            
        
            self.output_attributes[axis] = ".".join([self.transform, attr_name])
        
            cmds.transformLimits(self.transform,
                                 tx=(negative_limit[0], positive_limit[0]),
                                 ty=(negative_limit[1], positive_limit[1]),
                                 tz=(negative_limit[2], positive_limit[2]),
                                 etx=(True, True),
                                 ety=(True, True),
                                 etz=(True, True)
                                 )
        

            cmds.setAttr("{}.tx".format(self.transform),
                         k=negative_limit[0]!=0 or positive_limit[0]!=0,
                         l=not (negative_limit[0]!=0 or positive_limit[0]!=0)
                         )
             
            cmds.setAttr("{}.ty".format(self.transform),
                         k=negative_limit[1]!=0 or positive_limit[1]!=0,
                         l=not (negative_limit[1]!=0 or positive_limit[1]!=0)
                         )
#             
            cmds.setAttr("{}.tz".format(self.transform),
                         k=negative_limit[2]!=0 or positive_limit[2]!=0,
                         l=not (negative_limit[2]!=0 or positive_limit[2]!=0)
                         )
             
            cmds.setAttr("{}.rx".format(self.transform), k=False, l=True)
            cmds.setAttr("{}.ry".format(self.transform), k=False, l=True)
            cmds.setAttr("{}.rz".format(self.transform), k=False, l=True)
 
            cmds.setAttr("{}.sx".format(self.transform), k=False, l=True)
            cmds.setAttr("{}.sy".format(self.transform), k=False, l=True)
            cmds.setAttr("{}.sz".format(self.transform), k=False, l=True)

            
    def attach_to_mesh(self, mesh):
        """Attach control to given mesh.
        
        Notes:
        This attachment method is kind of slow, should try the combined shape transform idea.
        
        
        Args:
            mesh (str): Name of mesh to attach to.
        """
                
        self.attachment_point, self.attachment_constraint = transform.make_point_on_mesh(mesh, self.transform, name=NAMING_CONFIG["locator"].format(name = self.name+"_attachment"), maintain_offset=False)
        
        cmds.parent(self.attachment_point, self.root)
        decomp = cmds.createNode("decomposeMatrix", name="{}_attachment_DCM".format(self.name))
        
        offset = cmds.createNode("fourByFourMatrix", name="{}_attachment_offset_FFM".format(self.name))
        
        ctrl_matrix = np.array(cmds.xform(self.transform, m=True, ws=True, q=True)).reshape(4,4)
        attachment_point_matrix = np.array(cmds.xform(self.attachment_point, m=True, ws=True, q=True)).reshape(4,4)
         
        inverse_attachment_point_matrix = np.linalg.inv(attachment_point_matrix)
         
        offset_matrix = np.dot(ctrl_matrix, inverse_attachment_point_matrix)
         
        for i, first in enumerate(offset_matrix):
            for j, second in enumerate(first):
                cmds.setAttr("{}.in{}{}".format(offset, i, j), second)
         
        
        attachment_matrix_mult = cmds.createNode("multMatrix", name="{}_attachment_MM".format(self.name))
        offset_matrix_mult = cmds.createNode("multMatrix", name="{}_attachment_offset_MM".format(self.name))
        
        
        cmds.connectAttr("{}.output".format(offset), "{}.matrixIn[0]".format(offset_matrix_mult))
        cmds.connectAttr("{}.worldMatrix".format(self.attachment_point), "{}.matrixIn[1]".format(offset_matrix_mult))
        
        cmds.connectAttr("{}.inverseMatrix".format(self.transform), "{}.matrixIn[0]".format(attachment_matrix_mult))
        cmds.connectAttr("{}.matrixSum".format(offset_matrix_mult), "{}.matrixIn[1]".format(attachment_matrix_mult))

         
        cmds.connectAttr("{}.matrixSum".format(attachment_matrix_mult), "{}.inputMatrix".format(decomp))

        cmds.connectAttr("{}.outputTranslate".format(decomp), "{}.t".format(self.offset))
        cmds.connectAttr("{}.outputRotate".format(decomp), "{}.r".format(self.offset))
        cmds.connectAttr("{}.outputScale".format(decomp), "{}.s".format(self.offset))
        
        
        
        
    def create_shape(self, shape="locator", axes=["ty"], normal="z", parent=None, scale=[0.2, 0.2, 0.2], side="C"):
        """Import/create the shape and indicator to parent to the control transform.
        
        Notes:
            Normal flag currently doesn't do anything.
            
            Teardrop locator is also unidirectional and just points along the Y axis
            
        Args:
            shape (str): Type of control shape(Circle, Teardrop, Arrow, Locator).
            axes (list): Name of mesh to attach to.
            normal (str): Name of mesh to attach to.
            parent (str): Maya transform to parent control shape under.
            scale (list): Scale of control shape.
        
        Returns:
            list: string for the control shape name, dictionary of the indicator attribute per axis.
        """

        color = COLOR_CONFIG[side]


        if shape == "locator":
            loc_transform = cmds.spaceLocator()
            loc_shape = cmds.listRelatives(loc_transform, shapes=True)[0]
            loc_shape = cmds.rename(loc_shape, "{}_CTRLShape".format(self.name))
            self.shapes.extend(loc_shape)
            cmds.parent(loc_shape, parent, r=True, s=True)
            cmds.delete(loc_transform)
            return loc_shape, None
        
        elif shape == "teardrop":
            indicator_inputs = {
            "tx":None,
            "ty":None,
            "tz":None,
            "-tx":None,
            "-ty":None,
            "-tz":None
            }

            for axis in axes:
                if "-" in axis:
                    negative = True
                else:
                    negative = False
                
                ctrl_transform = cmds.file("C:/Users/Chris/Desktop/teardrop_curve.ma", i=True, rnn=True)[0]
                
                ctrl_shape = cmds.listRelatives(ctrl_transform, shapes=True)[0]
                ctrl_shape = cmds.rename(ctrl_shape, "{}_{}_CTRLShape".format(self.name, axis.replace("-", "n")))
                self.shapes.extend(ctrl_shape)
                
                cmds.xform(ctrl_transform, ro=[-90,0,0])
                cmds.makeIdentity(ctrl_transform, apply=True, t=1, r=1, s=1)
                
                if len(axes) == 1:
                    translate_value = 0
                else:
                    if negative:
                        translate_value = scale[0]/-0.6
                    else:
                        translate_value = scale[0]/0.6
                
                if "y" in axis and negative == True:
                    cmds.setAttr("{}.rz".format(ctrl_transform), 180)
                elif "x" in axis and negative == False:
                    cmds.setAttr("{}.rz".format(ctrl_transform), -90)
                elif "x" in axis and negative == True:
                    cmds.setAttr("{}.rz".format(ctrl_transform), 90)
                elif "z" in axis:
                    cmds.setAttr("{}.rx".format(ctrl_transform), 90)


                center_point_curve = cmds.curve(p=[[0,0,0], [0,0,0]], d=1)

                
                surface = cmds.loft( center_point_curve, ctrl_shape, ch=False, rn=True, ar=True)
                poly = cmds.nurbsToPoly(surface,
                                        polygonType=1,
                                        format=2,
                                        vNumber=128,
                                        uNumber=1,
                                        uType=1,
                                        vType=1,
                                        constructionHistory=False,
                                        useChordHeightRatio=False,
                                        name=NAMING_CONFIG["mesh"].format(name="_".join([self.name, axis.replace("-", "n")]))
                                        )[0]
                cmds.xform(poly, s=scale)
                
                cmds.makeIdentity(poly, apply=True, t=1, r=1, s=1)
                cmds.setAttr("{}.{}".format(poly, axis[-2:]), translate_value)

                cmds.setAttr("{}.{}".format(ctrl_transform, axis[-2:]), translate_value)
                cmds.makeIdentity(ctrl_transform, apply=True, t=1, r=1, s=1)
                cmds.xform(ctrl_transform, s=scale)
                cmds.makeIdentity(ctrl_transform, apply=True, t=1, r=1, s=1)
    
                

                cmds.parent(ctrl_shape, parent, r=True, s=True)
                
                

                
                cmds.setAttr("{}.overrideEnabled".format(poly), 1)
                cmds.setAttr("{}.overrideDisplayType".format(poly), 2)
                
#                 cmds.setAttr("{}.overrideEnabled".format(ctrl_shape), 1)
                cmds.color(ctrl_shape, rgb=color )
                cmds.polyColorPerVertex(poly, r=color[0], g=color[1], b=color[2], a=1, cdo=True)
                
                cmds.parent(poly, self.offset, r=True, s=True)
                cmds.parentConstraint(parent, poly, mo=True)
                
                cmds.delete([ctrl_transform]+[center_point_curve]+surface)
                
                max = cmds.createNode("plusMinusAverage")
                clamp = cmds.createNode("clamp")
                cmds.setAttr("{}.minR".format(clamp), 0)
                cmds.setAttr("{}.minG".format(clamp), 0)
                cmds.setAttr("{}.minB".format(clamp), 0)
                cmds.setAttr("{}.maxR".format(clamp), 1)
                cmds.setAttr("{}.maxG".format(clamp), 1)
                cmds.setAttr("{}.maxB".format(clamp), 1)
                
                cmds.connectAttr("{}.output1D".format(max), "{}.inputR".format(clamp))
                
                cmds.connectAttr("{}.outputR".format(clamp), "{}.sx".format(poly))
                cmds.connectAttr("{}.outputR".format(clamp), "{}.sy".format(poly))
                cmds.connectAttr("{}.outputR".format(clamp), "{}.sz".format(poly))
                
                if negative:
                    indicator_inputs["-{}".format(axis[-2:])] = "{}.input1D[0]".format(max)
                else:
                    indicator_inputs["{}".format(axis)] = "{}.input1D[0]".format(max)

            
            return ctrl_shape, indicator_inputs

def get_transform_from_maya_node(control, node, normalize=True):
    """Fill vtx_array with data extracted from a maya mesh.

    Should probably come up with a better way of running maya commands, not in generic module.
    maybe move this to model with the apply function.

    Args:
        control (str): Node to get mesh data from.
        subtract_neutral (bool) Whether to subtract the neutral and make the shape a delta.
        method (int) Type of command used to get vertex list, 0=openMaya, 1=xform.
    """
    
    if not type(control) == core.Control:
        LOG.error("Given control object is not an instance of core.Control")
        return

    if not cmds.objExists(node):
        LOG.error("Given node does not exist in scene.")
        return
    
    transform_matrix = transform.get_transform(node, normalize=normalize)
    
    return transform_matrix



def normalize_vector(v):
    """Set vector's magnitude to 1 (Make unit vector).
    Args:
        v (str): Vector to normalize.
    """
    norm = np.linalg.norm(v)
    if norm == 0: 
       return v
    return v / norm

 
def mirror_control_data(control):
    """Mirror the transform matrix and driven shape names.

    Args:
        control (object): Control to mirror.
    
    Returns:
        list: [mirrored transform matrix, mirrored driven shapes]
    """

    mirrored_driven_shapes = {}
    mirrored_transform_matrix = None
    mirror_axis = control.mirror_axis
      
    
    
    
    mirror_matrix = np.array([1,0,0,1,
                              0,1,0,1,
                              0,0,1,1,
                              0,0,0,1]).reshape(4,4)
    
    if mirror_axis == 0:
        mirror_matrix[0][0] = -1
    elif mirror_axis == 1:
        mirror_matrix[1][1] = -1
    elif mirror_axis == 2:
        mirror_matrix[2][2] = -1
    
    mirrored_transform_matrix = np.dot(control.transform_matrix, mirror_matrix)
      
    for attr, shape in control.driven_shapes.iteritems():
        if shape:
            split = re.findall(r'(.+?)([A-Z]\w+)', shape)[0]
            
            prefixes = "".join(sorted(split[0].replace(control.mirror_prefixes[0], control.mirror_prefixes[1])))
            
            mirrored_driven_shapes[attr] = prefixes + split[1]
        else:
            mirrored_driven_shapes[attr] = None
            
    return [mirrored_transform_matrix, mirrored_driven_shapes]
          
 
 
def create_interface_from_network(network, name="default", mesh=None, as_guides=False):
    """Create a control interface from provided shape network.
 
    Args:
        network (obj): Network containing controls to create.
        name (str): Name to give interface root node.
        mesh (str): Mesh to attach controls to (If enabled on control).
        as_guides (bool): If true, build locators in control location for updating.
 
    Returns:
        str: Name of face rig root.
    """
    
    if mesh == None:
        LOG.warning("create_interface_from_network: Mesh not supplied, attach_to_mesh controls will be treated as standard")
    
    if cmds.objExists(NAMING_CONFIG["group"].format(name = name)):
        root = NAMING_CONFIG["group"].format(name = name)
    else:
        root = cmds.createNode("transform", name=NAMING_CONFIG["group"].format(name = name))
     
     
    if as_guides:
         
        if cmds.objExists(NAMING_CONFIG["group"].format(name = name+"_guides")):
            cmds.delete(NAMING_CONFIG["group"].format(name = name+"_guides"))
                   
        guide_group = cmds.createNode("transform", name=NAMING_CONFIG["group"].format(name = name+"_guides"))
        cmds.parent(guide_group, root)
 
    
    connection_list = []
    
    for ctrl_name, ctrl in network.controls.iteritems():
        driven_shapes_axes = [axis for axis, inst in ctrl.driven_shapes.iteritems() if inst]
        enabled_axes = [axis for axis, inst in ctrl.enabled_axes.iteritems() if inst==True]
        
        
        if as_guides:
            guide = cmds.spaceLocator(name=NAMING_CONFIG["guide"].format(name = ctrl_name))
            cmds.xform(guide, m=ctrl.transform_matrix.reshape(1,16)[0].tolist(), ws=True)
            cmds.parent(guide, guide_group)
            
            continue
        
        if not enabled_axes:
            continue
        
        mag = [np.linalg.norm(ctrl.transform_matrix[0][:-1]),
               np.linalg.norm(ctrl.transform_matrix[1][:-1]),
               np.linalg.norm(ctrl.transform_matrix[2][:-1])]
        
        size = ctrl.size
        
        scale = [size/mag[0], size/mag[1], size/mag[2]]
        
        if ctrl.mirror:
            mirrored_transform_matrix, mirrored_driven_shapes = mirror_control_data(ctrl)
            maya_ctrl_name = "{}_{}".format(ctrl.mirror_prefixes[1].upper(), ctrl_name)
            
            mirrored_control = TranslateControl(name=maya_ctrl_name, axes=enabled_axes, scale=scale)
            mirrored_control.run()
            cmds.xform(mirrored_control.offset, m=mirrored_transform_matrix.reshape(1,16)[0].tolist(), ws=True)
            if ctrl.attach_to_mesh and mesh != None:
                mirrored_control.attach_to_mesh(mesh)
            
            cmds.parent(mirrored_control.root, root)
            
            
            for axis in enabled_axes:
                connection_list.append([mirrored_control.output_attributes[axis], mirrored_driven_shapes[axis]])
                
            maya_ctrl_name = "{}_{}".format(ctrl.mirror_prefixes[0].upper(), ctrl_name)

        else:
            maya_ctrl_name = "C_{}".format(ctrl_name)

        control = TranslateControl(name=maya_ctrl_name, axes=enabled_axes, scale=scale)
        control.run()
        cmds.xform(control.offset, m=ctrl.transform_matrix.reshape(1,16)[0].tolist(), ws=True)
        
        if ctrl.attach_to_mesh and mesh != None:
            control.attach_to_mesh(mesh)
            
        cmds.parent(control.root, root)

        for axis in enabled_axes:
            connection_list.append([control.output_attributes[axis], ctrl.driven_shapes[axis]])
 
    return root, connection_list


def connect_interface(connection_list, blendshape="default_blendShape"):
       
    for connection in connection_list:
        if cmds.objExists('{}.{}'.format(blendshape, connection[1])):
            cmds.connectAttr(connection[0], '{}.{}'.format(blendshape, connection[1]))
        else:
            LOG.error("%s.%s does not exist.", blendshape, connection[1])
            return




