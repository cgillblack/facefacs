"""Module docstring.

:author: Chris Gill

"""
import logging
import sys
from ctypes import c_float
import re

import face.core as fcore
import face.processing as processing
from ngSkinTools.importExport import LayerData
from ngSkinTools.mllInterface import MllInterface
import numpy as np
from maya import cmds
reload(fcore)
reload(processing)

NAMING_CONFIG = {
    "joint":"{name}_JNT"
    }


def get_skincluster_data(mesh):
    data = LayerData()
    data.loadFrom(mesh)
    
    layers = data.layers
    weights = layers[0].influences[0].weights
    
    output = {}
    for inf in layers[0].influences:
        output[inf.influenceName] = np.array(inf.weights)
    
    return output
    
#     self.map.map_array = map_array
