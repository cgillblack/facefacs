#https://gist.github.com/tbttfox/9ca775bf629c7a1285c27c8d9d961bca


from maya import OpenMaya as om
import numpy as np
from ctypes import c_float, c_double, c_int, c_uint

#type: (pyTyp, comps, ctp, ptrTyp)
_CONVERT_DICT = {
    om.MPointArray:       (float, 4, c_double, om.MScriptUtil.asDouble4Ptr),
    om.MFloatPointArray:  (float, 4, c_float , om.MScriptUtil.asFloat4Ptr),
    om.MVectorArray:      (float, 3, c_double, om.MScriptUtil.asDouble3Ptr),
    om.MFloatVectorArray: (float, 3, c_float , om.MScriptUtil.asFloat3Ptr),
    om.MDoubleArray:      (float, 1, c_double, om.MScriptUtil.asDoublePtr),
    om.MFloatArray:       (float, 1, c_double, om.MScriptUtil.asFloatPtr),
    om.MIntArray:         (int  , 1, c_int   , om.MScriptUtil.asIntPtr),
    om.MUintArray:        (int  , 1, c_uint  , om.MScriptUtil.asUintPtr),
}

def _swigConnect(mArray, count, util):
    '''
    Use an MScriptUtil to build SWIG array that we can read from and write to.
    Make sure to get the MScriptUtil from outside this function, otherwise
    it may be garbage collected
    The _CONVERT_DICT holds {mayaType: (pyType, numComps, cType, ptrType)} where
        pyType: The type that is used to fill the MScriptUtil array.
        numComps: The number of components. So a double4Ptr would be 4
        cType: The ctypes type used to read the data
        ptrType: An unbound method on MScriptUtil to cast the pointer to the correct type
            I can still call that unbound method by manually passing the usually-implicit
            self argument (which will be an instance of MScriptUtil)
    '''
    pyTyp, comps, ctp, ptrTyp = _CONVERT_DICT[type(mArray)]
    cc = (count * comps)
    util.createFromList([pyTyp()] * cc, cc)

    #passing util as 'self' to call the unbound method
    ptr = ptrTyp(util)
    mArray.get(ptr)

    # Multiplication doesn't follow the normal rules here:
    # ((ctype*3)*N) -> Nx3 array
    # ((ctype*N)*3) -> 3xN array
    cdata = (ctp * count) * comps

    # int(ptr) gives the memory address
    cta = cdata.from_address(int(ptr))
    
    # This makes numpy look at the same memory as the ctypes array
    # so we can both read from and write to that data through numpy
    npArray = np.ctypeslib.as_array(cta)
    return npArray, util, ptr

def mayaToNumpy(mArray):
    ''' Convert a maya array to a numpy array '''
    util = om.MScriptUtil()
    count = mArray.length()
    npArray, util, ptr = _swigConnect(mArray, count, util)
    return np.copy(npArray)

def numpyToMaya(ary, mType):
    ''' Convert a numpy array to a specific maya type array '''
    util = om.MScriptUtil()
    # Add a little shape checking
    pyTyp, comps, ctp, ptrTyp = _CONVERT_DICT[mType]
    if comps == 1:
        if len(ary.shape) != 1:
            raise ValueError("Numpy array must be 1D to convert to the given maya type")
    else:
        if len(ary.shape) != 2:
            raise ValueError("Numpy array must be 2D to convert to the given maya type")
        if ary.shape[1] != comps:
            raise ValueError("Numpy array must have the proper shape. Dimension 2 has size {0}, but needs size {1}".format(ary.shape[1], comps))
    count = ary.shape[0]
    mArray = mType(count)
    npArray, util, ptr = _swigConnect(mArray, count, util)
    
    np.copyto(npArray, ary)
    return mType(ptr, count)

################################################################################

def tmp_getNumpyAttr(attrName, meshIdx=0):
    ''' A temp function to show off how to get data 
    This will have to recognize more apiTypeStr values to be truly general
    '''
    sl = om.MSelectionList()
    sl.add(attrName)
    plug = om.MPlug()
    sl.getPlug(0, plug)

    pmo = plug.asMObject()
    apiTypeStr = pmo.apiTypeStr()

    if apiTypeStr == 'kPointArrayData':
        fnPmo = om.MFnPointArrayData(pmo)
        ary = fnPmo.array()
        return mayaToNumpy(ary)

    elif apiTypeStr == 'kComponentListData':
        fnPmo = om.MFnComponentListData(pmo)
        fnEL = om.MFnSingleIndexedComponent(fnPmo[meshIdx])
        mir = om.MIntArray()
        fnEL.getElements(mir)
        return mayaToNumpy(mir)

def test():
    import time
    from maya import cmds   
    meshName = 'Face_SIMPLEX'
    bsName = 'Face_BS'
    meshIdx = 0
    bsIdx = 1

    # A quick test showing how to build a numpy array
    # containing the deltas for a shape on a blendshape node
    numVerts = cmds.polyEvaluate(meshName, vertex=True)
    baseAttr = '{0}.it[{1}].itg[{2}].iti[6000]'.format(bsName, meshIdx, bsIdx)
    inPtAttr = baseAttr + '.inputPointsTarget'
    inCompAttr = baseAttr + '.inputComponentsTarget'

    start = time.time()
    points = tmp_getNumpyAttr(inPtAttr)
    idxs = tmp_getNumpyAttr(inCompAttr)
    ret = np.zeros((numVerts, 4))
    ret[idxs] = points
    end = time.time()

    print "IDXS", idxs.shape
    print "OUT", points.shape
    print "RET", ret.shape
    print "TOOK", end - start

if __name__ == "__main__":
    test()