"""Module docstring.

:author: Chris Gill

"""
import logging

from maya import cmds
import maya.api.OpenMaya as om

import numpy as np

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

def get_closest_point(mesh, point):
    point_p = om.MPoint(point[0],point[1],point[2])
    # return om.MVector(pnt1-pnt2).length()

    selectionList = om.MSelectionList()
    selectionList.add(mesh)
    nodeDagPath = selectionList.getDagPath(0)
    
    mfn = om.MFnMesh(nodeDagPath)

    intersection = mfn.getClosestPoint(point_p, om.MSpace.kWorld)


    #return intersection#list(intersection[0])[:-1]
    vector = list(intersection[0])
    MPoint = intersection[0]
    distance = np.linalg.norm(np.array([vector[0], vector[1], vector[2]]) - np.array([MPoint[0], MPoint[1], MPoint[2]]))#rig.distance_between(point, vector)


    return {'vector':vector, 'distance':distance, 'MPoint':MPoint}


def get_closest_uv_on_mesh(mesh, transform):
    """
    get closest uv value to mesh
    :param mesh: str | mesh
    :param transform: str | node
    :return: MPoint
    """

    # get mesh
    mfn_mesh = om.MFnMesh(om.MSelectionList().add(mesh).getDagPath(0))

    point = get_closest_point(mesh, transform)['MPoint']

    closest_uv = mfn_mesh.getUVAtPoint(point, om.MSpace.kWorld)

    return closest_uv



