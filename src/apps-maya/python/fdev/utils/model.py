"""Module docstring.

:author: Chris Gill

"""
import logging
import sys
from ctypes import c_float

from maya import cmds

import maya.api.OpenMaya as om
import maya.OpenMaya as om1

import numpy as np

from face import processing
from fdev.utils import mayaToNumpy as mtn
reload(mtn)

LOG = logging.getLogger(__name__)
LOG.setLevel(40)



    
def apply_shape_to_maya_node(shape, node, add_neutral=True, add_combos=False, include_self=True, world_space=False, network=None):
    """Fill vtx_array with data extracted from a maya mesh.

    Examples:
        Normal:(Shows current shape relative to neutral, with no combos)
            (node, add_neutral=True, add_combos=False, include_self=True, world_space=False)
            
        Absolute shape:(Shows result of all combos and current shape)
            (node, add_neutral=True, add_combos=True, include_self=True, world_space=False)
            
        Raw combo, no additional correction:(Used to create first shape base)
            (node, add_neutral=True, add_combos=True, include_self=False, world_space=False)

    Args:
        node (str): Node to get mesh data from.
        subtract_neutral (bool) Whether to subtract the neutral and make the shape a delta.
        add_combos (bool) Recursively add combo shapes, will make absolute version of current shape.
        include_self (bool) Applies current shape, false if raw combo base is required.
        world_space (bool) Whether to apply vtx_array in Local or World space.
    """
    
    if len(shape.neutral_shape.vtx_array) == 0:
        LOG.error("apply_vtx_array_to_maya_node: %s has no vtx_array.", type(shape.name))
        return
    
    if add_combos and not network:
        LOG.error("Cannot add combination shapes without reference network")
        return
    
    if add_neutral:
        if not shape._neutral_shape:
            LOG.error("%s has no neutral shape set.", type(shape.name))
            return
        new_vtx_array = processing.add_vtx_arrays(shape.vtx_array, [shape._neutral_shape.vtx_array])
    else:
        new_vtx_array = shape.vtx_array
        
    if not include_self:
        new_vtx_array = processing.subtract_vtx_arrays(new_vtx_array, [shape.vtx_array])
        
    if add_combos:
        core_shapes = network.get_combination_core_shapes(shape, recursive=True)
        combo_shapes = network.get_combination_shapes(core_shapes)
        all_shapes = core_shapes + combo_shapes
        all_shapes.remove(shape)
        new_vtx_array = processing.add_vtx_arrays(new_vtx_array, [shp.vtx_array for shp in all_shapes])


    apply_vtx_array(node, new_vtx_array, world_space=world_space, method=0)


def get_vtx_array_from_maya_node(shape, node, subtract_neutral=True, method=0, subtract_combos=True, network=None, inbetween=False):
    """Fill vtx_array with data extracted from a maya mesh.

    Should probably come up with a better way of running maya commands, not in generic module.
    maybe move this to model with the apply function.

    Args:
        node (str): Node to get mesh data from.
        subtract_neutral (bool) Whether to subtract the neutral and make the shape a delta.
        method (int) Type of command used to get vertex list, 0=openMaya, 1=xform.
    """
    if subtract_combos and not network:
        LOG.error("Cannot subtract combination shapes without reference network")
        return
    
    new_vtx_array = get_vtx_array(node, method=method)
    if subtract_neutral:
        if not shape._neutral_shape:
            LOG.error("get_vtx_array_from_maya_node: %s has no neutral shape set.", shape.name)
            return

        new_vtx_array = processing.subtract_vtx_arrays(new_vtx_array, [shape._neutral_shape.vtx_array])
    
    if subtract_combos and shape.combination_of:
        core_shapes = network.get_combination_core_shapes(shape)
        combo_shapes = network.get_combination_shapes(core_shapes)
        all_shapes = core_shapes + combo_shapes
        new_vtx_array = processing.subtract_vtx_arrays(new_vtx_array, [shp.vtx_array for shp in all_shapes])
    
    if inbetween:
        if not shape.percentage:
            LOG.error("get_vtx_array_from_maya_node: %s has no inbetween percentage set.", shape.name)
            return
        
        if not shape.inbetween_for:
            LOG.error("get_vtx_array_from_maya_node: %s has no inbetween_for set. (parent core shape)", shape.name)
            return
        
        percentage = float(shape.percentage)
        new_vtx_array = processing.subtract_vtx_arrays(new_vtx_array, [shape.inbetween_for.vtx_array*(percentage/100.0)])
    
    return new_vtx_array



def get_vtx_array(node, world_space=True, method=0):
    """Get a numpy array of the vertex position of all verticies in a mesh.

    Args:
        node (str): Provided node for which to extract vertex positions.
        world_space (bool): Get vertex position in world_space if True or local if False.
        method (int): Which method to use to extra the vertex positions (0:OpenMaya, 1:xform)

    Returns:
        npArray: Numpy array of all vertex positions.
    """
    if not cmds.objectType(node) == "mesh":
        LOG.warning("%s is not of type 'mesh' searching for mesh child.", node)
        node_shape = cmds.listRelatives(node, type="mesh")

        if node_shape:
            node = node_shape[0]
            LOG.info("\tMesh child found, using %r.", node)
        else:
            LOG.info("Mesh child cannot be found for %r.", node)
            return None

    if method == 0 or method == 2:

    
        if method == 0:
            selectionList = om.MSelectionList()
            selectionList.add(node)
            nodeDagPath = selectionList.getDagPath(0)
            
            mfn = om.MFnMesh(nodeDagPath)
            vtx_array = np.array(mfn.getPoints())
            
        elif method == 2:
            
            selectionList = om1.MSelectionList()
            selectionList.add(node)
    
            nodeDagPath = om1.MDagPath()
            selectionList.getDagPath(0, nodeDagPath)
    
            mfn = om1.MFnMesh(nodeDagPath)
            
            raw_points = mfn.getRawPoints()
            point_count = mfn.numVertices()
            # Cast the swig double pointer to a ctypes array
            
            cta = (c_float * 3 * point_count).from_address(int(raw_points))
            # Memory map the ctypes array into numpy
            out = np.ctypeslib.as_array(cta)
            # ptr, cta, and out are all pointing to the same memory address now
            # so changing out will change ptr
            # but in this case

            # for safety, make a copy of out so I don't corrupt memory
            vtx_array = np.copy(out)

            zeros = np.reshape(np.ones(point_count), (point_count, 1))
            vtx_array = np.concatenate((vtx_array,zeros),axis = 1)
            
    elif method == 1:
        print 'DONT USE THIS METHOD'
        vtx_array = np.array(
            cmds.xform(
                "{}.vtx[*]".format(node),
                translation=True,
                query=True,
                worldSpace=world_space,
            )
        )
        
        
        vtx_num = len(vtx_array)/3
        vtx_array = np.reshape(vtx_array,(vtx_num,3))
        zeros = np.reshape(np.ones(vtx_num), (vtx_num, 1))
        vtx_array = np.concatenate((vtx_array,zeros),axis = 1)
    

        
    return vtx_array




def apply_vtx_array(node, vtx_array, world_space=True, method=0):
    """Apply vertex position to provided model (setting model to that shape).

    Args:
        node (str): Provided node to be modified.
        vtx_array (obj): Numpy array of vertex positions.
        world_space (bool): Get vertex position in world_space if True or local if False.
        method (int): Which method to use to extra the vertex positions (0:OpenMaya)
    """
    if not cmds.objectType(node) == "mesh":
        LOG.warning("%s is not of type 'mesh' searching for mesh child.", node)
        node_shape = cmds.listRelatives(node, type="mesh")

        if node_shape:
            node = node_shape[0]
            LOG.info("\tMesh child found, using %r.", node)
        else:
            LOG.error("Mesh child cannot be found for %r.", node)
            return

    if method == 0:
        selectionList = om.MSelectionList()
        selectionList.add(node)
        nodeDagPath = selectionList.getDagPath(0)
        
        mfn = om.MFnMesh(nodeDagPath)
    
        
        vtx_array = mfn.setPoints([om.MFloatPoint(each) for each in vtx_array])




