import face.core as fcore
import face.processing as processing
from ngSkinTools.importExport import LayerData
from ngSkinTools.mllInterface import MllInterface
import numpy as np
from maya import cmds
reload(fcore)
reload(processing)


class Painter(object):
    
    def __init__(self, mesh, map=None, network=None):
        self.mesh = mesh
        self.map = map
        self.name = "default"
        self.network = network
        
        self.grp = None
        
        if not self.network:
            print "Reference network not supplied, making default network from supplied mesh"
            self.network = fcore.Network()
            neutral_shape = self.network.shapes['neutral_100']
        
        if not self.map:
            print "Map not supplied, making default map with L/R split"
            self.map = network.add_map('default')
            self.name = self.map.name
    
    
    def build(self):
        if not self.map:
            self.generate()
        
        self.grp = cmds.createNode("transform", name="{}_map_painter_grp".format(self.name))
        cmds.hide(self.grp)
        self.pos_jnt = cmds.createNode("joint", name="{}_positive".format(self.name), parent=self.grp)
        self.neg_jnt = cmds.createNode("joint", name="{}_negative".format(self.name), parent=self.grp)
        
        self.skincluster = cmds.skinCluster([self.pos_jnt, self.neg_jnt], self.mesh, name="{}_map_painter_skc".format(self.name), maximumInfluences=5)
                
        self.positive_weights = self.map.map_array.tolist()
        self.negative_weights = processing.invert_map_array(self.map.map_array).tolist()
    
        mll = MllInterface()
        mll.setCurrentMesh(self.mesh)
        mll.initLayers()
        self.layer = mll.createLayer('Map weights')
    
        mll.setInfluenceWeights(self.layer, 0, self.positive_weights)
        mll.setInfluenceWeights(self.layer, 1, self.negative_weights)
        
        self.map.painter = self
    
    
    def generate(self, width=1, offset=0.0, axis=0):
        self.map.map_array = processing.generate_soft_split_map(network.shapes["neutral_100"].vtx_array, width=width, offset=offset, axis=axis)


    def update_painter(self): 
        self.positive_weights = self.map.map_array.tolist()
        self.negative_weights = processing.invert_map_array(self.map.map_array).tolist()
    
        mll = MllInterface()
        mll.setCurrentMesh(mesh)
        
        mll.setInfluenceWeights(self.layer,0, self.positive_weights)
        mll.setInfluenceWeights(self.layer,1, self.negative_weights)


    def update_map(self):
        data = LayerData()
        data.loadFrom(self.mesh)
        
        layers = data.layers
        weights = layers[0].influences[0].weights
        map_array = np.array(weights)
        
        self.map.map_array = map_array
        
        
    def delete_painter(self):
        if cmds.objExists(self.grp):
            cmds.delete(self.grp)
        
        if cmds.objExists(self.skincluster[0]):
            cmds.delete(self.skincluster[0])
        
        self.map.painter = None
        
        