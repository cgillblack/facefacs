"""Module docstring.

:author: Chris Gill

"""
import logging

from maya import cmds
import maya.api.OpenMaya as om
from fdev.utils import mesh
import numpy as np

reload(mesh)

LOG = logging.getLogger(__name__)
LOG.setLevel(40)


# Method for finding closest UV value on mesh and attaching anchor point.
def make_point_on_mesh(obj1, obj2, name, maintain_offset=False):

    '''

    CHANGE NODE METHOD FOR API

    # '''
    
    point = cmds.xform(obj2, t=True, ws=True, q=True)
    
    closestUV_point = mesh.get_closest_uv_on_mesh(obj1, point)

    closestUV = [closestUV_point[0], closestUV_point[1]]

    # Create Null to attach to surface.
    anchorPoint = cmds.createNode("transform", n="{}_polyAnchor".format(name))

    # Create constraint between input object and mesh.
    polyConstraint = cmds.pointOnPolyConstraint(obj1,anchorPoint, maintainOffset=maintain_offset)
    
    # Set constraint UV value to closest UV value stored previously.
    cmds.setAttr( '{}.{}U0'.format(polyConstraint[0],obj1), closestUV[0] )

    cmds.setAttr('{}.{}V0'.format(polyConstraint[0],obj1), closestUV[1] )
    
    if maintain_offset:
        offset = cmds.createNode("transform", n="{}_polyAnchor_offset".format(name))
        cmds.parent(offset, anchorPoint)
        cmds.xform(offset, m=cmds.xform(obj2, m=True, ws=True, q=True), ws=True)
        return offset, polyConstraint[0]
    
    else:
        return anchorPoint, polyConstraint[0]




def get_transform(node, world_space=True, normalize=True):
    """Get a numpy array of the transform matrix of the node.

    Args:
        node (str): Provided node for which to extract transform matrix.
        world_space (bool): Get transform matrix in world_space if True or local if False.
        normalize (bool): Normalize scale of transform matrix if True.

    Returns:
        npArray: 4,4 Numpy array of object transform matrix.
    """
    
    objectType = cmds.objectType(node)
    
    if not objectType == "transform" and not objectType == "joint":
        LOG.error("%s is not of type 'transform' or 'joint'", node)
        return
    
    transform_matrix = np.array(
        cmds.xform(
            node,
            matrix=True,
            query=True,
            worldSpace=world_space,
        )
    )
    
    transform_matrix = transform_matrix.reshape(4,4)
    
    if normalize:
        transform_matrix[0] = np.append(normalize_vector(transform_matrix[0][:3]), 0)
        transform_matrix[1] = np.append(normalize_vector(transform_matrix[1][:3]), 0)
        transform_matrix[2] = np.append(normalize_vector(transform_matrix[2][:3]), 0)
    
    return transform_matrix



def normalize_vector(v):
    norm = np.linalg.norm(v)
    if norm == 0: 
       return v
    return v / norm


