"""Module docstring.

:author: Chris Gill

"""
import glob
import logging
import numpy as np
import copy
from itertools import combinations

from face.utils import names
reload(names)

# from pylint.extensions._check_docs_utils import get_setters_property

from face import processing
reload(processing)


# logging.basicConfig(format='[%(filename)s:%(lineno)d] %(message)s')


LOG = logging.getLogger(__name__)
LOG.setLevel(40)


class Shape(object):
    """Object containing vertex position data for given blendshape target.

    Default tag is "core"

    Attributes:
        name (str): Name of blendshape.
        vtx_array (npArray): npArray of vertex positions before extraction or splitting.
        split_vtx_arrays (dict): dictionary containing npArrays of vertex positions after splitting.
        tags (list): List of key words to describe the shape, for example ["driver", "inbetween"]
        extracted (bool): Has shape been extracted.
        split (bool): Has shape been split.

    """

    def __init__(self, name, vtx_array=None, tags=['core', 100], combination_of=None, neutral_shape=None, network=None):
        """Class __init__."""
        
        self.network = network
        self.name = name
        # Mesh numpy arrays
        self._vtx_array = None
        self.vtx_array_set = False
        
        self.slice_array = None

        self.split_maps = []
        self.split_from = None

        # driver, combo, corrective, inbetween
        self.tags = tags

        self.combination_of = combination_of
                
        self.inbetweens = {}
        self.inbetween_for = None
        self.percentage = 100
        
        self.correctives = []
        
        self.relative = True
        
        self._neutral_shape = None
                
        if neutral_shape:
            self.neutral_shape = neutral_shape
            
        if vtx_array:
            self.vtx_array = vtx_array
            
    @property
    def vtx_array(self):
        return self._vtx_array
     
    @vtx_array.setter
    def vtx_array(self, value):
        if not isinstance(value, np.ndarray):
            LOG.error("vtx_array setter: Value provided is not of type 'numpy.array'")
            #Should raise error
            return
          
        else:
            self._vtx_array = value
            self.vtx_array_set = True

    @property
    def neutral_shape(self):
        return self._neutral_shape
     
    @neutral_shape.setter
    def neutral_shape(self, value):
        if not isinstance(value, Shape):
            LOG.error("Neutral_shape setter: Value is of type '%s', should be 'Shape'", type(value))
            #Should raise error
            return
         
        else:
            self._neutral_shape = value
            
            if self._vtx_array == None:
                if isinstance(self._neutral_shape.vtx_array, np.ndarray):
                    self.vtx_array = np.zeros((len(self._neutral_shape.vtx_array), 4))
            

    def compress(self):
        compressed = processing.compress_vtx_array(self.vtx_array)
        self.vtx_array = compressed[0]
        self.slice_array = compressed[1]



        




class Network(object):
    """Contain collective information about a set of blendshape targets.

    Example1:
        import time
        import datetime
        
        from maya import mel
        
        from face import io
        import fdev.utils.model as mod
        import fdev.utils.painter as ptr
        import fdev.utils.blendshape as blendshape
        import face.core as fcore
        import face.processing as processing
        
        reload(io)
        reload(fcore)
        reload(ptr)
        reload(mod)
        reload(blendshape)
        reload(processing)
        
        
        #Network
        network = fcore.Network()
        neutral_shape = network.shapes['neutral_100']
        neutral_shape.vtx_array = mod.get_vtx_array_from_maya_node(neutral_shape, 'test:neutral_100', subtract_neutral=False, method=0, network=network)
        
        #Maps
        softX = network.add_map('softX', prefixes=["r","l"])
        softX.map_array = processing.generate_soft_split_map(network.shapes["neutral_100"].vtx_array, width=1, axis=0)
        
        softEyeUpperLower = network.add_map('softEyeUpperLower', prefixes=["u","d"])
        softEyeUpperLower.map_array = processing.generate_soft_split_map(network.shapes["neutral_100"].vtx_array, width=1, axis=0)
        
        softMouthUpperLower = network.add_map('softMouthUpperLower', prefixes=["u","d"])
        softMouthUpperLower.map_array = processing.generate_soft_split_map(network.shapes["neutral_100"].vtx_array, width=1, axis=0)
        
        #Painter
        painter = ptr.Painter('test:neutral_100', network.maps["softEyeUpperLower"], network)
        painter.build()
        painter.update_map()
        painter.delete_painter()
        
        #Shapes    
        lipStretcher_100 = network.add_shape('lipStretcher_100')
        lipStretcher_100.split_maps.append(softX)
        lipStretcher_100.vtx_array = mod.get_vtx_array_from_maya_node(
                                            lipStretcher_100,
                                            'test:lipStretcher_100',
                                            subtract_neutral=True,
                                            method=0,
                                            network=network
                                            )
                                            
        eyeClose_100 = network.add_shape('eyeClose_100')
        eyeClose_100.split_maps.append(softX)
        eyeClose_100.split_maps.append(softEyeUpperLower)
        eyeClose_100.vtx_array = mod.get_vtx_array_from_maya_node(
                                            eyeClose_100,
                                            'test:eyeClose_100',
                                            subtract_neutral=True,
                                            method=0,
                                            network=network
                                            )
        
        eyeSquint_100 = network.add_shape('eyeSquint_100')
        eyeSquint_100.split_maps.append(softX)
        eyeSquint_100.split_maps.append(softEyeUpperLower)
        eyeSquint_100.vtx_array = mod.get_vtx_array_from_maya_node(
                                            eyeSquint_100,
                                            'test:eyeSquint_100',
                                            subtract_neutral=True,
                                            method=0,
                                            network=network
                                            )
        
        #Combos
        eyeClose_100_eyeSquint_100_100 = network.add_combination_shape([eyeClose_100, eyeSquint_100])
        eyeClose_100_eyeSquint_100_100.vtx_array = mod.get_vtx_array_from_maya_node(
                                            eyeClose_100_eyeSquint_100_100,
                                            'test:eyeClose_100_eyeSquint_100_100',
                                            subtract_neutral=True,
                                            method=0,
                                            network=network
                                            )
        #Save
        directory = "C:/face/tmp/"
        file_name = 'hyena_v005'
        filepath = '{}/{}.p'.format(directory, file_name)
        
        io.save_pickle(filepath, network, compress=True)
        
        network2 = io.open_network(filepath)
        
        print network2.shapes["eyeClose_100"].tags
        
        '''
        network.shapes["eyeClose_100"].split_maps.append(network.maps["softX"])
        network.shapes["eyeSquint_100"].split_maps.append(network.maps["softX"])
        network.shapes["lipStretcher_100"].split_maps.append(network.maps["softX"])
        
        network.shapes["eyeClose_100"].split_maps.append(network.maps["softEyeUpperLower"])
        network.shapes["eyeSquint_100"].split_maps.append(network.maps["softEyeUpperLower"])
        '''
        
        #Build
        split_network = network2.split_shapes()
        
        bs_deformer = blendshape.create_blendshape_from_network(split_network, 'result', name='blendShape')
        
        blendshape.create_combo_drivers(split_network, bs_deformer)
        
        mel.eval("flushUndo")



 

    Attributes:
        shapes (dict): Dictionary of shape objects contained
            in this network. {name:shape}

    """

    def __init__(self):
        """Class __init__."""
        self.name = "default"
        self.shapes = {}
        self.maps = {}
        self.controls = {}
        self.correctives = {}
        
#         self.neutral_shape = 
        self.shapes['neutral_100'] = Shape("neutral_100", network=self)#self.neutral_shape
        self.shapes['neutral_100'].neutral_shape = self.shapes['neutral_100']

    def add_shape(self, name, tags=["core", "100"]):
        """Add shape to network.

        Args:
            name (str): Name for new shape.
            tags (list): List of tags to add by default. core/combo/inbetween.
        """
        
        if not "neutral_100" in self.shapes.keys():
            LOG.error("add_shape: Network has no neutral_100 shape.")
            return
        
        if 'core' in tags:
            if '_' in name:
                nme, _, percent = name.rpartition("_")
                if percent == "100":
                    name = '{}_{}'.format(names.camelise(nme), percent)
                else:
                    LOG.error("Name contains underscore that is not _100")
                    return
            else:
                name = "{}_100".format(names.camelise(name))
                 
        elif 'combo' in tags:
            name = name
        
        shape = Shape(name, network=self)
        shape.neutral_shape = self.shapes["neutral_100"]
        shape.tags = tags
        self.shapes[name] = shape
        
        return shape


    def add_combination_shape(self, shapes):
        """Add, set and name shape based on combinations.
    
        Args:
            shapes (list): Shapes for which to make a combination.
        """
        if not isinstance(shapes, list):
            shapes = [shapes]
        
        core_shapes = []

        for shp in shapes:   
            if "combo" in shp.tags:
                core_shapes.extend(shp.combination_of)
            else:
                core_shapes.append(shp)
        
        core_shapes = list(set(core_shapes))

        name = self.generate_combo_name(core_shapes)
        
        shape = Shape(name, network=self)
        shape.neutral_shape = self.shapes["neutral_100"]
        shape.combination_of = core_shapes
        shape.tags = ["combo", "100"]
        
        
        self.shapes[name] = shape
        
        return shape
    
    def generate_combo_name(self, combination_of):
        if not isinstance(combination_of, list):
            combination_of = [combination_of]
        
        if True in [True for shp in combination_of if "combo" in shp.tags]:
            core_shapes = []
    
            for shp in shapes:   
                if "combo" in shp.tags:
                    core_shapes.extend(shp.combination_of)
                else:
                    core_shapes.append(shp)
                    
            combination_of = core_shapes
            
        combination_of = list(set(combination_of))

        core_names = [s.name for s in combination_of]
        core_names.sort()
        core_names.append("100")
        name = "_".join(core_names)

        return name
    
    def regenerate_combo_name(self, shape):
        new_name = self.generate_combo_name(shape.combination_of)
        
        if new_name == shape.name:
            return
        else:
            self.rename_shape(shape, new_name)
    
    
    def add_inbetween_shape(self, shape, percentage=50):
        """Add inbetween to existing shape.
    
        Untested!

        Args:
            shape (obj): Shape to add inbetween to.
            percentage (int): Point in shape's firing curve to trigger inbetween 100%.
        """
        
        if not isinstance(shape, Shape):
            LOG.warning("add_inbetween_shape: '%s' is not of type 'Shape'.", type(shape))
            return
        
        if not isinstance(percentage, int):
            LOG.warning("add_inbetween_shape: '%s' is not of type 'int'.", type(percentage))
            return
        
        
        name = '{}_{}'.format(shape.name.rpartition('_')[0], percentage)
        
        inbetween_shape = Shape(name, network=self)
        inbetween_shape.neutral_shape = self.shapes['neutral_100']
        inbetween_shape.tags = ['inbetween', percentage]
        inbetween_shape.percentage = percentage
        inbetween_shape.inbetween_for = shape

#         if shape.inbetweens:
        shape.inbetweens[percentage] = inbetween_shape
        
        self.shapes[name] = inbetween_shape
        return inbetween_shape

    def rename_shape(self, shape, name): 
        
        if shape.name in self.shapes.keys():
            shape = self.shapes.pop(shape.name)
        else:
            LOG.error("rename_shape:The given shape is not in this network (Under this name).")
            return
               
        if name in self.shapes.keys():
            return False
        else:
            
            self.shapes[name] = shape
            shape.name = name
            
            for nm, combo in self.get_shapes(["combo"]).iteritems():
                if shape in combo.combination_of:
                    self.regenerate_combo_name(combo)
            
            return True


    def remove_shape(self, shape, dependents=True):
         
        if not shape.name in self.shapes:
            LOG.error("remove_shape:Shape with name '%s' not found in this network.", shape.name)
            return
        
#         if shape == self.shapes[shape.name]:
#             LOG.error("remove_shape:core.Shape object does not match object stored under name '%s'.", shape.name)
#             return
  
        if dependents:
            inbetweens = [shp for shp in shape.inbetweens]
            combos = [shp for shp in self.get_combination_shapes([shape], common=False)]
                
            for inbetween in inbetweens:
                inbetween.inbetween_for.inbetweens.pop(inbetween.name)
                    
                inb = self.shapes.pop(inbetween.name)
                del inb
                    
            for combo in combos:
                comb = self.shapes.pop(combo.name)
                del comb
                
        shp = self.shapes.pop(shape.name)
        del shp


    def remove_map(self, map):
        if not map.name in self.maps:
            LOG.error("remove_map:Map with name '%s' not found in this network.", map.name)
            return
  
        for shape_name, shape in self.shapes.iteritems():
            if map in shape.split_maps:
                shape.split_maps.remove(map)
                
        mp = self.maps.pop(map.name)
        del mp
         

    def remove_control(self, control):
        if not control.name in self.controls:
            LOG.error("remove_control:Control with name '%s' not found in this network.", control.name)
            return

        ctrl = self.controls.pop(control.name)
        del ctrl

    def remove_corrective(self, corrective):
        if not corrective.name in self.correctives:
            LOG.error("remove_corrective:Corrective with name '%s' not found in this network.", corrective.name)
            return
  
        for shape_name, shape in self.shapes.iteritems():
            if corrective in shape.correctives:
                shape.correctives.remove(corrective)
                
        cr = self.correctives.pop(corrective.name)
        del cr

    
    def add_map(self, name, prefixes=["r","l"]):
        """Add map to network.

        Args:
            name (str): Name for new map.
        """

        map = Map(name, prefixes)
        self.maps[name] = map
        
        return map
    
    def rename_map(self, map, name):
        map = self.maps.pop(map.name)
        if name in self.maps.keys():
            return False
        else:
            self.maps[name] = map
            return True
    
    
    def add_control(self, name, tags=["primary"]):
        """Add control to network.

        Args:
            name (str): Name for new map.
        """

        control = Control(name, tags)
        self.controls[name] = control
        
        return control
    
    
    def rename_control(self, control, name):
        if name in self.controls.keys():
            return False
        else:
            control = self.controls.pop(control.name)
            control.name = name
            self.controls[name] = control
            return True


    def rename_corrective(self, corrective, name):
        if name in self.correctives.keys():
            return False
        else:
            corrective = self.correctives.pop(corrective.name)
            corrective.name = name
            self.correctives[name] = corrective
            return True
        
    
    def add_corrective(self, name, tags=None):
        """Add corrective to network.

        Args:
            name (str): Name for new corrective.
        """

        corrective = Corrective(name, tags=tags)
        self.correctives[name] = corrective
        
        return corrective
    
    
    def get_shapes(self, tags=["core", "combo", "inbetween", "corrective"]):
        """List shapes in this network by tag.
        
        This needs updating, its bad.
        
        Args:
            tags (list): Tags to include in search.

        Returns:
            dict: All shapes matching provided tags. with their name as key.
        """
        if tags != None:
            shapes = {}

            for name, shp in self.shapes.iteritems():
                shp_tags = shp.tags
                
                for tag in tags:
                    if tag in shp_tags:
                        shapes[shp.name] = shp
    
            return shapes
        else:
            return self.shapes
    
    def get_controls(self, tags=["primary", "secondary", "tertiary"]):
        """List controls in this network by tag.
                
        Args:
            tags (list): Tags to include in search.

        Returns:
            dict: All controls matching provided tags. with their name as key.
        """
        if tags != None:
            controls = {}
            
            for name, ctrl in self.controls.iteritems():
                ctrl_tags = ctrl.tags
                
                for tag in tags:
                    if tag in ctrl_tags:
                        controls[ctrl.name] = ctrl
                        
            return controls
        else:
            return self.controls
    
    def get_maps(self, tags=None):
        """List maps in this network by tag.
        
        This needs updating, its bad.
        
        Args:
            tags (list): Tags to include in search.

        Returns:
            dict: All shapes matching provided tags. with their name as key.
        """
        if tags != None:
            maps = {}
                    
            for name, map in self.maps.iteritems():
                map_tags = map.tags
                
                for tag in tags:
                    if tag in map_tags:
                        maps[map.name] = map
    
            return maps
        else:
            return self.maps


    def get_correctives(self, tags=None):
        """List correctives in this network by tag.
        
        This needs updating, its bad.
        
        Args:
            tags (list): Tags to include in search.

        Returns:
            dict: All shapes matching provided tags. with their name as key.
        """
        if tags != None:
            correctives = {}
                    
            for name, corrective in self.correctives.iteritems():
                corrective_tags = corrective.tags
                
                for tag in tags:
                    if tag in map_tags:
                        correctives[corrective.name] = corrective
    
            return correctives
        else:
            return self.correctives
    
    
    def get_combination_core_shapes(self, shape, recursive=True, result=None):
        """Get shapes that this combo shape is a combination of.
        
        Recursive will return only core shapes.
        
        I think the logic of this is a little off, since all combos are combos of core shapes, combos of combos may not exist.
        
        Args:
            recursive (bool): Whether to get combos of combos of combos etc...
        """
 
        if recursive:
            if not result:
                result = []
            
            if shape.combination_of:
                for shp in shape.combination_of:
                    result.append(shp)
                    self.get_combination_core_shapes(shp, True, result)
                
            return result
        
        else:
            return shape.combination_of
        
        
    def get_combination_shapes(self, shapes, common=True):
        """Get combination shapes for listed shapes

        Args:
            shapes (list): List of shapes to search for combinations between.
            
        Returns:
            list: All combination shapes that have all of their combination_of shapes present in shapes list.
        """
        combos = []
        for name, shp in self.get_shapes(["combo"]).iteritems():
            if common:
                if all(s in shapes for s in shp.combination_of):
                    combos.append(shp)
            else:
                for shape in shapes:
                    if shape in shp.combination_of:
                        combos.append(shp)
        
        return combos
    
    
    def make_shape_relative(self, shape, extraction_shapes, neutral_shape=None):
        """Make shape into a shape relative to provided extraction shapes.

        Args:
            shape (obj): Shape object to be processed
            extraction_shapes (bool): Shapes to remove from main shape.
            neutral_shape (bool): If required, neutral shape
                to be subtracted (In the case that the main shape is absolute).

        """        

        if not isinstance(shape, Shape):
            LOG.error("make_shape_relative: '%s' is not of type 'Shape'.", type(shape))
            return

        if not isinstance(extraction_shapes, list):
            extraction_shapes = [extraction_shapes]

        extraction_arrays = [shp.vtx_array for shp in extraction_shapes]
        new_array = processing.subtract_vtx_arrays(shape.vtx_array, extraction_arrays)
        shape.vtx_array = new_array
        shape.relative = True
        
        
    def make_shape_absolute(self, shape, addition_shapes, neutral_shape=None):
        """Make shape into a shape absolute using provided addition shapes.

        Args:
            shape (obj): Shape object to be processed
            addition_shapes (bool): Shapes to add to main shape.
            neutral_shape (bool): If required, neutral shape
                to be subtracted (In the case that the main shape is absolute).

        """        

        if not isinstance(shape, Shape):
            LOG.error("make_shape_absolute: '%s' is not of type 'Shape'.", type(shape))
            return

        if not isinstance(addition_shapes, list):
            addition_shapes = [addition_shapes]
        
        
        addition_arrays = [shp.vtx_array for shp in addition_shapes]
        new_array = processing.add_vtx_arrays(shape.vtx_array, addition_arrays)
        shape.vtx_array = new_array
        shape.relative = True
        
    def apply_map_to_shape(self, shape, map, invert=False):
        """Multiply Map's map_array with Shape's vtx_array.

        Args:
            shape (obj): Shape to be affected.
            map (obj): Map to apply.
        """
        if invert:
            new_array = processing.multiply_map_arrays(shape.vtx_array, processing.invert_map_array(map.map_array), sum=True)
        else:
            new_array = processing.multiply_map_arrays(shape.vtx_array, map.map_array, sum=True)
            
        shape.vtx_array = new_array
    
    
    
    def split_shapes(self):
        """Split all shapes by corresponding maps.

        Maybe swap the return network method to just a tag and filter method.
        
        Returns:
            obj: New network object containing only split shapes, not source shapes.
        """
        
        new_network = Network()
        new_network.shapes["neutral_100"].vtx_array = self.shapes["neutral_100"].vtx_array
        
        split_core_shape_data = self.split_core_shapes(new_network)
        for split_data in split_core_shape_data:
            shp = split_data["shape"]
            split_name = split_data["split_name"]
            maps_to_apply = split_data["maps_to_apply"]

            #Add shape and apply maps, logging parent shape.
            shape = new_network.add_shape(split_name, ["core"])
            shape.vtx_array = copy.copy(shp.vtx_array)
            shape.split_from = shp
            shape.correctives = shp.correctives
            
            for mp in maps_to_apply:#Apply all maps, inverted if required.
                new_network.apply_map_to_shape(shape, mp["map"], invert=mp["invert"])
            
        
        split_inbetween_shape_data = self.split_inbetween_shapes(new_network)
        for split_data in split_inbetween_shape_data:
            shp = split_data["shape"]
            split_name = split_data["split_name"]
            core_split_name = split_data["core_split_name"]
            maps_to_apply = split_data["maps_to_apply"]
 
            #Add shape and apply maps, logging parent shape.
            shape = new_network.add_inbetween_shape(new_network.shapes[core_split_name], shp.percentage)
            shape.vtx_array = copy.copy(shp.vtx_array)
            shape.split_from = shp
            shape.correctives = shp.correctives
            
            for mp in maps_to_apply:#Apply all maps, inverted if required.
                new_network.apply_map_to_shape(shape, mp["map"], invert=mp["invert"])
        
        split_combo_shape_data = self.get_split_combo_shapes_data(new_network)
        for split_data in split_combo_shape_data:
            shp = split_data["shape"]
            combination_of = split_data["combination_of"]
            maps_to_apply = split_data["maps_to_apply"]
            
            #Add shape and apply maps, logging parent shape.
            shape = new_network.add_combination_shape(combination_of)
            shape.vtx_array = copy.copy(shp.vtx_array)
            shape.split_from = shp
            shape.correctives = shp.correctives

    
            for mp in maps_to_apply:#Apply all maps, inverted if required.
                new_network.apply_map_to_shape(shape, mp["map"], invert=mp["invert"])

# 
#         split_combo_inbetween_shape_data = self.get_split_combo_inbetween_shapes_data(new_network)
#         for split_data in split_combo_inbetween_shape_data:
#             shp = split_data["shape"]
#             split_name = split_data["split_name"]
#             combo_split_name = split_data["combo_split_name"]
#             maps_to_apply = split_data["maps_to_apply"]
#  
#             #Add shape and apply maps, logging parent shape.
#             shape = new_network.add_inbetween_shape(new_network.shapes[combo_split_name], shp.percentage)
#             shape.combination_of = new_network.shapes[combo_split_name].combination_of
#             shape.vtx_array = copy.copy(shp.vtx_array)
#             shape.split_from = shp
#             
#             for mp in maps_to_apply:#Apply all maps, inverted if required.
#                 new_network.apply_map_to_shape(shape, mp["map"], invert=mp["invert"])


        return new_network            


    def split_core_shapes(self, network):
        """Add docstring you fool
        
        
        
        """    
        split_shape_data = []
        for name, shp in self.get_shapes(tags=["core"]).iteritems():
            
            #Get all split maps associated with this shape.
            maps = []

            for mp in shp.split_maps:
                if not mp in maps:
                    maps.append(mp)
            
            #Dictionary of each prefix and the corresponding map and indicator as to whether the map is inverted
            comb = {}
            for mp in maps:
                for side in range(2):
                    comb[mp.prefixes[side]] = {"map":mp, "invert":side}
            
            #Loop through all combinations that are the length of the number of maps. 2 maps = [r,l][l,r][l,l][r,r]
            for c in combinations(comb, len(maps)):
                
                c = list(c)
                c.sort()#Maybe introduce a custom sorting method. lr before du etc...?
                
                #Skip if combination has appeared before in a different order.
                if c in [sorted(comb[prefix]["map"].prefixes) for prefix in c]:
                    continue
                
                #Build new shape name.
                if c:
                    split_name = "{}{}{}".format("".join(c), name[0].upper(), name[1:])
                else:
                    split_name = name
                
                maps_to_apply = [comb[prefix] for prefix in c]
                split_shape_data.append({"shape":shp, "split_name":split_name, "maps_to_apply":maps_to_apply})
                

        return split_shape_data
                

    def split_inbetween_shapes(self, network):
        """Add docstring you fool
        
        
        
        """    
        split_shape_data = []
        for name, shp in self.get_shapes(tags=["core"]).iteritems():
            
            #Get all split maps associated with this shape.
            maps = []

            for mp in shp.split_maps:
                if not mp in maps:
                    maps.append(mp)
            
            #Dictionary of each prefix and the corresponding map and indicator as to whether the map is inverted
            comb = {}
            for mp in maps:
                for side in range(2):
                    comb[mp.prefixes[side]] = {"map":mp, "invert":side}
            
            #Loop through all combinations that are the length of the number of maps. 2 maps = [r,l][l,r][l,l][r,r]
            for c in combinations(comb, len(maps)):
                
                c = list(c)
                c.sort()#Maybe introduce a custom sorting method. lr before du etc...?
                
                #Skip if combination has appeared before in a different order.
                if c in [sorted(comb[prefix]["map"].prefixes) for prefix in c]:
                    continue
                
                #Build split core shape name.
                if c:
                    core_split_name = "{}{}{}".format("".join(c), name[0].upper(), name[1:])
                else:
                    core_split_name = name
                
                maps_to_apply = [comb[prefix] for prefix in c]
                
                #Inbetweens
                for percentage, inbetween in shp.inbetweens.iteritems():
                    if c:
                        inbetween_split_name = "{}{}{}".format("".join(c), inbetween.name[0].upper(), inbetween.name[1:])
                    else:
                        inbetween_split_name = name
                    
                    split_shape_data.append({"shape":inbetween, "split_name":inbetween_split_name, "maps_to_apply":maps_to_apply, "core_split_name":core_split_name})
                

        return split_shape_data


    def get_split_combo_shapes_data(self, network):
        """Add docstring you fool
        
        
        
        """
        # Nearly working but not quite
        
        split_shape_data = []
        
        #Loop all combo shapes.
        for name, shp in self.get_shapes(tags=["combo"]).iteritems():
 
            maps = []
            core_combinations = {}
            #Loop all the core shapes that make up current combo
            for com in shp.combination_of:
                
                #Collect all maps of all the core shapes.
                core_maps = com.split_maps
                maps.extend(com.split_maps)

                #Dictionary of each prefix for each map and the corresponding Map object and indicator as to whether the map is inverted
                comb = {}
                for mp in core_maps:
                    for side in range(2):
                        comb[mp.prefixes[side]] = {"map":mp, "invert":side}
                         
                for c in combinations(comb, len(core_maps)):
                     
                    c = list(c)
                    c.sort()#Maybe introduce a custom sorting method. lr before du etc...?
                    
                    #Skip if combination has appeared before in a different order.
                    if c in [sorted(comb[prefix]["map"].prefixes) for prefix in c]:
                        continue
                    
                    #Build new shape names and list dicts containing the maps that need to be applied.
                    if not c:
                        core_combinations[com.name] = []
                    else:
                        core_combinations["{}{}{}".format("".join(c), com.name[0].upper(), com.name[1:])] = [comb[i] for i in c]
                    
            #Loop through all combinations that are the length of the number of maps. 2 maps = [r,l][l,r][l,l][r,r]
            for core_shape_combinations in combinations(core_combinations.keys(), len(shp.combination_of)):
                core_shape_combinations = list(core_shape_combinations)
                core_shape_combinations.sort()
                combination_of = []
                maps_to_apply = []
                
                #Collect Core shapes to combine and maps to apply.
                for split in core_shape_combinations:
                    combination_of.append(network.shapes[split])
                    maps_to_apply.extend(core_combinations[split])

                #Work out if proposed combination is made of splits of the same shape, if so then skip it. eg [ru, lu]
                sources = [co.split_from for co in combination_of]
                if len(sources) > len(set(sources)):
                    continue
                
                split_shape_data.append({"shape":shp, "combination_of":combination_of, "maps_to_apply":maps_to_apply})

        return split_shape_data

    def apply_correctives(self, return_new_network=False):
        """Apply corrective transformations to all valid shapes.
        
        Returns:
            obj: New network object containing only transformed shapes, not source shapes.
        """
        
        for name, shape in self.shapes.iteritems():
            for cor in shape.correctives:
                shape.vtx_array = processing.apply_corrective(shape.vtx_array, cor, self)


# 
#     def get_split_combo_inbetween_shapes_data(self, network):
#         """Add docstring you fool
#         
#         
#         
#         """
#         # Nearly working but not quite
#         
#         split_shape_data = []
#         
#         #Loop all combo shapes.
#         for name, shp in self.get_shapes(tags=["combo"]).iteritems():
#  
#             maps = []
#             core_combinations = {}
#             #Loop all the core shapes that make up current combo
#             for com in shp.combination_of:
#                 
#                 #Collect all maps of all the core shapes.
#                 core_maps = com.split_maps
#                 maps.extend(com.split_maps)
# 
#                 #Dictionary of each prefix for each map and the corresponding Map object and indicator as to whether the map is inverted
#                 comb = {}
#                 for mp in core_maps:
#                     for side in range(2):
#                         comb[mp.prefixes[side]] = {"map":mp, "invert":side}
#                          
#                 for c in combinations(comb, len(core_maps)):
#                      
#                     c = list(c)
#                     c.sort()#Maybe introduce a custom sorting method. lr before du etc...?
#                     
#                     #Skip if combination has appeared before in a different order.
#                     if c in [sorted(comb[prefix]["map"].prefixes) for prefix in c]:
#                         continue
#                     
#                     #Build new shape names and list dicts containing the maps that need to be applied.
#                     if not c:
#                         core_combinations[com.name] = []
#                     else:
#                         core_combinations["{}{}{}".format("".join(c), com.name[0].upper(), com.name[1:])] = [comb[i] for i in c]
#                     
#             #Loop through all combinations that are the length of the number of maps. 2 maps = [r,l][l,r][l,l][r,r]
#             for core_shape_combinations in combinations(core_combinations.keys(), len(shp.combination_of)):
#                 core_shape_combinations = list(core_shape_combinations)
#                 core_shape_combinations.sort()
#                 combination_of = []
#                 maps_to_apply = []
#                 
#                 #Collect Core shapes to combine and maps to apply.
#                 for split in core_shape_combinations:
#                     combination_of.append(network.shapes[split])
#                     maps_to_apply.extend(core_combinations[split])
# 
#                 #Work out if proposed combination is made of splits of the same shape, if so then skip it. eg [ru, lu]
#                 sources = [co.split_from for co in combination_of]
#                 if len(sources) > len(set(sources)):
#                     continue
#                 
# #                 split_shape_data.append({"shape":shp, "combination_of":combination_of, "maps_to_apply":maps_to_apply})
# 
#                 combo_split_name = self.generate_combo_name(combination_of)
# 
#                 #Inbetweens
#                 for percentage, inbetween in shp.inbetweens.iteritems():
#                     if c:
#                         inbetween_split_name = "{}{}{}".format("".join(c), inbetween.name[0].upper(), inbetween.name[1:])
#                     else:
#                         inbetween_split_name = name
#                     
#                     split_shape_data.append({"shape":inbetween, "split_name":inbetween_split_name, "maps_to_apply":maps_to_apply, "combo_split_name":combo_split_name})
# 
# 
# 
#         return split_shape_data



class Map(object):
    """Object containing map values per vertex for splitting/combining shapes.

    Attributes:
        name (str): Name of map.
        vtx_array (npArray): npArray of map values.
        tags (list): List of key words to describe the map, for example ["brow", "lip"]
        prefixes (string): Prefix to differentiate between positive and negative shapes after split.
    """

    def __init__(self, name, prefixes=["r","l"], tags=[]):
        """Class __init__."""
        self.name = name
        self.prefixes = prefixes
        self.tags = tags
        
        self.painter = None
        
        # Map numpy arrays
        self._map_array = None
        self.map_array_set = False
        
            
    @property
    def map_array(self):
        return self._map_array
     
    @map_array.setter
    def map_array(self, value):
        if not isinstance(value, np.ndarray):
            LOG.error("map_array setter: Value provided is not of type 'numpy.array'")
            #Should raise error
            return
          
        else:
            self._map_array = value
            self.map_array_set = True

    def get_map_array_from_maya_attr(self, node, attr):
        """Fill map_array with data extracted from a maya attr.

        Args:
            node (str): Node to get attr data from.
            attr (str): Name of attribute.
        """

        from fdev.utils import attr as attribute
        reload(attribute)
        
        self.map_array = attribute.get_map_array(node, attr)
        



class Control(object):
    """Object containing generic data defining a control object.

    Attributes:
        name (str): Name of control.
        transform_matrix (npArray): transform matrix describing position and orientation of the control (normlized).
        tags (list): List of key words to describe the map, for example ["brow", "lip"]
    """

    def __init__(self, name, tags=None):
        """Class __init__."""
        self.name = name
        self.tags = tags
        
        self.mirror = False
        self.mirror_prefixes = ["r", "l"]
        self.mirror_axis = 0
        
        self.attach_to_mesh = False
        
        self.size = 0.2
        
        self._transform_matrix = np.array(
                                        [1.0, 0.0, 0.0, 1.0,
                                         0.0, 1.0, 0.0, 1.0,
                                         0.0, 0.0, 1.0, 1.0,
                                         0.0, 0.0, 0.0, 1.0]
                                        ).reshape(4,4)
        
        self.driven_shapes = {
            "tx":None,
            "ty":None,
            "tz":None,
            "-tx":None,
            "-ty":None,
            "-tz":None
            }
        
        self.enabled_axes = {
            "tx":False,
            "ty":False,
            "tz":False,
            "-tx":False,
            "-ty":False,
            "-tz":False
            }
        
            
    @property
    def transform_matrix(self):
        return self._transform_matrix
     
    @transform_matrix.setter
    def transform_matrix(self, value):
        if not isinstance(value, np.ndarray):
            LOG.error("transform_matrix setter: Value provided is not of type 'numpy.array'")
            #Should raise error
            return
          
        else:
            self._transform_matrix = value

    
    def add_driven_shape(self, shape, axis="y", attribute="t", negative=False):
        """
 
        Args:
            shape (obj): Shape object containing data.
            axis (str): Axis (x,y,z) which should be used to trigger shape plus the direction (+,-).
        """
         
        if not isinstance(shape, Shape):
            LOG.error("add_driven_shape: Shape provided is not of type 'core.Shape'")
            #Should raise error
            return
                 
        if negative:
            direction = "-"
        else:
            direction = "+"
             
        if direction + attribute + axis in self.driven_shapes.keys():
            self.driven_shapes[direction + attribute + axis] = shape
    
    def get_transform_from_maya_node(self, node):
        """Fill map_array with data extracted from a maya attr.

        Args:
            node (str): Node to get transform data from.
        """

        from fdev.utils import control
        reload(control)
        
        self.transform_matrix = control.get_transform_from_maya_node(self, node)
        




class Corrective(object):
    """Object containing generic data defining a control object.

    Attributes:
        name (str): Name of control.
        transform_matrix (npArray): transform matrix describing position and orientation of the control (normlized).
        tags (list): List of key words to describe the map, for example ["brow", "lip"]
    """

    def __init__(self, name, tags=None):
        """Class __init__."""
        self.name = name
        self.tags = tags
        
        self._start_matrix = np.array(
                                        [1.0, 0.0, 0.0, 1.0,
                                         0.0, 1.0, 0.0, 1.0,
                                         0.0, 0.0, 1.0, 1.0,
                                         0.0, 0.0, 0.0, 1.0]
                                        ).reshape(4,4)

        self._end_matrix = np.array(
                                        [1.0, 0.0, 0.0, 1.0,
                                         0.0, 1.0, 0.0, 1.0,
                                         0.0, 0.0, 1.0, 1.0,
                                         0.0, 0.0, 0.0, 1.0]
                                        ).reshape(4,4)
        
    
    
        # Map numpy arrays
        self._weight_array = None
        self.weight_array_set = False
            
    @property
    def start_matrix(self):
        return self._start_matrix
     
    @start_matrix.setter
    def start_matrix(self, value):
        if not isinstance(value, np.ndarray):
            LOG.error("transform_matrix setter: Value provided is not of type 'numpy.array'")
            #Should raise error
            return
          
        else:
            self._start_matrix = value

    @property
    def end_matrix(self):
        return self._end_matrix
     
    @end_matrix.setter
    def end_matrix(self, value):
        if not isinstance(value, np.ndarray):
            LOG.error("transform_matrix setter: Value provided is not of type 'numpy.array'")
            #Should raise error
            return
          
        else:
            self._end_matrix = value

    @property
    def weight_array(self):
        return self._weight_array
     
    @weight_array.setter
    def weight_array(self, value):
        if not isinstance(value, np.ndarray):
            LOG.error("transform_matrix setter: Value provided is not of type 'numpy.array'")
            #Should raise error
            return
          
        else:
            self._weight_array = value




