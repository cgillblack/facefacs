"""Module containing functions for saving and loading networks.
 
:author: Chris Gill

TO DO:
Add struct as a saving format option (also benchmark it).


"""
 
import json
import cPickle as pickle
try:
    import msgpack
except:
    msgpack = None
    
import numpy as np
from face import core, processing
reload(core)
reload(processing)
 
 
def open_network(filepath):
    """Create network from file contents.
  
    Args:
        filepath (str): File location to open.
  
    Returns:
        obj: Created network.
    """
      
    filepath.replace("\\", "/")
    directory, _,file = filepath.rpartition("/")
    file, _, ext = file.rpartition(".")
  
    network = core.Network()
      
    if ext == "json":
        load_json(filepath, network)
    elif ext == "p":
        load_pickle(filepath, network)
    elif ext == "msgpack":
        load_msgpack(filepath, network)
      
    return network
      
      
def save_network(filepath, network, compress=False):
    """Save network to file.
  
    Args:
        network (object): Network to save.
        filepath (str): File location to save to.
  
    Returns:
        str: Path to saved file.
    """
      
    filepath.replace("\\", "/")
    directory, _,file = filepath.rpartition("/")
    file, _, ext = file.rpartition(".")
        
    if ext == "json":
        save_json(filepath, network, compress=compress)
    elif ext == "p":
        save_pickle(filepath, network)
    elif ext == "msgpack":
        save_msgpack(filepath, network)
      
    return network
      
  
def save_json(filepath, network, compress=False):
    """Save Json file of network, containing Shape data and Map data.
  
    Args:
        filepath (str): File location to save to.
        network (obj): face.core.Network object containing metadata and Shape information.
    """
  
    data = {"shapes":{}, "maps":{}}
    for name, shape in network.shapes.iteritems():
        if compress:
            vtx_array, slice_array = processing.compress_vtx_array(shape.vtx_array)
            data["shapes"][name] = {"vtx_array":vtx_array.tolist(), "slice_array":slice_array.tolist(), "split_maps":[map.name for map in shape]}
        else:
            vtx_array = shape.vtx_array
            data["shapes"][name] = {"vtx_array":vtx_array.tolist(), "split_maps":[map.name for map in shape]}
  
    for name, map in network.maps.iteritems():
        if map.map_array:
            map_array = map.map_array
            data["maps"][name] = {"map_array":map_array.tolist()}
        else:
            data["maps"][name] = {"map_array":[]}
      
    encoded = json.dumps(data)#, indent=4,
#                          sort_keys=True, separators=(",", ":"))
      
    with open(filepath, "w") as f:
        json.dump(encoded, f)#, indent=4, sort_keys=True,
#                   separators=(",", ":"))
      
      
def load_json(filepath, network):
    """Load Json file in existing network.
  
    Args:
        filepath (str): File location to load from.
        network (obj): face.core.Netwrok object containing metadata and Shape information.
  
    """
    with open(filepath, "r") as f:
        loaded = json.load(f)
        data = json.loads(loaded)
      
    shapes = data["shapes"]
    maps = data["maps"]
      
      
      
    for name, vtx_array in shapes.iteritems():
        shape = network.add_shape(name)
        if 'slice_array' in vtx_array.keys():
            shape.vtx_array = processing.decompress_vtx_array(np.array(vtx_array["vtx_array"]), np.array(vtx_array["slice_array"]))
        else:
            shape.vtx_array = np.array(vtx_array["vtx_array"])
      
    for name, map_array in maps.iteritems():
        map = network.add_map(name)
          
        map.map_array = np.array(map_array["map_array"])
  
    return network    
 
def encode_data(network, compress=False):
    """Fills dictionary with data extracted from a network.
  
    Args:
        network (object): Network to encode.
        compress (bool): Whether to run the remove zeros compression (Warning, very slow).
    Returns:
        dict: Dictionary containing all data required to decode full network.
    """
    data = {"shapes":{}, "maps":{}, "controls":{}, "correctives":{}, "name":network.name}
    
    for name, shape in network.shapes.iteritems():
        if compress:
            vtx_array, slice_array = processing.compress_vtx_array(shape.vtx_array)
            data["shapes"][name] = {"vtx_array":vtx_array.tolist(), "slice_array":slice_array.tolist()}
        else:
            vtx_array = shape.vtx_array
            data["shapes"][name] = {"vtx_array":vtx_array.tolist()}
        
        if shape.combination_of:
            data["shapes"][name]['combination_of'] = [shp.name for shp in shape.combination_of]
            
        data["shapes"][name]['tags'] = shape.tags
        data["shapes"][name]['split_maps'] = [map.name for map in shape.split_maps]
        data["shapes"][name]['percentage'] = shape.percentage
        data["shapes"][name]['inbetweens'] = [inb.name for percentage, inb in shape.inbetweens.iteritems()]
        data["shapes"][name]['correctives'] = [cor.name for cor in shape.correctives]
        
        if shape.inbetween_for:
            data["shapes"][name]['inbetween_for'] = shape.inbetween_for.name
            
        
    for name, map in network.maps.iteritems():
        if isinstance(map.map_array, np.ndarray):
            map_array = map.map_array
            data["maps"][name] = {"map_array":map_array.tolist()}
        else:
            data["maps"][name] = {"map_array":[]}
        data["maps"][name]["prefixes"] = map.prefixes
        data["maps"][name]['tags'] = map.tags

    for name, control in network.controls.iteritems():
        if isinstance(control.transform_matrix, np.ndarray):
            transform_matrix = control.transform_matrix
            data["controls"][name] = {"transform_matrix":control.transform_matrix.tolist()}
        else:
            data["controls"][name] = {"transform_matrix":[]}
        
        data["controls"][name]["mirror"] = control.mirror
        data["controls"][name]["mirror_prefixes"] = control.mirror_prefixes
        data["controls"][name]["mirror_axis"] = control.mirror_axis
        data["controls"][name]["driven_shapes"] = control.driven_shapes
        data["controls"][name]["enabled_axes"] = control.enabled_axes
        data["controls"][name]["size"] = control.size
        data["controls"][name]["attach_to_mesh"] = control.attach_to_mesh
        data["controls"][name]['tags'] = control.tags


    for name, corrective in network.correctives.iteritems():
        data["correctives"][name] = {}
        
        if isinstance(corrective.weight_array, np.ndarray):
            weight_array = corrective.weight_array
            data["correctives"][name]["weight_array"] = corrective.weight_array.tolist()
        else:
            data["correctives"][name]["weight_array"] = []

        if isinstance(corrective.start_matrix, np.ndarray):
            start_matrix = corrective.start_matrix
            data["correctives"][name]["start_matrix"] = corrective.start_matrix.tolist()
        else:
            data["correctives"][name]["start_matrix"] = []

        if isinstance(corrective.end_matrix, np.ndarray):
            end_matrix = corrective.end_matrix
            data["correctives"][name]["end_matrix"] = corrective.end_matrix.tolist()
        else:
            data["correctives"][name]["end_matrix"] = []
    
            
        
        data["correctives"][name]['tags'] = corrective.tags

        
    return data

def decode_data(data, network):
    """Fills dictionary with data extracted from a network.
  
    Args:
        data (dict): Dictionary created using encode method to convert back to usable network..
        network (object): Network to encode.
    """

    name = data.get("name", {})
    shapes = data.get("shapes", {})
    maps = data.get("maps", {})
    controls = data.get("controls", {})
    correctives = data.get("correctives", {})

    
    #Add neutral. Must be done first as it is need for subtraction if another shape is added.
    network.shapes['neutral_100'].vtx_array = np.array(shapes["neutral_100"]["vtx_array"])
    network.name = name
    
    #Add non-neutral core shapes.
    for name, data in shapes.iteritems():
        if not name == "neutral_100" and "core" in data.get('tags'):
            shape = network.add_shape(name, tags=data.get('tags'))

    #Add inbetween shapes
    for name, data in shapes.iteritems():
        if "inbetween" in data.get('tags'):
            shape = network.add_shape(name, tags=data.get('tags'))
            core_shape = network.shapes[data.get('inbetween_for')]
            if core_shape:
                shape = network.add_inbetween_shape(core_shape, percentage=int(data.get('percentage')))
            
    #Add combination shapes.
    for name, data in shapes.iteritems():
        if 'combo' in data.get('tags'):
            if 'combination_of' in data.keys():
                combination_of = [network.shapes[shp_name] for shp_name in data['combination_of']]
                combo = network.add_combination_shape(combination_of)
                
    #Add maps.
    for name, data in maps.iteritems():
        map = network.add_map(name)
            
        map.map_array = np.array(data["map_array"])
        if "prefixes" in data.keys():
            map.prefixes = data["prefixes"]
            
    #Add controls
    for name, data in controls.iteritems():
        control = network.add_control(name, tags=data["tags"])
            
        control.transform_matrix = np.array(data["transform_matrix"])
            
        if "mirror" in data.keys():
            control.mirror = data["mirror"]
        if "mirror_prefixes" in data.keys():
            control.mirror_prefixes = data["mirror_prefixes"]
        if "mirror_axis" in data.keys():
            control.mirror_axis = data["mirror_axis"]
        if "driven_shapes" in data.keys():
            control.driven_shapes = data["driven_shapes"]
        if "attach_to_mesh" in data.keys():
            control.attach_to_mesh = data["attach_to_mesh"]
        if "enabled_axes" in data.keys():
            control.enabled_axes = data["enabled_axes"]
        if "size" in data.keys():
            control.size = data["size"]


    #Add correctives
    for name, data in correctives.iteritems():
        corrective = network.add_corrective(name, tags=data["tags"])
        corrective.weight_array = np.array(data["weight_array"])
        
        corrective.start_matrix = np.array(data["start_matrix"])
        corrective.end_matrix = np.array(data["end_matrix"])
    
    for name, shape in network.shapes.iteritems():
        #Set vertex array data for all shapes.
        shape.vtx_array = np.array(shapes[name]["vtx_array"])
        
        #If a slice array is present then the data has been compressed, so decompress it
        if 'slice_array' in shapes[name].keys():
            shape.vtx_array = processing.decompress_vtx_array(shape.vtx_array, np.array(shapes[name]["slice_array"]))
        
        #Assign split maps and correctives
        split_maps = list(set([network.maps[map_name] for map_name in shapes[name]["split_maps"]]))
        shape.split_maps = split_maps

        correctives_list = list(set([network.correctives[corrective_name] for corrective_name in shapes[name]["correctives"]]))
        shape.correctives = correctives_list
    
        

def save_pickle(filepath, network, compress=False):
    """Save cPickle file of network, containing Shape data and Map data.
  
    Args:
        filepath (str): File location to save to.
        network (obj): face.core.Network object containing metadata and Shape information.
    """
    
    data = encode_data(network, compress=compress)
      
    pickle.dump(data, open(filepath, "wb" ))
      
      
def load_pickle(filepath, network):
    """Load cPickle file in existing network.
  
    Args:
        filepath (str): File location to load from.
        network (obj): face.core.Netwrok object containing metadata and Shape information.
  
    """
    data = pickle.load( open( filepath, "rb" ) )
    
    decode_data(data, network)
    
    return network   
 



def save_msgpack(filepath, network, compress=False):
    """Save msgpack file of network, containing Shape data and Map data.
  
    Args:
        filepath (str): File location to save to.
        network (obj): face.core.Network object containing metadata and Shape information.
    """
  
    data = {"shapes":{}, "maps":{}}
    for name, shape in network.shapes.iteritems():
        if compress:
            vtx_array, slice_array = processing.compress_vtx_array(shape.vtx_array)
            data["shapes"][name] = {"vtx_array":vtx_array.tolist(), "slice_array":slice_array.tolist()}
        else:
            vtx_array = shape.vtx_array
            data["shapes"][name] = {"vtx_array":vtx_array.tolist()}
  
    for name, map in network.maps.iteritems():
        if map.map_array:
            map_array = map.map_array
            data["maps"][name] = {"map_array":map_array.tolist()}
        else:
            data["maps"][name] = {"map_array":[]}
      
    with open(filepath, 'w') as outfile:
        msgpack.pack(data, outfile)

      
      
def load_msgpack(filepath, network):
    """Load msgpack file in existing network.
  
    Args:
        filepath (str): File location to load from.
        network (obj): face.core.Netwrok object containing metadata and Shape information.
  
    """
    
    with open(filepath) as data_file:
        data = msgpack.unpack(data_file)
        
    shapes = data["shapes"]
    maps = data["maps"]
      
      
    for name, vtx_array in shapes.iteritems():
        shape = network.add_shape(name)
        if 'slice_array' in vtx_array.keys():
            shape.vtx_array = processing.decompress_vtx_array(np.array(vtx_array["vtx_array"]), np.array(vtx_array["slice_array"]))
        else:
            shape.vtx_array = np.array(vtx_array["vtx_array"])
      
    for name, map_array in maps.iteritems():
        map = network.add_map(name)
          
        map.map_array = np.array(map_array["map_array"])
  
    return network  



