"""Module docstring.

:author: Chris

"""
import numpy as np
import copy
import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)



#These should maybe be combined into "vtx_array_math" or "vtx_array_operation".
def subtract_vtx_arrays(target_array, extraction_arrays):
    """Subtract list of arrays from main array.

    Args:
        target_array (nparray): Array others will be extracted from.
        extraction_arrays (list): [obj,obj,obj] Arrays to extract from main array.
        
    Returns:
        nparray: New vertex array with the result of the operation.
    """
    if not isinstance(target_array, np.ndarray):
        LOG.error("subtract_vtx_array: Type '%s' is not valid, should be numpy array.", type(target_array))
        return

    if not isinstance(extraction_arrays, list):
        extraction_arrays = [extraction_arrays]

    new_array = copy.copy(target_array)
    
    for ex_array in extraction_arrays:
        new_array = new_array - ex_array
    
    
    return new_array



def add_vtx_arrays(target_array, addition_arrays):
    """Subtract list of arrays from main array.

    Args:
        target_array (nparray): Array others will be added to.
        addition_arrays (list): [obj,obj,obj] Arrays to added to main array.
        
    Returns:
        nparray: New vertex array with the result of the operation.
    """
    if not isinstance(target_array, np.ndarray):
        LOG.error("add_vtx_arrays: Type '%s' is not valid, should be numpy array.", type(target_array))
        return

    if not isinstance(addition_arrays, list):
        addition_arrays = [addition_arrays]

    new_array = copy.copy(target_array)
        
    for ex_array in addition_arrays:
        new_array = new_array + ex_array
    
    
    return new_array




def multiply_map_arrays(target_array, multiplication_arrays, sum=True):
    """Subtract list of arrays from main array.

    Args:
        target_array (nparray): Array others will be added to.
        addition_arrays (list): [obj,obj,obj] Arrays to added to main array.
        
    Returns:
        nparray: New vertex array with the result of the operation.
    """
    if not isinstance(target_array, np.ndarray):
        LOG.error("multiply_map_arrays: Type '%s' is not valid, should be numpy array.", type(target_array))
        return

    if not isinstance(multiplication_arrays, list):
        multiplication_arrays = [multiplication_arrays]
        
    new_array = copy.copy(target_array)
    
    if len(np.shape(new_array)) > 1:
        if sum:
            new_array = new_array*np.clip(np.sum(multiplication_arrays, axis=0), 0, 1)[:,np.newaxis]
        else:
            for ex_array in multiplication_arrays:
                new_array = new_array*ex_array[:,np.newaxis]
    else:
        if sum:
            new_array = new_array*np.clip(np.sum(multiplication_arrays, axis=0), 0, 1)
        else:
            for ex_array in multiplication_arrays:
                new_array = new_array*ex_array
    
    
    return new_array
    
def invert_map_array(map_array):
    """Invert the values of a map array (Normalized). 

    Converts 1 to 0, 0 to 1 and float values to 1-value.
    
    Args:
        map_array (nparray): Array to be inverted.
        
    Returns:
        nparray: New map array with the result of the operation.
    """
    
    new_array = copy.copy(map_array)
    
    return 1-new_array
    
def generate_hard_split_map(vtx_array, invert=False, axis=0):
    """Generate binary 1/0 split map along axis at origin from vertex array.

    Currently theres no flex in the center average, x HAS to be 0, maybe should be loosened a litte.

    Args:
        vtx_array (nparray): Vertex array to analyse.
        invert (bool): Switch 1 and 0 in resulting map.
        axis (str): Axis to split along. xyz 012.
        
    Returns:
        nparray: New map array with split data.
    """

    map_array = np.where(vtx_array[:,axis] > 0, invert, not invert)
    #average center verticies
    map_array = np.where(vtx_array[:,axis] == 0, 0.5, map_array)

    
    return map_array

def generate_soft_split_map(vtx_array, width=1.0, invert=False, offset=0.0, axis=0):
    """Generate soft split map along axis at origin from vertex array using sin curve.

    1. Clip the input vertex axis positions to half the width on each side.
    2. Invert x position value if applicable. (0-0.5) = -0.5, *2 = -1, (1-0.5) = 0.5, *2 = 1.
    3. cycling through points divide by half width to remap width to -1/1.
    4. multiply by half pi so sin of an input value of 1.0 = 1.0, instead of sin(1.0) which is 0.841470984808.
    5. Divide by 2 in order to shrink the range of -1/1 into -0.5/0.5.
    6. Then add 0.5 to offset the result up to 0/1
    

    Args:
        vtx_array (nparray): Vertex array to analyse.
        width (float): Width of softness, width*0.5 on either side of the origin.
        invert (bool): Switch 1 and 0 in resulting map.
        axis (str): Axis to split along. xyz 012.
        
    Returns:
        nparray: New map array with split data.
    """
    
    if not isinstance(vtx_array, np.ndarray):
        LOG.error("Type '%s' is not valid, should be numpy array.", type(vtx_array))
        return
    
    width = float(width)
    
    a = np.clip(vtx_array[:,axis]+offset, -(width/2.0), width/2.0)*((invert-0.5)*2)
    map_array = ((np.sin((a/(width/2.0))*(np.pi/2.0))/2.0)+0.5)
        
    return map_array
    
    
def compress_vtx_array(vtx_array):
    """Remove zeros from array and return compressed array and zero split array for rebuild.

    Args:
        vtx_array (nparray): Array to be compressed.
        
    Returns:
        [nparray, nparray]: Array of vertex position with zeros removed and the split coords of removed zeros.
    """
    
    if not isinstance(vtx_array, np.ndarray):
        LOG.error("compress_vtx_array: Type '%s' is not valid, should be numpy array.", type(vtx_array))
        return
    
    vtx_array_sum = np.absolute(vtx_array.sum(axis=1))
    
    zeros = np.where(vtx_array_sum < 0.000001)[0]
    values = np.where(vtx_array_sum > 0.000001)[0]
    
    value_ones = np.ones(len(values))
    
    negative_shift = values - value_ones
    positive_shift = values + value_ones
    
    negative_intersection = np.intersect1d(zeros, negative_shift)
    positive_intersection = np.intersect1d(zeros, positive_shift)
    
    merged = np.concatenate((negative_intersection, positive_intersection), axis=0)
    if vtx_array_sum[0] < 0.000001:
        merged = np.append(merged, 0)
    if vtx_array_sum[-1] < 0.000001:
        merged = np.append(merged, len(vtx_array_sum)-1)
        
    flat_chunks = np.sort(merged)
    chunk_slices_2d = np.reshape(flat_chunks, (len(flat_chunks)/2, 2))
    
    vtx_array_culled = np.delete(vtx_array, zeros, axis=0)
    
    return vtx_array_culled, chunk_slices_2d
    
    
    
def decompress_vtx_array(vtx_array, slice_array):
    """Re-add zeros based on split array

    Args:
        vtx_array (nparray): Array to be decompressed.
        split_map (nparray): Array to be used to insert zeros.
        
    Returns:
        nparray: Vertex array with zeros returned to array, matching original saved input.
    """
    
    if not isinstance(vtx_array, np.ndarray):
        LOG.error("decompress_vtx_array: Type '%s' is not valid, should be numpy array.", type(vtx_array))
        return
    
    if not isinstance(slice_array, np.ndarray):
        LOG.error("decompress_vtx_array: Type '%s' is not valid, should be numpy array.", type(slice_array))
        return
    
    new_vtx_array = copy.copy(vtx_array)
    for slice in slice_array:
        
        num = int(slice[1] - slice[0])+1
        if len(new_vtx_array) > 0:
            new_vtx_array = np.insert(new_vtx_array, int(slice[0]), np.zeros((num,4)), axis=0)
        else:
            new_vtx_array = np.zeros((num,4))
    
    
    return new_vtx_array
    
    

def apply_corrective(vtx_array, corrective, network):
    """Apply weights transform to correct skincluster deformation.

    Args:
        vtx_array (nparray): Array to be decompressed.
        corrective (object): Corrective data to apply (core.Corrective).
        network (object): Network to get neutral from.
        
    Returns:
        nparray: Vertex array with transformed points.
    """
    #Add neutral #Might be unnecessary since the transformed neutral is added later.
    vtx_array = add_vtx_arrays(vtx_array, [network.shapes["neutral_100"].vtx_array])
    
    vtx_array[...,3] = 1
    
    #Convert vertex vetor to transform matrix for transformation
    mat_fill = np.tile(np.array([1,0,0,1,
                            0,1,0,1,
                            0,0,1,1]), (len(vtx_array), 1))
    
    #Get neutral vertex positions
    neutral_vtx_array = network.shapes["neutral_100"].vtx_array
    
    neutral_vtx_mat_array = np.append(mat_fill, neutral_vtx_array, axis=1)
    neutral_vtx_mat_array = neutral_vtx_mat_array.reshape(len(neutral_vtx_array), 4, 4)
    
    #Starting or "neutral" position of corrective transform
    start_matrix = corrective.start_matrix
    
    #Inverse matrix of start to negatate world transformation.
    inverted_start = np.linalg.inv(start_matrix)
    multiplied_end = np.dot(corrective.end_matrix, inverted_start).reshape(1,16)
    
    inverted_multiplied_end = np.linalg.inv(multiplied_end.reshape(4,4))
    
    multiplied_end_matrix_array = np.tile(multiplied_end, (len(vtx_array), 1)).reshape(len(vtx_array), 4, 4)
    inverse_start_matrix_array = np.tile(inverted_start.reshape(1,16), (len(vtx_array), 1)).reshape(len(vtx_array), 4, 4)
    start_matrix_array = np.tile(corrective.start_matrix.reshape(1,16), (len(vtx_array), 1)).reshape(len(vtx_array), 4, 4)
    
    inverted_multiplied_end_rotation = inverted_multiplied_end
    inverted_multiplied_end_rotation[...,3] = np.array([0,0,0,1])
    
    inverse_rotation_matrix_array = np.tile(inverted_multiplied_end_rotation, (len(vtx_array), 1)).reshape(len(vtx_array), 4, 4)
    

    #Multiply and array of matricies by another array of matricies.
    transformed_neutral_vtx_mat_array = np.einsum('lij,ljk->lik', neutral_vtx_mat_array, inverse_start_matrix_array)
    transformed_neutral_vtx_mat_array = np.einsum('lij,ljk->lik', transformed_neutral_vtx_mat_array, multiplied_end_matrix_array)
    transformed_neutral_vtx_mat_array = np.einsum('lij,ljk->lik', transformed_neutral_vtx_mat_array, start_matrix_array)

    #Convert array of matricies to an array of vectors
    transformed_neutral_vtx_array = transformed_neutral_vtx_mat_array[...,3,:]
    #Transform neutral to make skincluster equivalent for subtraction.
    corrective_neutral = add_vtx_arrays(multiply_map_arrays(subtract_vtx_arrays(transformed_neutral_vtx_array, [neutral_vtx_array]), [corrective.weight_array]), [neutral_vtx_array])
    
    #Subtract transformed neutral to make deltas
    untransformed_corrective_delta_vtx_array = subtract_vtx_arrays(vtx_array, [corrective_neutral])
    
    #Convert vectors to matricies
    untransformed_corrective_delta_mat_array = np.append(mat_fill, untransformed_corrective_delta_vtx_array, axis=1)
    untransformed_corrective_delta_mat_array = untransformed_corrective_delta_mat_array.reshape(len(vtx_array), 4, 4)
    
    #Apply inverse transformation to all points equally.
    transformed_corrective_delta_mat_array = np.einsum('lij,ljk->lik', untransformed_corrective_delta_mat_array, inverse_rotation_matrix_array)
    
    #Convert matricies to vectors
    transformed_corrective_delta_vtx_array = transformed_corrective_delta_mat_array[...,3,:]
    
    #Calculate weighted transform. points are blended to their untransformed neutral based on skincluster weight.
    output_vtx_array = add_vtx_arrays(multiply_map_arrays(subtract_vtx_arrays(transformed_corrective_delta_vtx_array, [untransformed_corrective_delta_vtx_array]), [corrective.weight_array]), [untransformed_corrective_delta_vtx_array])


    return output_vtx_array
    
    
    
    
    
    

        