import copy
import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

from PySide2 import QtCore, QtGui, QtWidgets
# from maya import OpenMayaUI as omui
from maya import cmds
from functools import partial
# from shiboken2 import wrapInstance
# from face import io, core, processing
# from face.utils import names
# from fdev.utils import model, control, blendshape, painter, corrective, transform
from fdev.utils import corrective
# 
# reload(io)
# reload(core)
# reload(processing)
# reload(model)
# reload(control)
# reload(blendshape)
# reload(painter)
# reload(transform)
reload(corrective)

from face.ui import generic

reload(generic)


class CorrectiveViewer(QtWidgets.QWidget):
    primary_selection_changed = QtCore.Signal(list)
    corrective_removed = QtCore.Signal(object)
    add_update_correctives = QtCore.Signal()

    def __init__(self, network=None):
        super(CorrectiveViewer, self).__init__()
        self.primary_selected = []
        self.secondary_selected = []
        self.ITEMS = {}
        self.network = network
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)
        self.setLayout(self.layout)
        
        self.primary_list = QtWidgets.QTreeWidget()
        self.primary_list.setColumnCount(1)
        self.primary_list.setHeaderLabels(['Name'])
        
        header = self.primary_list.header()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        
        self.layout.addWidget(self.primary_list)
        
        self.primary_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.primary_list.customContextMenuRequested.connect(self.context_menu)
        
        self.primary_list.itemSelectionChanged.connect(self.primary_selection_changed_emit)
        self.primary_list.itemChanged.connect(self.corrective_renamed)
        

    def context_menu(self, QPos): 
        self.listMenu= QtWidgets.QMenu()
        add_update = self.listMenu.addAction("Add/Update Correctives")
        add_update.triggered.connect(self.add_update_correctives)

        remove_item = self.listMenu.addAction("Remove Corrective")
        remove_item.triggered.connect(self.corrective_removed_emit)
        
        parentPosition = self.primary_list.mapToGlobal(QtCore.QPoint(0, 0))        
        self.listMenu.move(parentPosition + QPos)
        self.listMenu.show() 

    def corrective_removed_emit(self):
        corrective = self.primary_list.currentItem().contents
        self.corrective_removed.emit(corrective)

    def primary_selection_changed_emit(self):
        self.primary_selected = self.primary_list.selectedItems()

        
    def corrective_renamed(self, item):
        result = self.network.rename_corrective(item.contents, item.text(0))
        if not result:
            item.setText(item.contents.name)

        
    def refresh(self):
        if not self.network:
            return
        self.populate()
    
    def clear(self):
        self.primary_list.clear()
        for name, item in self.ITEMS.iteritems():
            del item
        
        self.ITEMS = {}

    def populate(self):
        if not self.network:
            return

        for name, cor in self.network.correctives.iteritems():
            
            if name in self.ITEMS.keys():
                continue
            
            item = generic.treeWidgetItem(contents=cor)
            item.setText(0, name)
            
            #Add global corrective enabling.
#             if type_by_name == "invalid":
#                 item.setCheckState(0, QtCore.Qt.Unchecked)
#             else:
#                 item.setCheckState(0, QtCore.Qt.Checked)
 

            self.primary_list.addTopLevelItem(item)
                        
            self.ITEMS[cor.name] = item


class UpdateCorrectivesDialog(QtWidgets.QDialog):
    
    def __init__(self, parent=generic.maya_main_window()):
        super(UpdateCorrectivesDialog, self).__init__(parent)
        
        self.ITEMS = []
        self.CORRECTIVE_DATA = []
        
        self.resize(700,600)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_layout)
        
        self.name_label = QtWidgets.QLabel("Name:")
        self.main_layout.addWidget(self.name_label)
        
        self.corrective_list = QtWidgets.QTreeWidget()
        self.corrective_list.setColumnCount(4)
        self.corrective_list.setHeaderLabels([None, 'Transform', "Corrective name", "Off frame", "On frame"])
        
        header = self.corrective_list.header()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
          
        self.main_layout.addWidget(self.corrective_list)
        
        self.confirm_button = QtWidgets.QPushButton("Confirm")
        self.confirm_button.clicked.connect(self.confirm)
        self.main_layout.addWidget(self.confirm_button)

        self.populate_from_scene()
        
        self.exec_()    
    
    def confirm(self):
        
        for item in self.ITEMS:
            include = item.checkState(0) == QtCore.Qt.Checked
            
            if not include:
                continue
            
            influence = item.text(1)
            corrective_name = self.corrective_list.itemWidget(item, 2).text()
            off_frame = self.corrective_list.itemWidget(item, 3).value()
            on_frame = self.corrective_list.itemWidget(item, 4).value()
            
            self.CORRECTIVE_DATA.append({"influence":influence,
                                         "name":corrective_name,
                                         "off_frame":off_frame,
                                         "on_frame":on_frame})
                        
        self.accept()
        
        
    def populate_from_scene(self):
        selection = cmds.ls(sl=True)
        
        if not selection:
            LOG.error("UpdateCorrectivesDialog: Nothing selected")
        
        history = cmds.listHistory(selection[0])
        
        skincluster = [node for node in history if cmds.objectType(node) == "skinCluster"]
        if skincluster:
            skincluster = skincluster[0]
        else:
            LOG.error("UpdateCorrectivesDialog: No skincluster found on '%s'", selection[0])
            return
        
        influences = [cmds.ls(inf, long=True)[0] for inf in cmds.skinCluster(skincluster,inf=True,q=True)]
        
        for inf in influences:
            short_name = inf.split("|")[-1]
             
            item = QtWidgets.QTreeWidgetItem()
            item.setText(1, short_name)
            item.setCheckState(0, QtCore.Qt.Checked)
 
            if inf.endswith(corrective.NAMING_CONFIG["joint"].format(name="")):
                name = short_name.replace(corrective.NAMING_CONFIG["joint"].format(name=""), "")#Gotta move the naming config more central
            else:
                name = short_name
            
            name_line_edit = QtWidgets.QLineEdit(name, self)

            off_frame_spinbox = QtWidgets.QSpinBox(self)
            off_frame_spinbox.setValue(1)
            on_frame_spinbox = QtWidgets.QSpinBox(self)
            on_frame_spinbox.setValue(2)
 
             
            self.corrective_list.addTopLevelItem(item)
             
            self.corrective_list.setItemWidget(item, 2, name_line_edit)
            self.corrective_list.setItemWidget(item, 3, off_frame_spinbox)
            self.corrective_list.setItemWidget(item, 4, on_frame_spinbox)
            
            self.ITEMS.append(item)                
                
                