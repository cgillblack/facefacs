import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

from PySide2 import QtCore, QtGui, QtWidgets
from maya import OpenMayaUI as omui
from shiboken2 import wrapInstance

class secondaryItem(QtWidgets.QListWidgetItem):
    def __init__(self, *args, **kwargs):
        self.contents = kwargs.pop('contents', None)
        super(secondaryItem, self).__init__(*args, **kwargs)
         
class primaryItem(QtWidgets.QListWidgetItem):
    def __init__(self, *args, **kwargs):
        self.contents = kwargs.pop('contents', None)
        super(primaryItem, self).__init__(*args, **kwargs)
        self.setFlags(self.flags() | QtCore.Qt.ItemIsEditable)
                


class treeWidgetItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, *args, **kwargs):
        self.contents = kwargs.pop('contents', None)
        super(treeWidgetItem, self).__init__(*args, **kwargs)
        self.setFlags(self.flags() | QtCore.Qt.ItemIsEditable)
       

def maya_main_window():
    '''
    Return the Maya main window widget as a Python object
    '''
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)
