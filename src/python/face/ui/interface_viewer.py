import copy
import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

from PySide2 import QtCore, QtGui, QtWidgets
# from maya import OpenMayaUI as omui
from maya import cmds
from functools import partial
# from shiboken2 import wrapInstance
# from face import io, core, processing
from face.utils import names
# from fdev.utils import model, control, blendshape, painter, corrective, transform
from fdev.utils import control
# 
# reload(io)
# reload(core)
# reload(processing)
# reload(model)
reload(control)
# reload(blendshape)
# reload(painter)
# reload(transform)
# reload(corrective)
from face.ui import generic


reload(generic)

class InterfaceViewer(QtWidgets.QWidget):
    primary_selection_changed = QtCore.Signal(list)
#     secondary_selection_changed = QtCore.Signal(list)
#     primary_double_clicked = QtCore.Signal(object)
#     secondary_double_clicked = QtCore.Signal(object)
    
    control_removed = QtCore.Signal(object)
    
    build_guides = QtCore.Signal()
    add_update = QtCore.Signal()
    
    def __init__(self, network=None):
        super(InterfaceViewer, self).__init__()
        self.primary_selected = []
        self.secondary_selected = []
        self.network = network
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)
        self.setLayout(self.layout)
        
        self.primary_list = QtWidgets.QListWidget()
        self.layout.addWidget(self.primary_list)
        
        self.primary_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.primary_list.customContextMenuRequested.connect(self.context_menu)
        
        self.primary_list.itemSelectionChanged.connect(self.primary_selection_changed_emit)
        self.primary_list.itemChanged.connect(self.control_renamed)
        
        self.options_list = QtWidgets.QFrame()
        
        self.options_layout = QtWidgets.QVBoxLayout()
        self.options_list.setLayout(self.options_layout)
        
        self.layout.addWidget(self.options_list)
        
        
        self.name_label_title = QtWidgets.QLabel("Name:")
        self.options_layout.addWidget(self.name_label_title)
        self.name_label = QtWidgets.QLabel("")
        self.options_layout.addWidget(self.name_label)

        self.driven_shapes_label_title = QtWidgets.QLabel("Driven Shapes:")
        self.options_layout.addWidget(self.driven_shapes_label_title)
        
        self.driven_shapes_layout = QtWidgets.QGridLayout()
        self.options_layout.addLayout(self.driven_shapes_layout)
        
        self.driven_shapes_layout.addWidget(QtWidgets.QLabel("X"), 0,1)
        self.driven_shapes_layout.addWidget(QtWidgets.QLabel("Y"), 1,1)
        self.driven_shapes_layout.addWidget(QtWidgets.QLabel("Z"), 2,1)
        
        self.driven_shapes_layout.addWidget(QtWidgets.QLabel("-X"), 3,1)
        self.driven_shapes_layout.addWidget(QtWidgets.QLabel("-Y"), 4,1)
        self.driven_shapes_layout.addWidget(QtWidgets.QLabel("-Z"), 5,1)
        
        #Positive
        self.positive_x_line_edit = QtWidgets.QLineEdit()
        self.positive_x_line_edit.textEdited.connect(partial(self.driven_shapes_change, "tx"))
        self.driven_shapes_layout.addWidget(self.positive_x_line_edit, 0,2)
        self.positive_x_checkbox = QtWidgets.QCheckBox()
        self.positive_x_checkbox.stateChanged.connect(partial(self.enabled_axes_change, "tx"))
        self.driven_shapes_layout.addWidget(self.positive_x_checkbox, 0,0)
        
        self.positive_y_line_edit = QtWidgets.QLineEdit()
        self.positive_y_line_edit.textEdited.connect(partial(self.driven_shapes_change, "ty"))
        self.driven_shapes_layout.addWidget(self.positive_y_line_edit, 1,2)
        self.positive_y_checkbox = QtWidgets.QCheckBox()
        self.positive_y_checkbox.stateChanged.connect(partial(self.enabled_axes_change, "ty"))
        self.driven_shapes_layout.addWidget(self.positive_y_checkbox, 1,0)


        self.positive_z_line_edit = QtWidgets.QLineEdit()
        self.positive_z_line_edit.textEdited.connect(partial(self.driven_shapes_change, "tz"))
        self.driven_shapes_layout.addWidget(self.positive_z_line_edit, 2,2)
        self.positive_z_checkbox = QtWidgets.QCheckBox()
        self.positive_z_checkbox.stateChanged.connect(partial(self.enabled_axes_change, "tz"))
        self.driven_shapes_layout.addWidget(self.positive_z_checkbox, 2,0)
        
        
        #Negative
        self.negative_x_line_edit = QtWidgets.QLineEdit()
        self.negative_x_line_edit.textEdited.connect(partial(self.driven_shapes_change, "-tx"))
        self.driven_shapes_layout.addWidget(self.negative_x_line_edit, 3,2)
        self.negative_x_checkbox = QtWidgets.QCheckBox()
        self.negative_x_checkbox.stateChanged.connect(partial(self.enabled_axes_change, "-tx"))
        self.driven_shapes_layout.addWidget(self.negative_x_checkbox, 3,0)
        
        self.negative_y_line_edit = QtWidgets.QLineEdit()
        self.negative_y_line_edit.textEdited.connect(partial(self.driven_shapes_change, "-ty"))
        self.driven_shapes_layout.addWidget(self.negative_y_line_edit, 4,2)
        self.negative_y_checkbox = QtWidgets.QCheckBox()
        self.negative_y_checkbox.stateChanged.connect(partial(self.enabled_axes_change, "-ty"))
        self.driven_shapes_layout.addWidget(self.negative_y_checkbox, 4,0)
        
        self.negative_z_line_edit = QtWidgets.QLineEdit()
        self.negative_z_line_edit.textEdited.connect(partial(self.driven_shapes_change, "-tz"))
        self.driven_shapes_layout.addWidget(self.negative_z_line_edit, 5,2)
        self.negative_z_checkbox = QtWidgets.QCheckBox()
        self.negative_z_checkbox.stateChanged.connect(partial(self.enabled_axes_change, "-tz"))
        self.driven_shapes_layout.addWidget(self.negative_z_checkbox, 5,0)
        
        
        self.mirror_checkbox = QtWidgets.QCheckBox("Mirror:")
        self.options_layout.addWidget(self.mirror_checkbox)
        self.mirror_checkbox.stateChanged.connect(self.mirror_change)

        self.mirror_axis_combo_title = QtWidgets.QLabel("Mirror Axis:")
        self.options_layout.addWidget(self.mirror_axis_combo_title)
        self.mirror_axis_combo = QtWidgets.QComboBox()
        self.mirror_axis_combo.addItem("x")
        self.mirror_axis_combo.addItem("y")
        self.mirror_axis_combo.addItem("z")
        self.mirror_axis_combo.currentIndexChanged.connect(self.mirror_axis_change)
        
        self.options_layout.addWidget(self.mirror_axis_combo)
        
        self.mirror_prefix_lineedit_title = QtWidgets.QLabel("Mirror prefixes:")
        self.options_layout.addWidget(self.mirror_prefix_lineedit_title)  
        self.mirror_prefix_lineedit = QtWidgets.QLineEdit()
        self.options_layout.addWidget(self.mirror_prefix_lineedit)   
        self.mirror_prefix_lineedit.returnPressed.connect(self.mirror_prefix_change)

        self.size_spin_box_title = QtWidgets.QLabel("Size:")
        self.options_layout.addWidget(self.size_spin_box_title)  
        self.size_spin_box = QtWidgets.QDoubleSpinBox()
        self.size_spin_box.setSingleStep(0.05)
        self.options_layout.addWidget(self.size_spin_box)   
        self.size_spin_box.valueChanged.connect(self.size_spin_box_change)
        
        
        self.attach_to_mesh_checkbox = QtWidgets.QCheckBox("Attach to mesh:")
        self.options_layout.addWidget(self.attach_to_mesh_checkbox)
        self.attach_to_mesh_checkbox.stateChanged.connect(self.attach_to_mesh_changed)
        
        self.options_layout.addStretch()

    def context_menu(self, QPos): 
        self.listMenu= QtWidgets.QMenu()
        add_update_controls_item = self.listMenu.addAction("Add/Update controls")
        add_update_controls_item.triggered.connect(self.add_update)

        build_guides_item = self.listMenu.addAction("Build guide locators")
        build_guides_item.triggered.connect(self.build_guides)

        remove_item = self.listMenu.addAction("Remove Item")
        remove_item.triggered.connect(self.control_removed_emit)

        
        parentPosition = self.primary_list.mapToGlobal(QtCore.QPoint(0, 0))        
        self.listMenu.move(parentPosition + QPos)
        self.listMenu.show() 

    def control_removed_emit(self):
        control = self.primary_list.currentItem().contents
        self.control_removed.emit(control)

    def primary_selection_changed_emit(self):
        self.primary_selected = self.primary_list.selectedItems()
        if self.primary_selected:        
            self.fill_options(self.primary_selected[0].contents)

        
    def fill_options(self, ctrl):
        name = ctrl.name
        mirror = ctrl.mirror
        mirror_axis = ctrl.mirror_axis
        mirror_prefixes = ctrl.mirror_prefixes
        size = ctrl.size
        driven_shapes = ctrl.driven_shapes
        enabled_axes = ctrl.enabled_axes
        attach_to_mesh = ctrl.attach_to_mesh
        
        self.name_label.setText(name)
        self.mirror_checkbox.setChecked(mirror)
        self.mirror_axis_combo.setCurrentIndex(mirror_axis)
        self.mirror_prefix_lineedit.setText(", ".join(mirror_prefixes))
        
        #Positive
        self.positive_x_line_edit.setText(driven_shapes["tx"])
        self.positive_y_line_edit.setText(driven_shapes["ty"])
        self.positive_z_line_edit.setText(driven_shapes["tz"])
        
        self.positive_x_checkbox.setChecked(enabled_axes["tx"])
        self.positive_y_checkbox.setChecked(enabled_axes["ty"])
        self.positive_z_checkbox.setChecked(enabled_axes["tz"])

        #Negative
        self.negative_x_line_edit.setText(driven_shapes["-tx"])
        self.negative_y_line_edit.setText(driven_shapes["-ty"])
        self.negative_z_line_edit.setText(driven_shapes["-tz"])

        self.negative_x_checkbox.setChecked(enabled_axes["-tx"])
        self.negative_y_checkbox.setChecked(enabled_axes["-ty"])
        self.negative_z_checkbox.setChecked(enabled_axes["-tz"])

        self.size_spin_box.setValue(size)
        self.attach_to_mesh_checkbox.setChecked(attach_to_mesh)

    
    def clear_options(self):       
        self.name_label.setText("")
        self.mirror_checkbox.setChecked(False)
        self.mirror_axis_combo.setCurrentIndex(0)
        self.mirror_prefix_lineedit.setText("r, l")
        
        #Positive
        self.positive_x_line_edit.setText("")
        self.positive_y_line_edit.setText("")
        self.positive_z_line_edit.setText("")

        self.positive_x_checkbox.setChecked(False)
        self.positive_y_checkbox.setChecked(False)
        self.positive_z_checkbox.setChecked(False)
        #Negative
        self.negative_x_line_edit.setText("")
        self.negative_y_line_edit.setText("")
        self.negative_z_line_edit.setText("")

        self.negative_x_checkbox.setChecked(False)
        self.negative_y_checkbox.setChecked(False)
        self.negative_z_checkbox.setChecked(False)

        self.size_spin_box.setValue(0.2)


    def mirror_change(self):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            ctrl.mirror = self.mirror_checkbox.isChecked()

    def mirror_axis_change(self):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            ctrl.mirror_axis = self.mirror_axis_combo.currentIndex()

    def mirror_prefix_change(self):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            ctrl.mirror_prefixes = self.mirror_prefix_lineedit.text().replace(" ","").split(",")
        
    def driven_shapes_change(self, driver, text):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            ctrl.driven_shapes[driver] = text

    def enabled_axes_change(self, driver, state):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            
            if state == QtCore.Qt.Checked:
                ctrl.enabled_axes[driver] = True
            elif state == QtCore.Qt.Unchecked:
                ctrl.enabled_axes[driver] = False

    def size_spin_box_change(self):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            ctrl.size = self.size_spin_box.value()

    def attach_to_mesh_changed(self):
        if self.primary_list.selectedItems():
            ctrl = self.primary_list.selectedItems()[0].contents
            ctrl.attach_to_mesh = self.attach_to_mesh_checkbox.isChecked()

    def control_renamed(self, item):
        result = self.network.rename_control(item.contents, item.text())
        if not result:
            item.setText(item.contents.name.rpartition("_")[0])
                
        self.fill_options(item.contents)
        
    def refresh(self):
        self.primary_list.clear()
        self.clear_options()
        
        if not self.network:
            return
        
        controls = self.network.get_controls(None)
        for name, ctrl in controls.iteritems():
            control = generic.primaryItem(name, contents=ctrl)
            self.primary_list.addItem(control)
            
        self.primary_list.sortItems()  


class UpdateControlsDialog(QtWidgets.QDialog):
    
    def __init__(self, parent=generic.maya_main_window()):
        super(UpdateControlsDialog, self).__init__(parent)
        
        self.ITEMS = []
        self.CONTROL_DATA = []
        
        self.resize(700,600)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_layout)
        
        self.name_label = QtWidgets.QLabel("Name:")
        self.main_layout.addWidget(self.name_label)
        
        self.control_list = QtWidgets.QTreeWidget()
        self.control_list.setColumnCount(2)
        self.control_list.setHeaderLabels([None, 'Transform', "Control name"])
        
        header = self.control_list.header()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
#         self.control_list.setColumnWidth(0,25)  
          
        self.main_layout.addWidget(self.control_list)
        
        self.confirm_button = QtWidgets.QPushButton("Confirm")
        self.confirm_button.clicked.connect(self.confirm)
        self.main_layout.addWidget(self.confirm_button)

        self.populate_from_scene()
        
        self.exec_()    
    
    def confirm(self):
        
        for item in self.ITEMS:
            include = item.checkState(0) == QtCore.Qt.Checked
            
            if not include:
                continue
            
            node = item.text(1)
#             shape_type = self.control_list.itemWidget(item, 2).currentText().lower()
            control_name = self.control_list.itemWidget(item, 2).text()
            
            self.CONTROL_DATA.append({"transform":node, "name":control_name})
                        
        self.accept()
        
        
    def populate_from_scene(self):
        selection = cmds.ls(sl=True)
         
        for obj in selection:
            type_by_name = names.info_from_name(obj)[0]
             
            item = QtWidgets.QTreeWidgetItem()
            item.setText(1, obj)
            item.setCheckState(0, QtCore.Qt.Checked)
 
            if obj.endswith(control.NAMING_CONFIG["guide"].format(name="")):
                name = obj.replace(control.NAMING_CONFIG["guide"].format(name=""), "")
            else:
                name = obj
            
            name_line_edit = QtWidgets.QLineEdit(name, self)
 
             
            self.control_list.addTopLevelItem(item)
             
            self.control_list.setItemWidget(item, 2, name_line_edit)
            
            self.ITEMS.append(item)
                
                
                