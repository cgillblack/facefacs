import copy
import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

from PySide2 import QtCore, QtGui, QtWidgets
from maya import OpenMayaUI as omui
from maya import cmds
from functools import partial
from shiboken2 import wrapInstance
# from face import io, core, processing
from face.utils import names
# from fdev.utils import model, control, blendshape, painter, corrective, transform
# 
# reload(io)
# reload(core)
# reload(processing)
# reload(model)
# reload(control)
# reload(blendshape)
# reload(painter)
# reload(transform)
# reload(corrective)

from face.ui import generic

reload(generic)

class MapViewer(QtWidgets.QWidget):
    primary_selection_changed = QtCore.Signal(list)
    map_removed = QtCore.Signal(object)
    build_painter = QtCore.Signal(object)
    update_painter = QtCore.Signal(object)
    delete_painter = QtCore.Signal(object)
    create_map = QtCore.Signal()
    
    
    def __init__(self, network=None):
        super(MapViewer, self).__init__()
        self.primary_selected = []
        self.secondary_selected = []
        
        self.ITEMS = {}
        
        self.clipboard = None

        self.network = network
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)
        self.setLayout(self.layout)
        
        self.primary_list = QtWidgets.QTreeWidget()
        self.primary_list.setColumnCount(2)
        self.primary_list.setHeaderLabels(['Name', "Prefixes"])
        
        header = self.primary_list.header()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
#         header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

        self.layout.addWidget(self.primary_list)
        
        self.primary_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.primary_list.customContextMenuRequested.connect(self.context_menu)

        self.primary_list.itemSelectionChanged.connect(self.primary_selection_changed_emit)
        self.primary_list.itemChanged.connect(self.map_renamed)
        
        self.options_list = QtWidgets.QFrame()
        
#         self.options_layout = QtWidgets.QVBoxLayout()
#         self.options_list.setLayout(self.options_layout)
#         
#         self.layout.addWidget(self.options_list)
#         
#         self.name_label_title = QtWidgets.QLabel("Name:")
#         self.options_layout.addWidget(self.name_label_title)
#         self.name_label = QtWidgets.QLabel("")
#         self.options_layout.addWidget(self.name_label)
#         
#         self.prefix_lineedit_title = QtWidgets.QLabel("Prefixes:")
#         self.options_layout.addWidget(self.prefix_lineedit_title)  
#         self.prefix_lineedit = QtWidgets.QLineEdit()
#         self.options_layout.addWidget(self.prefix_lineedit)   
#         self.prefix_lineedit.returnPressed.connect(self.prefix_change)
        
#         self.options_layout.addStretch()


    def context_menu(self, QPos): 
        self.listMenu= QtWidgets.QMenu()
        create_map = self.listMenu.addAction("Create map")
        create_map.triggered.connect(self.create_map)

        remove_map = self.listMenu.addAction("Remove map")
        remove_map.triggered.connect(self.map_removed_emit)

        copy_map = self.listMenu.addAction("Copy map array")
        copy_map.triggered.connect(self.copy_map_func)

        paste_map = self.listMenu.addAction("Paste map array")
        paste_map.triggered.connect(self.paste_map_func)
    
        build_painter = self.listMenu.addAction("Build painter")
        build_painter.triggered.connect(self.build_painter_emit)
        
        update_painter = self.listMenu.addAction("Update painter")
        update_painter.triggered.connect(self.update_painter_emit)

        delete_painter = self.listMenu.addAction("Delete painter")
        delete_painter.triggered.connect(self.delete_painter_emit)
        
        parentPosition = self.primary_list.mapToGlobal(QtCore.QPoint(0, 0))        
        self.listMenu.move(parentPosition + QPos)
        self.listMenu.show() 
        
        
    def map_removed_emit(self):
        map = self.primary_list.currentItem().contents
        self.map_removed.emit(map)

    def copy_map_func(self):
        map = self.primary_list.currentItem().contents
        self.clipboard = map.map_array

    def paste_map_func(self):
        if type(self.clipboard) == type(None):
            LOG.warning("No map array to paste.")
            return
        
        map = self.primary_list.currentItem().contents
        map.map_array = self.clipboard

        
    def build_painter_emit(self):
        map = self.primary_list.currentItem().contents
        self.build_painter.emit(map)
        
    def update_painter_emit(self):
        map = self.primary_list.currentItem().contents
        if map.painter:
            self.update_painter.emit(map.painter)
        
    def delete_painter_emit(self):
        map = self.primary_list.currentItem().contents
        if map.painter:
            self.delete_painter.emit(map.painter)

    def primary_selection_changed_emit(self):
        self.primary_selected = self.primary_list.selectedItems()
        if self.primary_selected:        
            self.fill_options(self.primary_selected[0].contents)

        
    def fill_options(self, map):
        return
        name = map.name
        prefixes = map.prefixes
        self.name_label.setText(name)
        self.prefix_lineedit.setText(", ".join(prefixes))

    def prefix_change(self, item):
        
        map = item.contents
        map.prefixes = self.primary_list.itemWidget(item, 1).text().replace(" ","").split(",")

    def map_renamed(self, item):
        result = self.network.rename_map(item.contents, item.text(0))
        if not result:
            item.setText(item.contents.name)
        
        self.fill_options(item.contents)
   
    def refresh(self):
#         self.primary_list.clear()
#         self.clear_options()
        
        if not self.network:
            return
        
        self.populate()
        

    def clear(self):
        self.primary_list.clear()
        for name, item in self.ITEMS.iteritems():
            del item
        
        self.ITEMS = {}
        
    def populate(self):
        if not self.network:
            return

        for name, map in self.network.maps.iteritems():

            if name in self.ITEMS.keys():
                item = self.ITEMS[name]
                
                self.primary_list.itemWidget(item, 1).setText(", ".join(map.prefixes))
                continue
                            
            item = generic.treeWidgetItem(contents=map)
            item.setText(0, name)
            
            #Add global map enabling.
#             if type_by_name == "invalid":
#                 item.setCheckState(0, QtCore.Qt.Unchecked)
#             else:
#                 item.setCheckState(0, QtCore.Qt.Checked)

            prefixes_line_edit = QtWidgets.QLineEdit(", ".join(map.prefixes), self)
            prefixes_line_edit.returnPressed.connect(partial(self.prefix_change, item))
 

            self.primary_list.addTopLevelItem(item)
             
            self.primary_list.setItemWidget(item, 1, prefixes_line_edit)
            
            self.ITEMS[map.name] = item
                



class CreateMapDialog(QtWidgets.QDialog):
    def __init__(self, parent=generic.maya_main_window()):
        super(CreateMapDialog, self).__init__(parent)
        
        self.MAP_DATA = []
        
        self.main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_layout)
        
        self.name_label = QtWidgets.QLabel("Name:")
        self.main_layout.addWidget(self.name_label)
        self.name_line_edit = QtWidgets.QLineEdit()
        self.main_layout.addWidget(self.name_line_edit)
        
        self.axis_label = QtWidgets.QLabel("Axis:")
        self.main_layout.addWidget(self.axis_label)
        self.axis_combo = QtWidgets.QComboBox(self)
        self.axis_combo.addItem("X")
        self.axis_combo.addItem("Y")
        self.axis_combo.addItem("Z")
        
        self.main_layout.addWidget(self.axis_combo)
        
        self.width_label = QtWidgets.QLabel("Width:")
        self.main_layout.addWidget(self.width_label)
        self.width_spin_box = QtWidgets.QDoubleSpinBox(self)
        self.width_spin_box.setSingleStep(0.5)
        self.width_spin_box.setValue(1)
        self.main_layout.addWidget(self.width_spin_box)

        self.prefixes_label = QtWidgets.QLabel("Prefixes:")
        self.main_layout.addWidget(self.prefixes_label)
        self.prefixes_line_edit = QtWidgets.QLineEdit("r, l")
        self.main_layout.addWidget(self.prefixes_line_edit)

        self.offset_label = QtWidgets.QLabel("Offset:")
        self.main_layout.addWidget(self.offset_label)
        self.offset_spin_box = QtWidgets.QDoubleSpinBox(self)
        self.offset_spin_box.setSingleStep(0.5)
        self.main_layout.addWidget(self.offset_spin_box)
        
        self.add_button = QtWidgets.QPushButton("Add")
        self.add_button.clicked.connect(self.add)
        self.main_layout.addWidget(self.add_button)
        
        self.exec_()    
    
    def add(self):
        
        name = self.name_line_edit.text()
        axis = self.axis_combo.currentIndex()
        prefixes = self.prefixes_line_edit.text().replace(" ", "").split(",")
        width = self.width_spin_box.value()
        offset = self.offset_spin_box.value()
            
        self.MAP_DATA = {"name":name, "axis":axis, "prefixes":prefixes, "width":width, "offset":offset}
                        
        self.accept()


class MapListDialog(QtWidgets.QDialog):
    def __init__(self, parent=generic.maya_main_window()):
        super(MapListDialog, self).__init__(parent)
        
        self.MAP_DATA = []
        
#         self.resize(700,600)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_layout)
        
        self.name_label = QtWidgets.QLabel("Name:")
        self.main_layout.addWidget(self.name_label)
        self.name_line_edit = QtWidgets.QLineEdit()
        self.main_layout.addWidget(self.name_line_edit)
        
        self.axis_label = QtWidgets.QLabel("Axis:")
        self.main_layout.addWidget(self.axis_label)
        self.axis_combo = QtWidgets.QComboBox(self)
        self.axis_combo.addItem("X")
        self.axis_combo.addItem("Y")
        self.axis_combo.addItem("Z")
        
        self.main_layout.addWidget(self.axis_combo)
        
        self.width_label = QtWidgets.QLabel("Width:")
        self.main_layout.addWidget(self.width_label)
        self.width_spin_box = QtWidgets.QDoubleSpinBox(self)
        self.width_spin_box.setSingleStep(0.5)
        self.width_spin_box.setValue(1)
        self.main_layout.addWidget(self.width_spin_box)

        self.offset_label = QtWidgets.QLabel("Offset:")
        self.main_layout.addWidget(self.offset_label)
        self.offset_spin_box = QtWidgets.QDoubleSpinBox(self)
        self.offset_spin_box.setSingleStep(0.5)
        self.main_layout.addWidget(self.offset_spin_box)
        
        self.confirm_button = QtWidgets.QPushButton("Confirm")
        self.confirm_button.clicked.connect(self.confirm)
        self.main_layout.addWidget(self.confirm_button)
        
        self.exec_()    
    
    def confirm(self):
        
        name = self.name_line_edit.text()
        axis = self.axis_combo.currentIndex()
        width = self.width_spin_box.value()
        offset = self.offset_spin_box.value()
            
        self.MAP_DATA = {"name":name, "axis":axis, "width":width, "offset":offset}
                        
        self.accept()
                
                
                
                