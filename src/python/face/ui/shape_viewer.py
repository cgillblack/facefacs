import copy
import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

from PySide2 import QtCore, QtGui, QtWidgets
from maya import OpenMayaUI as omui
from maya import cmds
from functools import partial
from shiboken2 import wrapInstance
from face import core
from face.utils import names
# from fdev.utils import model, control, blendshape, painter, corrective, transform
from face.ui import generic


reload(generic)

# reload(io)
reload(core)
# reload(processing)
# reload(model)
# reload(control)
# reload(blendshape)
# reload(painter)
# reload(transform)
# reload(corrective)


class ShapeViewer(QtWidgets.QWidget):
    primary_selection_changed = QtCore.Signal(list)
    secondary_selection_changed = QtCore.Signal(list)
    primary_double_clicked = QtCore.Signal(object)
    secondary_double_clicked = QtCore.Signal(object)

    shape_removed = QtCore.Signal(object)
    
    map_removed = QtCore.Signal(object, object)
    map_added = QtCore.Signal(object, object)

    corrective_removed = QtCore.Signal(object, object)
    corrective_added = QtCore.Signal(object, object)
    
    add_update_shapes = QtCore.Signal()
    
    def __init__(self, network=None):
        super(ShapeViewer, self).__init__()
        self.primary_selected = []
        self.secondary_selected = []
        self.network = network
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(2)
        self.setLayout(self.layout)
        
        #Primary list
        self.primary_list = QtWidgets.QListWidget()
        self.primary_list.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.layout.addWidget(self.primary_list)
        
        #Right click menu
        self.primary_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.primary_list.customContextMenuRequested.connect(self.primary_context_menu)

        self.primary_list.itemSelectionChanged.connect(self.primary_selection_changed_emit)
        self.primary_list.itemClicked.connect(self.refresh_secondary_list)
        self.primary_list.itemChanged.connect(self.shape_renamed)
        
        #Secondary list
        self.secondary_list = QtWidgets.QListWidget()
        self.layout.addWidget(self.secondary_list)

        #Right click menu
        self.secondary_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.secondary_list.customContextMenuRequested.connect(self.secondary_context_menu)
        
        self.secondary_list.itemSelectionChanged.connect(self.secondary_selection_changed_emit)
        self.secondary_list.itemDoubleClicked.connect(self.secondary_double_clicked_emit)

        
        self.clear_secondary_list()
    

    def primary_context_menu(self, QPos): 
        self.listMenu= QtWidgets.QMenu()
        add_update_shapes = self.listMenu.addAction("Add/Update shapes")
        add_update_shapes.triggered.connect(self.add_update_shapes)
        
        remove_shape = self.listMenu.addAction("Remove Shape")
        remove_shape.triggered.connect(self.primary_shape_removed)

        add_map = self.listMenu.addAction("Add Map")
        add_map.triggered.connect(self.primary_add_map)

        add_corrective = self.listMenu.addAction("Add Corrective")
        add_corrective.triggered.connect(self.primary_add_corrective)
        
        
        parentPosition = self.primary_list.mapToGlobal(QtCore.QPoint(0, 0))        
        self.listMenu.move(parentPosition + QPos)
        self.listMenu.show() 

    def secondary_context_menu(self, QPos):
        self.listMenu= QtWidgets.QMenu()
        item = self.secondary_list.currentItem()
        if not item:
            LOG.error("No list item found")
            return
        
        contents = item.contents
        
        if isinstance(contents, core.Shape):      
            apply_shape_item = self.listMenu.addAction("Apply Shape")
            apply_shape_item.triggered.connect(partial(self.secondary_double_clicked.emit, contents))
            
            remove_shape_item = self.listMenu.addAction("Remove Shape")
            remove_shape_item.triggered.connect(self.secondary_shape_removed)
        elif isinstance(contents, core.Map):
            menu_item = self.listMenu.addAction("Remove Map")
            menu_item.triggered.connect(self.secondary_map_removed)     
        elif isinstance(contents, core.Corrective):
            menu_item = self.listMenu.addAction("Remove Corrective")
            menu_item.triggered.connect(self.secondary_corrective_removed)   
        else:
            LOG.error("%s is not valid.", contents)
              
        parentPosition = self.secondary_list.mapToGlobal(QtCore.QPoint(0, 0))        
        self.listMenu.move(parentPosition + QPos)
        self.listMenu.show() 

    
    def primary_shape_removed(self):
        shape = self.primary_list.currentItem().contents
        self.shape_removed.emit(shape)
    
    def primary_add_map(self):
        
        self.add_map_list_dialog = QtWidgets.QDialog(self)
        
        self.add_map_body_layout = QtWidgets.QVBoxLayout()
        self.add_map_list_dialog.setLayout(self.add_map_body_layout)
        
        self.name_label = QtWidgets.QLabel("Maps:")
        self.add_map_body_layout.addWidget(self.name_label)
        self.map_list_widget = QtWidgets.QListWidget()
        
        for name, map in self.network.maps.iteritems():
            mp = generic.primaryItem(name, contents=map)
            self.map_list_widget.addItem(mp)
        
        self.add_map_body_layout.addWidget(self.map_list_widget)
        
        self.confirm_button = QtWidgets.QPushButton("Confirm")
        self.confirm_button.clicked.connect(self.add_map_list_dialog.accept)
        self.add_map_body_layout.addWidget(self.confirm_button)
        
        self.add_map_list_dialog.exec_()    
        added_map = self.map_list_widget.currentItem()
        self.add_map_list_dialog.deleteLater()
            
        if added_map:
            items = self.primary_list.selectedItems()
    
            for item in items:
                shape = item.contents
                self.map_added.emit(added_map.contents, shape)
    
    

    def primary_add_corrective(self):
        shape = self.primary_list.currentItem().contents
        
        self.add_corrective_list_dialog = QtWidgets.QDialog(self)
        
        self.add_corrective_body_layout = QtWidgets.QVBoxLayout()
        self.add_corrective_list_dialog.setLayout(self.add_corrective_body_layout)
        
        self.name_label = QtWidgets.QLabel("Correctives:")
        self.add_corrective_body_layout.addWidget(self.name_label)
        self.corrective_list_widget = QtWidgets.QListWidget()
        
        for name, corrective in self.network.correctives.iteritems():
            cor = generic.primaryItem(name, contents=corrective)
            self.corrective_list_widget.addItem(cor)
        
        self.add_corrective_body_layout.addWidget(self.corrective_list_widget)
        
        self.confirm_button = QtWidgets.QPushButton("Confirm")
        self.confirm_button.clicked.connect(self.add_corrective_list_dialog.accept)
        self.add_corrective_body_layout.addWidget(self.confirm_button)
        
        self.add_corrective_list_dialog.exec_()    
        added_corrective = self.corrective_list_widget.currentItem()
        self.add_corrective_list_dialog.deleteLater()
        
        if added_corrective:
            items = self.primary_list.selectedItems()
            for item in items:
                shape = item.contents
                self.corrective_added.emit(added_corrective.contents, shape)
    
    
    def secondary_shape_removed(self):
        shape = self.secondary_list.currentItem().contents
        self.shape_removed.emit(shape)

    def secondary_map_removed(self):
        map = self.secondary_list.currentItem().contents
        shape = self.primary_list.currentItem().contents
        self.map_removed.emit(shape, map)
    
    def secondary_corrective_removed(self):
        corrective = self.secondary_list.currentItem().contents
        shape = self.primary_list.currentItem().contents
        self.corrective_removed.emit(shape, corrective)
    

    def primary_selection_changed_emit(self):
        self.primary_selected = self.primary_list.selectedItems()
        self.primary_selection_changed.emit([item.contents for item in self.primary_selected])
                    
    def secondary_double_clicked_emit(self, *args):
        self.primary_double_clicked.emit(args[0].contents)
        
    def secondary_selection_changed_emit(self):
        self.secondary_selected = self.secondary_list.selectedItems()
        self.secondary_selection_changed.emit([item.contents for item in self.secondary_selected])
            
    def shape_renamed(self, item):
        result = self.network.rename_shape(item.contents, item.text()+"_100")
        if not result:
            item.setText(item.contents.name.rpartition("_")[0])
        
        self.refresh_secondary_list(item)

    def refresh_secondary_list(self, shape_primary_item):
        
        if len(self.primary_selected) > 1:
            self.clear_secondary_list()
            return
                
        shape_name = shape_primary_item.text()
        shape_name = shape_name + "_100"
        
        if not self.network:
            return
        
        if not shape_name in self.network.shapes.keys():
            return
        
        self.blockSignals(True)
        self.secondary_list.clear()
        self.blockSignals(False)
        
        percent = QtWidgets.QListWidgetItem("Percent:")
        percent.setFlags(QtCore.Qt.NoItemFlags)
        percent.setTextColor(QtGui.QColor(255,255,255))
        self.secondary_list.addItem(percent)
        
        shape = generic.secondaryItem("100", contents=self.network.shapes[shape_name])
        
        if self.network.shapes[shape_name].vtx_array_set:
            shape.setTextColor(QtGui.QColor(150,255,150))
        else:
            shape.setTextColor(QtGui.QColor(255,0,0))
            shape_primary_item.setTextColor(QtGui.QColor(255,0,0))

        self.secondary_list.addItem(shape)
        
        inbetweens = shape.contents.inbetweens
        
        sorted_inbetween_keys = reversed(sorted(inbetweens.keys()))

        for percentage in sorted_inbetween_keys:
            inbetween = inbetweens[percentage]
            
            inbetween_shape = generic.secondaryItem(str(percentage), contents=inbetween)
            if inbetween.vtx_array_set:
                inbetween_shape.setTextColor(QtGui.QColor(150,255,150))
            else:
                inbetween_shape.setTextColor(QtGui.QColor(255,0,0))
                shape_primary_item.setTextColor(QtGui.QColor(255,0,0))
            
            self.secondary_list.addItem(inbetween_shape)
            
        combinations = QtWidgets.QListWidgetItem("Combinations:")
        combinations.setFlags(QtCore.Qt.NoItemFlags)
        combinations.setTextColor(QtGui.QColor(255,255,255))
        self.secondary_list.addItem(combinations)
        
        
        combo_shapes = self.network.get_combination_shapes([self.network.shapes[shape_name]], common=False)
        for combo_shp in combo_shapes:
            title = QtWidgets.QListWidgetItem(" "*8+"/".join([comb.name for comb in combo_shp.combination_of]))
            title.setFlags(QtCore.Qt.NoItemFlags)
            title.setTextColor(QtGui.QColor(150,150,255))
            self.secondary_list.addItem(title)
            
            combo_shape = generic.secondaryItem(" "*16+"100", contents=combo_shp)
                            
            if combo_shp.vtx_array_set:
                combo_shape.setTextColor(QtGui.QColor(150,255,150))
            else:
                combo_shape.setTextColor(QtGui.QColor(255,0,0))
                shape_primary_item.setTextColor(QtGui.QColor(255,0,0))
        
            self.secondary_list.addItem(combo_shape)
                        
            combo_inbetweens = combo_shp.inbetweens
            sorted_inbetween_keys = reversed(sorted(combo_inbetweens.keys()))
    
            for percentage in sorted_inbetween_keys:
                combo_inbetween = combo_inbetweens[percentage]
                
                inbetween_shape = generic.secondaryItem(" "*16+str(percentage), contents=combo_inbetween)
                if combo_inbetween.vtx_array_set:
                    inbetween_shape.setTextColor(QtGui.QColor(150,255,150))
                else:
                    inbetween_shape.setTextColor(QtGui.QColor(255,0,0))
                    shape_primary_item.setTextColor(QtGui.QColor(255,0,0))
                
                self.secondary_list.addItem(inbetween_shape)



        split_maps = QtWidgets.QListWidgetItem("Maps:")
        split_maps.setFlags(QtCore.Qt.NoItemFlags)
        split_maps.setTextColor(QtGui.QColor(255,255,255))
        self.secondary_list.addItem(split_maps)
        
        for mp in self.network.shapes[shape_name].split_maps:
            
            map = generic.secondaryItem(" "*8+mp.name, contents=mp)
            
            if mp.map_array_set:
                map.setTextColor(QtGui.QColor(150,255,150))
            else:
                map.setTextColor(QtGui.QColor(255,0,0))
                shape_primary_item.setTextColor(QtGui.QColor(255,0,0))

            self.secondary_list.addItem(map)


        correctives = QtWidgets.QListWidgetItem("Correctives:")
        correctives.setFlags(QtCore.Qt.NoItemFlags)
        correctives.setTextColor(QtGui.QColor(255,255,255))
        self.secondary_list.addItem(correctives)
        
        for corrective in self.network.shapes[shape_name].correctives:
            
            cor = generic.secondaryItem(" "*8+corrective.name, contents=corrective)
            
#             if cor.map_array_set:
            cor.setTextColor(QtGui.QColor(150,255,150))
#             else:
#                 map.setTextColor(QtGui.QColor(255,0,0))
#                 shape_primary_item.setTextColor(QtGui.QColor(255,0,0))

            self.secondary_list.addItem(cor)





        self.secondary_list.setItemSelected(shape, True)
        

            
    def clear_secondary_list(self):
        self.secondary_list.clear()
        percent = QtWidgets.QListWidgetItem("Percent:")
        percent.setFlags(QtCore.Qt.NoItemFlags)
        self.secondary_list.addItem(percent)
        
        combinations = QtWidgets.QListWidgetItem("Combinations:")
        combinations.setFlags(QtCore.Qt.NoItemFlags)
        self.secondary_list.addItem(combinations)
        
        split_maps = QtWidgets.QListWidgetItem("Split Maps:")
        split_maps.setFlags(QtCore.Qt.NoItemFlags)
        self.secondary_list.addItem(split_maps)   
        
#         split_shapes = QtWidgets.QListWidgetItem("Split Shapes:")
#         split_shapes.setFlags(QtCore.Qt.NoItemFlags)
#         self.secondary_list.addItem(split_shapes)
# 
#         split_shapes = QtWidgets.QListWidgetItem("Split Combination Shapes:")
#         split_shapes.setFlags(QtCore.Qt.NoItemFlags)
#         self.secondary_list.addItem(split_shapes)
        
    def refresh(self):
        self.primary_list.clear()
        self.clear_secondary_list()
        
        if not self.network:
            return
        
        core_shapes = self.network.get_shapes(['core'])

        for name, shp in core_shapes.iteritems():
            shape = generic.primaryItem(name.rpartition("_")[0], contents=shp)
            self.primary_list.addItem(shape)
        
        self.primary_list.sortItems()  

            
            




class UpdateShapesDialog(QtWidgets.QDialog):
    
    def __init__(self, parent=generic.maya_main_window()):
        super(UpdateShapesDialog, self).__init__(parent)
        
        self.ITEMS = []
        self.SHAPE_DATA = []
        
        self.resize(700,600)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.main_layout)
        
        self.name_label = QtWidgets.QLabel("Name:")
        self.main_layout.addWidget(self.name_label)
        
        self.shape_list = QtWidgets.QTreeWidget()
        self.shape_list.setColumnCount(4)
        self.shape_list.setHeaderLabels([None, 'Mesh', "Type", "Shape name"])
        
        header = self.shape_list.header()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        header.setStretchLastSection(False)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
          
        self.main_layout.addWidget(self.shape_list)
        
        self.confirm_button = QtWidgets.QPushButton("Confirm")
        self.confirm_button.clicked.connect(self.confirm)
        self.main_layout.addWidget(self.confirm_button)

        self.populate_from_scene()
        
        self.exec_()    
    
    def confirm(self):
        
        for item in self.ITEMS:
            include = item.checkState(0) == QtCore.Qt.Checked
            
            if not include:
                continue
            
            mesh = item.text(1)
            shape_type = self.shape_list.itemWidget(item, 2).currentText().lower()
            shape_name = self.shape_list.itemWidget(item, 3).text()
            
            self.SHAPE_DATA.append({"mesh":mesh, "type":shape_type, "name":shape_name})
        
        self.accept()
        
        
    def populate_from_scene(self):
        selection = cmds.ls(sl=True)
         
        for obj in selection:
            type_by_name = names.info_from_name(obj)[0]
             
            item = QtWidgets.QTreeWidgetItem()
            item.setText(1, obj)
            if type_by_name == "invalid":
                item.setCheckState(0, QtCore.Qt.Unchecked)
            else:
                item.setCheckState(0, QtCore.Qt.Checked)
 
            type_combo = QtWidgets.QComboBox(self)
            type_combo.addItem("Core")
            type_combo.addItem("Inbetween")
            type_combo.addItem("Combo")
            type_combo.addItem("Invalid")
            type_combo.addItem("Neutral")
             
            type_combo.setCurrentIndex(["core", "inbetween", "combo", "invalid", "neutral"].index(type_by_name))
 
            name_line_edit = QtWidgets.QLineEdit(obj, self)
            
             
            self.shape_list.addTopLevelItem(item)
             
            self.shape_list.setItemWidget(item, 2, type_combo)
            self.shape_list.setItemWidget(item, 3, name_line_edit)
            self.ITEMS.append(item)
                