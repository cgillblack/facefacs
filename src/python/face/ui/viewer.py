import copy
import logging

LOG = logging.getLogger(__name__)
LOG.setLevel(40)

from PySide2 import QtCore, QtGui, QtWidgets
from maya import OpenMayaUI as omui
from maya import cmds
from functools import partial

from maya.app.general.mayaMixin import MayaQWidgetDockableMixin

import weakref

import maya.cmds as cmds
import maya.OpenMayaUI as omui
from shiboken2 import wrapInstance
import maya.OpenMayaUI as mui

from face.ui import generic, shape_viewer, map_viewer, interface_viewer, corrective_viewer
from face import io, core, processing
from face.utils import names
from fdev.utils import model, control, blendshape, painter, corrective, transform

reload(generic)
reload(shape_viewer)
reload(map_viewer)
reload(interface_viewer)
reload(corrective_viewer)

reload(io)
reload(core)
reload(processing)
reload(model)
reload(control)
reload(blendshape)
reload(painter)
reload(transform)
reload(corrective)





main_stylesheet = """
QTabWidget::pane { /* The tab widget frame */
    border-top: 0px solid #000000;
}

QTabWidget::tab-bar {
    left: 0px; /* move to the right by 5px */
    padding: 0px;

}

/* Style the tab using the tab sub-control. Note that it reads QTabBar _not_ QTabWidget */
QTabBar::tab {
    background: red;
    border: 0px solid #000000;
    border-bottom-color: #C2C7CB; /* same as the pane color */
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    min-width: 8ex;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
    margin-right: 2px;
}

QTabBar::tab:hover {
    color:white;
    background: blue;
}

QTabBar::tab:selected {
    color:white;
    background: #474f54;
}



QTabBar::tab:!selected {
    margin-top: 0px; /* make non-selected tabs look smaller */
    color:#9b9d9e;
    background: #2f3739;
}
QTabBar::tab:disabled {
    color:#111111;
    background: #333333;
}

QLineEdit{
    color:white;
    background: #181a1a;
    border: 0px solid #000000;
    border-radius: 0px;
    padding:0px;
    padding-left:3px;
    margin:0px;
}

QLineEdit#target_geometry_lineedit {
    color:white;
    background: #181a1a;
    border: 0px solid #000000;
    border-radius: 2px;
    padding:0px;
    padding-left:3px;
    margin:0px;
    height:20px;
}

QFrame{
    color:white;
    background: #474f54;
}

QTreeWidget{
    color:white;
    background: #2f3739;
    border: 0px solid #000000;
    border-radius: 4px;
    padding:0px;
    margin:0px;
    alternate-background-color: yellow;

}

QHeaderView{
    color:white;
    background: #3b4346;
    border: 0px solid #000000;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    padding:0px;
    margin:0px;
    border-bottom:2px solid #474f54;
}

QHeaderView::section{
    color:white;
    background: transparent;
    border: 0px solid #000000;
    padding:5px;
    margin:0px;
}


QListWidget{
    color:white;
    background: #2f3739;
    border: 0px solid #000000;
    border-radius: 4px;
    padding:5px;
    margin:0px;
}


QPushButton#add_shapes_button{
    color:white;
    background: transparent;
    border-image: url(C:/facefacs/resources/icons/add_to_list_icon.png);
    border: 0px solid #000000;
    border-radius: 4px;
    padding:5px;
    margin:5px;
    margin-left:0px;
    width:20px;
    height:20px;
}

QPushButton#add_controls_button{
    color:white;
    background: transparent;
    border-image: url(C:/facefacs/resources/icons/add_to_list_icon.png);
    border: 0px solid #000000;
    border-radius: 4px;
    padding:5px;
    margin:5px;
    margin-left:0px;
    width:20px;
    height:20px;
}

QPushButton#plus_button{
    color:white;
    background: transparent;
    border-image: url(C:/facefacs/resources/icons/plus_icon.png);
    border: 0px solid #000000;
    border-radius: 4px;
    padding:0px;
    margin:5px;
    margin-left:0px;
    width:15px;
    height:15px;
}

QPushButton#build_controls_button{
    color:white;
    background: #616c73;
    border: 0px solid #3b4346;
    border-radius:4;
    padding:2px;
    padding-left:3px;
    padding-right:7px;
    margin:0px;
}

QPushButton#build_blendshape_button{

    color:white;
    background: #616c73;
    border: 0px solid #3b4346;
    border-radius:4;
    padding:2px;
    padding-left:7px;
    padding-right:7px;
    margin:0px;
}

QPushButton#build_controls_button:hover{
    background: #3b4346;
}
QPushButton#build_blendshape_button:hover{
    background: #3b4346;
}

"""


class DockableWidget(MayaQWidgetDockableMixin, QtWidgets.QWidget):


    instances = list()
    CONTROL_NAME = 'faceFACS'

    def __init__(self, parent=None):

        super(DockableWidget, self).__init__(parent=parent)

        DockableWidget.delete_instances()
        self.__class__.instances.append(weakref.proxy(self))
        self.main_layout = QtWidgets.QVBoxLayout()
        self.button = Face(self)
        self.main_layout.addWidget(self.button)
        self.main_layout.setContentsMargins(0,0,0,0)
        self.setLayout(self.main_layout)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle(self.CONTROL_NAME)


    @staticmethod
    def delete_instances():
        print "deleting"
        for ins in DockableWidget.instances:
            try:
                ins.setParent(None)
                ins.deleteLater()
            except:
                pass
                DockableWidget.instances.remove(ins)
            del ins



class Face(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(Face, self).__init__(parent)

        self.selected = []

        self.main_layout = QtWidgets.QVBoxLayout()
        self.main_layout.setContentsMargins(0,0,0,0)
        self.main_layout.setSpacing(0)
        self.setLayout(self.main_layout)


        
        mainMenu = QtWidgets.QMenuBar()
        self.main_layout.addWidget(mainMenu)
        fileMenu = mainMenu.addMenu('File')

        new_button = QtWidgets.QAction('New', self)
        new_button.triggered.connect(self.new)
        fileMenu.addAction(new_button)
        

        open_button = QtWidgets.QAction('Open', self)
        open_button.triggered.connect(self.open)
        fileMenu.addAction(open_button)
        
        
        save_as_button = QtWidgets.QAction('Save As', self)
        save_as_button.triggered.connect(self.save_as)
        fileMenu.addAction(save_as_button)
        
        
        self.body_layout = QtWidgets.QVBoxLayout(self)
        self.body_layout.setContentsMargins(0,0,0,0)
        self.body_layout.setSpacing(0)
        self.main_layout.addLayout(self.body_layout)
        
        #Tab setup
        self.tabwidget = QtWidgets.QTabWidget()
        self.body_layout.addWidget(self.tabwidget)

        
        #Shapes section

        self.shapes_widget = QtWidgets.QFrame(self)
        self.shapes_layout = QtWidgets.QVBoxLayout(self)
        self.shapes_layout.setContentsMargins(5,5,5,5)
        self.shapes_layout.setSpacing(5)
        self.shapes_widget.setLayout(self.shapes_layout)

        self.tabwidget.addTab(self.shapes_widget, "Shape")

        self.shapes_header_layout = QtWidgets.QHBoxLayout(self)
        self.shapes_header_layout.setSpacing(0)
        self.shapes_layout.addLayout(self.shapes_header_layout)
        
        self.update_shapes_layout = QtWidgets.QVBoxLayout()
        self.update_shapes_layout.addStretch()
        self.shapes_header_layout.addLayout(self.update_shapes_layout)

        self.update_shapes_button = QtWidgets.QPushButton()
        self.update_shapes_button.setObjectName("add_shapes_button")
        self.update_shapes_layout.addWidget(self.update_shapes_button)
        self.update_shapes_button.clicked.connect(self.update_shapes)

        self.shapes_header_layout.addStretch()
        
        self.blendshape_build_data_layout = QtWidgets.QGridLayout(self)
        self.blendshape_build_data_layout.setColumnMinimumWidth (1, 200)
        self.blendshape_build_data_layout.setSpacing(2)
        self.shapes_header_layout.addLayout(self.blendshape_build_data_layout)
        
        self.blendshape_name_label = QtWidgets.QLabel("Blendshape Name:")
        self.blendshape_name_label.setObjectName("blendshape_name_label")
        self.blendshape_build_data_layout.addWidget(self.blendshape_name_label, 0,0)
        
        self.blendshape_name_lineedit = QtWidgets.QLineEdit("default_blendshape")
        self.blendshape_name_lineedit.setObjectName("target_geometry_lineedit")
        self.blendshape_build_data_layout.addWidget(self.blendshape_name_lineedit, 0,1)
        
        
        self.target_geometry_label = QtWidgets.QLabel("Blendshape Target Geometry:")
        self.target_geometry_label.setObjectName("target_geometry_label")
        self.blendshape_build_data_layout.addWidget(self.target_geometry_label, 1,0)
        
        self.blendshape_target_geometry_lineedit = TargetGeometryLineEdit("C_headA_MSH")
        self.blendshape_target_geometry_lineedit.setObjectName("target_geometry_lineedit")
        self.blendshape_build_data_layout.addWidget(self.blendshape_target_geometry_lineedit, 1,1)
        

        self.shapes_body_layout = QtWidgets.QHBoxLayout(self)
        self.shapes_layout.addLayout(self.shapes_body_layout)
        
        self.shapes_body_splitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        
        self.shapes_layout.addWidget(self.shapes_body_splitter)
        
        self.shape_viewer = shape_viewer.ShapeViewer()
        
        self.shape_viewer.add_update_shapes.connect(self.update_shapes)
        self.shape_viewer.map_added.connect(self.add_map)
        self.shape_viewer.corrective_added.connect(self.add_corrective)

        
        self.shape_viewer.shape_removed.connect(self.remove_shape)
        self.shape_viewer.map_removed.connect(self.remove_map_from_shape)
        
        self.shape_viewer.secondary_selection_changed.connect(self.selection_changed)
        
        self.shape_viewer.primary_double_clicked.connect(self.item_double_clicked)
        self.shape_viewer.secondary_double_clicked.connect(self.item_double_clicked)
        
        self.shapes_body_splitter.addWidget(self.shape_viewer)
        



        #Interface section
        
        self.interface_widget = QtWidgets.QFrame(self)
        self.interface_layout = QtWidgets.QVBoxLayout(self)
        self.interface_layout.setContentsMargins(5,5,5,5)
        self.interface_layout.setSpacing(5)
        self.interface_widget.setLayout(self.interface_layout)
        
        self.tabwidget.addTab(self.interface_widget, "Interface")        
        
        self.interface_header_layout = QtWidgets.QHBoxLayout(self)
        self.interface_layout.addLayout(self.interface_header_layout)

        self.update_controls_layout = QtWidgets.QVBoxLayout()
        self.update_controls_layout.addStretch()
        self.interface_header_layout.addLayout(self.update_controls_layout)

        self.update_controls_button = QtWidgets.QPushButton()
        self.update_controls_button.setObjectName("add_controls_button")
        self.update_controls_layout.addWidget(self.update_controls_button)
        self.update_controls_button.clicked.connect(self.update_controls)
        
        
        self.interface_header_layout.addStretch()
        
        self.interface_build_data_layout = QtWidgets.QGridLayout(self)
        self.interface_build_data_layout.setColumnMinimumWidth (1, 200)
        self.interface_build_data_layout.setSpacing(2)
        self.interface_header_layout.addLayout(self.interface_build_data_layout)
                
        
        self.target_blendshape_label = QtWidgets.QLabel("Target Blendshape:")
        self.target_blendshape_label.setObjectName("target_geometry_label")
        self.interface_build_data_layout.addWidget(self.target_blendshape_label, 0,0)
        
        self.target_blendshape_lineedit = TargetGeometryLineEdit("default_blendshape")
        self.target_blendshape_lineedit.setObjectName("target_geometry_lineedit")
        self.interface_build_data_layout.addWidget(self.target_blendshape_lineedit, 0,1)
        
        
        self.interface_target_geometry_label = QtWidgets.QLabel("Interface Target Geometry:")
        self.interface_target_geometry_label.setObjectName("target_geometry_label")
        self.interface_build_data_layout.addWidget(self.interface_target_geometry_label, 1,0)
        
        self.interface_target_geometry_lineedit = TargetGeometryLineEdit("C_headA_MSH")
        self.interface_target_geometry_lineedit.setObjectName("target_geometry_lineedit")
        self.interface_build_data_layout.addWidget(self.interface_target_geometry_lineedit, 1,1)
        
        self.interface_build_data_layout.setColumnStretch (1, 1)
        
        
        self.interface_body_layout = QtWidgets.QHBoxLayout(self)
        self.interface_layout.addLayout(self.interface_body_layout)

        self.interface_button_layout = QtWidgets.QVBoxLayout(self)
        self.interface_body_layout.addLayout(self.interface_button_layout)
        
        self.interface_button_layout.addStretch()
        self.interface_viewer = interface_viewer.InterfaceViewer()

        self.interface_viewer.control_removed.connect(self.remove_control)
        self.interface_viewer.add_update.connect(self.update_controls)
        self.interface_viewer.build_guides.connect(self.build_guides)

        self.interface_body_layout.addWidget(self.interface_viewer)


        #Map section
        self.map_widget = QtWidgets.QFrame(self)
        self.map_layout = QtWidgets.QVBoxLayout(self)
        self.map_layout.setContentsMargins(0,0,0,0)
        self.map_layout.setSpacing(0)
        self.map_widget.setLayout(self.map_layout)
        
        self.map_label = QtWidgets.QLabel("Maps")
        self.map_layout.addWidget(self.map_label)
        
        self.shapes_body_splitter.addWidget(self.map_widget)        

        self.map_body_layout = QtWidgets.QHBoxLayout(self)
        self.map_layout.addLayout(self.map_body_layout)

        self.map_button_layout = QtWidgets.QVBoxLayout(self)
        self.map_body_layout.addLayout(self.map_button_layout)

        self.generate_map_button = QtWidgets.QPushButton()
        self.generate_map_button.setObjectName("plus_button")
        self.map_button_layout.addWidget(self.generate_map_button)
        self.generate_map_button.clicked.connect(self.create_map)
        self.generate_map_button.setToolTip("Generate map")

        self.map_button_layout.addStretch()

        self.map_viewer = map_viewer.MapViewer()
        
        self.map_viewer.create_map.connect(self.create_map)
        self.map_viewer.build_painter.connect(self.build_painter_on_selected)
        self.map_viewer.update_painter.connect(self.update_map_from_painter)
        self.map_viewer.delete_painter.connect(self.delete_painter)
        self.map_viewer.map_removed.connect(self.remove_map)
        
        self.map_body_layout.addWidget(self.map_viewer)

        



        #Corrective section

        self.corrective_widget = QtWidgets.QFrame(self)
        self.corrective_layout = QtWidgets.QVBoxLayout(self)
        self.corrective_layout.setContentsMargins(0,0,0,0)
        self.corrective_layout.setSpacing(0)
        self.corrective_widget.setLayout(self.corrective_layout)

        self.shapes_body_splitter.addWidget(self.corrective_widget)        

        self.corrective_label = QtWidgets.QLabel("Correctives")
        self.corrective_layout.addWidget(self.corrective_label)

        self.corrective_body_layout = QtWidgets.QHBoxLayout(self)
        self.corrective_layout.addLayout(self.corrective_body_layout)

        self.corrective_button_layout = QtWidgets.QVBoxLayout(self)
        self.corrective_body_layout.addLayout(self.corrective_button_layout)
        
        self.update_correctives_button = QtWidgets.QPushButton()
        self.update_correctives_button.setObjectName("plus_button")

        self.corrective_button_layout.addWidget(self.update_correctives_button)
        self.update_correctives_button.clicked.connect(self.update_correctives)
        self.update_correctives_button.setToolTip("Add/Update correctives")

        self.corrective_button_layout.addStretch()
        self.corrective_viewer = corrective_viewer.CorrectiveViewer()
        
        self.corrective_viewer.add_update_correctives.connect(self.update_correctives)
        self.corrective_viewer.corrective_removed.connect(self.remove_corrective)

        self.corrective_body_layout.addWidget(self.corrective_viewer)
        

        #Deformer section

        self.deformer_widget = QtWidgets.QFrame(self)
        self.deformer_layout = QtWidgets.QVBoxLayout(self)
        self.deformer_widget.setLayout(self.deformer_layout)
        self.tabwidget.addTab(self.deformer_widget, "Deformer")
        
        self.tabwidget.setTabEnabled(2, False)
        
        self.footer_layout = QtWidgets.QHBoxLayout()
        self.footer_widget = QtWidgets.QFrame()
        self.footer_widget.setLayout(self.footer_layout)
        self.footer_layout.setSpacing(5)
        self.main_layout.addWidget(self.footer_widget)
        
        self.build_blendshape_button = QtWidgets.QPushButton(" Build Blendshape")
        self.build_blendshape_button.setIcon(QtGui.QIcon('C:/facefacs/resources/icons/face_icon.png'))
        self.build_blendshape_button.setIconSize(QtCore.QSize(28,28))
        
        self.build_blendshape_button.setObjectName("build_blendshape_button")
        self.build_blendshape_button.clicked.connect(self.build_blendshape)
        self.footer_layout.addWidget(self.build_blendshape_button)
        
        self.build_controls_button = QtWidgets.QPushButton("Build Controls")
        self.build_controls_button.setIcon(QtGui.QIcon('C:/facefacs/resources/icons/control_icon.png'))
        self.build_controls_button.setIconSize(QtCore.QSize(28,28))
        
        self.build_controls_button.setObjectName("build_controls_button")
        self.build_controls_button.clicked.connect(self.build_controls)
        self.footer_layout.addWidget(self.build_controls_button)
        
        self.footer_layout.addStretch()
        
        self.progress_bar = QtWidgets.QProgressBar()
        self.main_layout.addWidget(self.progress_bar)
        
        
        self.body_layout.setStretch(0,0)
        self.body_layout.setStretch(1,1)
        self.body_layout.setStretch(2,1)
        
        self.shapes_body_splitter.setStretchFactor(0, 1)
        self.shapes_body_splitter.setStretchFactor(1, 0)
        self.shapes_body_splitter.setStretchFactor(2, 0)
        self.shapes_body_splitter.setCollapsible(0,0)
        self.shapes_body_splitter.setCollapsible(1,0)
        self.shapes_body_splitter.setCollapsible(2,0)
#         self.shapes_layout.addStretch()

        self.new()
        
        self.refresh_viewers()
        
        self.setStyleSheet(main_stylesheet)
        
    def refresh_viewers(self):
        self.shape_viewer.refresh()
        self.interface_viewer.refresh()
        self.map_viewer.refresh()
        self.corrective_viewer.refresh()

    
    def remove_shape(self, shape):
        if isinstance(shape, core.Shape):
            inbetweens = [shp.name for shp in shape.inbetweens]
            combos = [shp.name for shp in self.network.get_combination_shapes([shape], common=False)]
            
            all_shapes = [shape.name]
            all_shapes.extend(combos)
            all_shapes.extend(inbetweens)
            
            warning_string = "This operation will remove:\n{}".format("\n".join(all_shapes))
            
            
            
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            
            msg.setText("Are you sure?")
            msg.setWindowTitle("Removing shapes")
            msg.setInformativeText("Once shapes are removed they cannot be recovered.\n\n{}".format(warning_string))
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
             
            retval = msg.exec_()
            
            if retval == QtWidgets.QMessageBox.Ok:
                self.network.remove_shape(shape)
            elif retval == QtWidgets.QMessageBox.Cancel:
                return
        
            self.shape_viewer.refresh()



    def remove_map_from_shape(self, shape, map):
        if isinstance(shape, core.Shape) and isinstance(map, core.Map):
            
            if map in shape.split_maps:
                shape.split_maps.remove(map)
                self.shape_viewer.refresh()

    
    def remove_control(self, control):
        if isinstance(control, core.Control):
            warning_string = "This operation will remove:\n{}".format(control.name)

            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            
            msg.setText("Are you sure?")
            msg.setWindowTitle("Removing Control")
            msg.setInformativeText("Once a control is removed it cannot be recovered.\n\n{}".format(warning_string))
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
             
            retval = msg.exec_()
            
            if retval == QtWidgets.QMessageBox.Ok:
                self.network.remove_control(control)
            elif retval == QtWidgets.QMessageBox.Cancel:
                return
        
            self.interface_viewer.refresh()
    
    def remove_map(self, map):
        if isinstance(map, core.Map):
            warning_string = "This operation will remove:\n{}".format(map.name)

            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            
            msg.setText("Are you sure?")
            msg.setWindowTitle("Removing Map")
            msg.setInformativeText("Once a map is removed it cannot be recovered.\n\n{}".format(warning_string))
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
             
            retval = msg.exec_()
            
            if retval == QtWidgets.QMessageBox.Ok:
                self.network.remove_map(map)
            elif retval == QtWidgets.QMessageBox.Cancel:
                return
        
            self.map_viewer.clear()
            self.map_viewer.populate()
            
            self.shape_viewer.refresh()
            

    
    
    def remove_corrective(self, corrective):
        if isinstance(corrective, core.Corrective):
            warning_string = "This operation will remove:\n{}".format(corrective.name)

            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            
            msg.setText("Are you sure?")
            msg.setWindowTitle("Removing Corrective")
            msg.setInformativeText("Once a corrective is removed it cannot be recovered.\n\n{}".format(warning_string))
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
             
            retval = msg.exec_()
            
            if retval == QtWidgets.QMessageBox.Ok:
                self.network.remove_corrective(corrective)
            elif retval == QtWidgets.QMessageBox.Cancel:
                return
        
            self.corrective_viewer.refresh()
            self.shape_viewer.refresh()
    
    
    def selection_changed(self, *args):
        if not args:
            return 
        
        selected = args[0]
        
#         if len(selected) == 1:
#             if isinstance(selected[0], core.Shape):
#                 print "Shape selected", selected[0].name
#             elif isinstance(selected[0], core.Map):
#                 print "Map selected"
#         elif len(selected) > 1:
#             print "Multiple items selected", selected
#         elif len(selected) == 0:    
#             print "Nothing selected"

        self.selected = selected
    
    def item_double_clicked(self, *args):
        if not args:
            return 
        
        if isinstance(args[0], core.Shape):
            self.apply(shape=args[0])
#         elif isinstance(args[0], core.Map):
#             print "Map double clicked"

    def add_map(self, map, shape):
        shape.split_maps.append(map)
        
        self.shape_viewer.refresh()
        self.map_viewer.refresh()

    def add_corrective(self, corrective, shape):
        shape.correctives.append(corrective)
        
        self.shape_viewer.refresh()
        self.corrective_viewer.refresh()
        
    
    def update_shapes(self):
        update_dialog = shape_viewer.UpdateShapesDialog()
        shape_data = copy.deepcopy(update_dialog.SHAPE_DATA)
        update_dialog.deleteLater()
        
        self.progress_bar.setValue(0)
        
        for neutral_check in shape_data:
            if neutral_check["type"] == "neutral": #Check if the neutral is being added/updated in this batch of shapes.
                neutral = self.network.shapes['neutral_100']
                neutral.vtx_array = model.get_vtx_array_from_maya_node(
                    neutral,
                    neutral_check["mesh"],
                    subtract_neutral=False,
                    method=0,
                    network=self.network)
        
        self.progress_bar.setValue(5)
        num_shapes = float(len(shape_data))
        
        for i, core_shape_info in enumerate(shape_data):
            mesh = core_shape_info["mesh"]
            name = core_shape_info["name"]
            shape_type = core_shape_info["type"]
            
            if shape_type == "core":
                if name in self.network.shapes.keys():
                    #Shape with that name already exists
                    shape = self.network.shapes[name]
                else:
                    shape = self.network.add_shape(name)
                
                shape.vtx_array = model.get_vtx_array_from_maya_node(
                                            shape,
                                            mesh,
                                            subtract_neutral=True,
                                            method=0,
                                            network=self.network
                                            )
            if num_shapes > 1:
                self.progress_bar.setValue(((i/(num_shapes-1))*45)+5)
        
        
        
        
        self.progress_bar.setValue(66)
        
        for combo_shape_info in shape_data:
            mesh = combo_shape_info["mesh"]
            name = combo_shape_info["name"]
            shape_type = combo_shape_info["type"]

            if shape_type == "combo":

                if name in self.network.shapes.keys():
                    #Shape with that name already exists
                    combo_shape = self.network.shapes[name]
                else:
                    name_info = names.info_from_name(name)
                    combination_of_names = name_info[2]
                    combination_of_shapes = [self.network.shapes[nm] for nm in combination_of_names]
                    
                    combo_shape = self.network.add_combination_shape(combination_of_shapes)
                    
                combo_shape.vtx_array = model.get_vtx_array_from_maya_node(
                                        combo_shape,
                                        mesh,
                                        subtract_neutral=True,
                                        method=0,
                                        network=self.network
                                        )
            
            if num_shapes > 1:
                self.progress_bar.setValue(((i/(num_shapes-1))*45.0)+50.0)


        for inbetween_shape_info in shape_data:
            mesh = inbetween_shape_info["mesh"]
            name = inbetween_shape_info["name"]
            shape_type = inbetween_shape_info["type"]

            if shape_type == "inbetween":
                if name in self.network.shapes.keys():
                    #Shape with that name already exists
                    inbetween_shape = self.network.shapes[name]
                else:
                    name_info = names.info_from_name(name)
                    if not name_info[3] in self.network.shapes.keys():
                        continue
                    
                    inbetween_for = self.network.shapes[name_info[3]]
                    tokens = name_info[1]
                    percentage = int(tokens[-1])
                    inbetween_shape = self.network.add_inbetween_shape(inbetween_for, percentage)
                    
                inbetween_shape.vtx_array = model.get_vtx_array_from_maya_node(
                                        inbetween_shape,
                                        mesh,
                                        subtract_neutral=True,
                                        method=0,
                                        network=self.network,
                                        inbetween=True
                                        )

        self.shape_viewer.refresh()
        self.progress_bar.setValue(100)



    def update_controls(self):
        update_dialog = interface_viewer.UpdateControlsDialog()
        control_data = copy.deepcopy(update_dialog.CONTROL_DATA)
        update_dialog.deleteLater()
        
        self.progress_bar.setValue(0)
        
        for i, control_info in enumerate(control_data):
            node = control_info["transform"]
            name = control_info["name"]
            
            if name in self.network.controls.keys():
                #Shape with that name already exists
                ctrl = self.network.controls[name]
            else:
                ctrl = self.network.add_control(name)
                
            transform_matrix = transform.get_transform(node, world_space=True, normalize=False)
            ctrl.transform_matrix = transform_matrix
                
#             if num_shapes > 1:
#                 self.progress_bar.setValue(((i/(num_shapes-1))*45)+5)
            
#         self.progress_bar.setValue(66)
        
        self.interface_viewer.refresh()
        self.progress_bar.setValue(100)



    def update_correctives(self):
        selection = cmds.ls(sl=True)
        if len(selection) < 1:
            return
        
        mesh = selection[0]
        update_dialog = corrective_viewer.UpdateCorrectivesDialog()
        corrective_data = copy.deepcopy(update_dialog.CORRECTIVE_DATA)
        update_dialog.deleteLater()
        
        if not corrective_data:
            return
        
        self.progress_bar.setValue(0)
        
        skincluster_data = corrective.get_skincluster_data(mesh)
        
        for i, corrective_info in enumerate(corrective_data):
            node = corrective_info["influence"]
            name = corrective_info["name"]
            off_frame = corrective_info["off_frame"]
            on_frame = corrective_info["on_frame"]
            
            
            long_name = cmds.ls(node, long=True)[0]
            if not long_name in skincluster_data.keys():
                LOG.error("update_correctives: '%s' not found in ng influences", long_name)
                continue
            
            if name in self.network.correctives.keys():
                #Corrective with that name already exists
                cor = self.network.correctives[name]
            else:
                cor = self.network.add_corrective(name)
            
            
            cmds.currentTime(off_frame)
            start_matrix = transform.get_transform(node, world_space=True, normalize=False)
            cor.start_matrix = start_matrix

            cmds.currentTime(on_frame)
            end_matrix = transform.get_transform(node, world_space=True, normalize=False)
            cor.end_matrix = end_matrix
            
            cor.weight_array = skincluster_data[long_name]
            
                
        
        self.corrective_viewer.refresh()
        self.shape_viewer.refresh()
        self.progress_bar.setValue(100)



    def create_map(self):
        create_map_dialog = map_viewer.CreateMapDialog()
        map_data = copy.deepcopy(create_map_dialog.MAP_DATA)
        create_map_dialog.deleteLater()
        
        self.progress_bar.setValue(0)
        
        name = map_data["name"]
        axis = map_data["axis"]
        prefixes = map_data["prefixes"]
        width = map_data["width"]
        offset = map_data["offset"]
        
        if name in self.network.controls.keys():
            #Shape with that name already exists
            map = self.network.map[name]
        else:
            map = self.network.add_map(name)
        
        map.prefixes = prefixes
        map.map_array = processing.generate_soft_split_map(self.network.shapes["neutral_100"].vtx_array, width=width, axis=axis)
            
        self.map_viewer.refresh()
        self.progress_bar.setValue(100)

    def build_painter_on_selected(self, map):
        selection = cmds.ls(sl=True)[0]
        ptr = painter.Painter(selection, map, self.network)
        ptr.build()
    
    def update_map_from_painter(self, ptr):
        ptr.update_map()

    def delete_painter(self, ptr):
        ptr.delete_painter()
    
    def new(self):
        self.network = core.Network()
        
        self.shape_viewer.network = self.network
        self.shape_viewer.refresh()
        
        self.interface_viewer.network = self.network
        self.interface_viewer.refresh()
        
        self.map_viewer.network = self.network
        self.map_viewer.clear()

        self.corrective_viewer.network = self.network
        self.corrective_viewer.clear()        
    
    def open(self):
        
        file_dialog = QtWidgets.QFileDialog()
        file_dialog.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        # the name filters must be a list
        file_dialog.setNameFilters(["json (*.json)", "pickle (*.p)", "messagePack (*.msgpack)", "all files (*.*)"])
        file_dialog.selectNameFilter("pickle (*.p)")
        # show the dialog
        result = file_dialog.exec_()
        
        if not result:
            return
        
        
        path = file_dialog.selectedFiles()[0]
        part_path, _, ext = path.rpartition('.')

        if ext in ['json']:
            LOG.info("open: Opened json")
        elif ext in ['p']:
            LOG.info("open: Opened pickle")
        elif ext in ['msgpack']:
            LOG.info("open: Opened messagePack")
        else:
            LOG.warning("%s is not a valid path", path)
            return
            
        self.network = io.open_network(path)  
        
        self.shape_viewer.network = self.network
        self.shape_viewer.refresh()
        
        self.interface_viewer.network = self.network
        self.interface_viewer.refresh()
        
        self.map_viewer.network = self.network
        self.map_viewer.clear()
        self.map_viewer.populate()

        self.corrective_viewer.network = self.network
        self.corrective_viewer.clear()        
        self.corrective_viewer.populate()        

    def save_as(self):
        file_dialog = QtWidgets.QFileDialog()
        file_dialog.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        filters = ["json (*.json)", "pickle (*.p)", "messagePack (*.msgpack)", "all files (*.*)"]
        path = file_dialog.getSaveFileName(self, "Save F:xile",
                                       "",
                                       ";;".join(filters),
                                       filters[-1])[0]
        part_path, _, ext = path.rpartition('.')

        if ext in ['json']:
            LOG.info("Save to json")
        elif ext in ['p']:
            LOG.info("Save to pickle")
        elif ext in ['msgpack']:
            LOG.info("Save to messagePack")
        else:
            LOG.warning("%s is not a valid path", path)
            return
        
        io.save_network(path, self.network)
                
    def apply(self, shape=None):
        
        if shape == None:
            if len(self.selected) > 1:
                LOG.error("Apply: Too many items selected")
                return
            
            shape = self.selected[0]
            
        elif not isinstance(shape, core.Shape):
            LOG.error("Apply: Given shape is of type '%s', should be 'Shape'", type(value))
            return
        
        selection = cmds.ls(sl=True, type="transform")
        if selection:
            selection = selection[0]
        
        if shape == self.network.shapes["neutral_100"]:
            model.apply_shape_to_maya_node(shape, selection, add_neutral=False, world_space=True)
        else:
            model.apply_shape_to_maya_node(shape, selection, add_neutral=True, world_space=True)
    
    
    def build_blendshape(self):
        
        reload(blendshape)
        
        target_geometry = self.blendshape_target_geometry_lineedit.text()
        if not target_geometry:
            LOG.warning("No target geometry specified")
            return
        
        blendshape_name = self.blendshape_name_lineedit.text()
        
        if not blendshape_name:
            blendshape_name = target_geometry + "_blendshape"
        
        #Split shapes
        split_network = self.network.split_shapes()
        
        split_network.apply_correctives()
        #Making blendshape deformer
        bs_deformer = blendshape.create_blendshape_from_network(split_network, target_geometry, name=blendshape_name)
        #Connect Inbetweens
        blendshape.create_inbetween_drivers(split_network, bs_deformer)
        #Connect Combos
        blendshape.create_combo_drivers(split_network, bs_deformer)
        
    def build_guides(self):
        control.create_interface_from_network(self.network, name=self.network.name, mesh=None, as_guides=True)

    def build_controls(self):

        reload(control)

        target_geometry = self.interface_target_geometry_lineedit.text()
        if not target_geometry:
            LOG.warning("No target geometry specified")
            return
        
        blendshape_name = self.target_blendshape_lineedit.text()
        
        if not blendshape_name:
            blendshape_name = target_geometry + "_blendshape"
        
        root, connection_list = control.create_interface_from_network(self.network, name=self.network.name, mesh=target_geometry, as_guides=False)
        
        control.connect_interface(connection_list, blendshape=blendshape_name)


class TargetGeometryLineEdit(QtWidgets.QLineEdit):
    def __init__(self, *args, **kwargs):
        super(TargetGeometryLineEdit, self).__init__(*args, **kwargs)

        #Right click menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.primary_context_menu)
    
    def primary_context_menu(self, QPos): 
        self.listMenu= QtWidgets.QMenu()
        from_selection = self.listMenu.addAction("Get target from selection")
        from_selection.triggered.connect(self.from_selection)
        
        
        
        parentPosition = self.mapToGlobal(QtCore.QPoint(0, 0))        
        self.listMenu.move(parentPosition + QPos)
        self.listMenu.show() 
    
    def from_selection(self):
        selection = cmds.ls(sl=True)
        if len(selection) > 0:
            self.setText(selection[0])




def DockableWidgetUIScript(restore=False):

    global customMixinWindow

    if restore == True:
        restoredControl = mui.MQtUtil.getCurrentParent()

    customMixinWindow = DockableWidget()
    if customMixinWindow is None:
        #customMixinWindow = DockableWidget()
        customMixinWindow.setObjectName('faceFACS')

    if restore == True:
        mixinPtr = mui.MQtUtil.findControl(customMixinWindow.objectName())
        mui.MQtUtil.addWidgetToMayaLayout(long(mixinPtr), long(restoredControl))

    else:
        try:
            
            cmds.workspaceControl('faceFACSWorkspaceControl', e=True, close=True)
            cmds.deleteUI('faceFACSWorkspaceControl')
        except Exception as inst:
            pass
        customMixinWindow.show(dockable=True, restore=True, height=400, width=400, uiScript='import dockWin; dockWin.DockableWidgetUIScript(restore=True)')

                