"""Module for name processing and analysis.

:author: Chris Gill

"""


def camelise(name):
    """Remove underscores and recombine in camelcase.

    Untested!

    Args:
        name (str): Name to edit.
    """
    
    if not name:
        print 'No text supplied'
        return
        
    if '_' in name:
        split = name.split('_')
        new_name = ''
        for i,each in enumerate(split):
            
            if i == 0:
                if len(each) > 1:
                    new_name = '{}{}{}'.format(new_name, each[0].lower(), each[1:])
                else:
                    new_name = '{}{}'.format(new_name, each[0].lower())
            else:
                if len(each) > 1:
                    new_name = '{}{}{}'.format(new_name, each[0].upper(), each[1:])
                else:
                    new_name = '{}{}'.format(new_name, each[0].upper())
    else:
        new_name = name[0].lower() + name[1:]
    
    return new_name


def info_from_name(name):
    """Extract shape info from mesh name.

    Args:
        name (str): Name to analyse.
    
    Returns:
        list: [string of shape type,
            the tokens in the split name,
            if a combo then what the combo consists of,
            if and inbetween which core shape it corresponds to]
    """
    
    type = "invalid"
    combination_of = []
    inbetween_for = None
    
    tokens = name.split("_")
    if len(tokens) == 2:
        #Core or Inbetween
        if not tokens[-1].isdigit():
            type = "invalid" #Name does not end in a number
        else:
            if int(tokens[-1]) > 100:
                type = "invalid"
            elif int(tokens[-1]) == 100:
                if name == "neutral_100":
                    type = "neutral"
                else:
                    type = "core"
            else:
                type = "inbetween"
                inbetween_for = "{}_100".format(tokens[0])
                
            
    elif len(tokens) > 2:
        # Combo
        if len(tokens)%2 == 0:
            type = "invalid" #Odd numbered tokens are invalid
        else:
            for pair in [tokens[:-1][i:i+2] for i in range(0, len(tokens[:-1]), 2)]:
                combination_of.append("_".join(pair))
                combination_of.sort()
                
#             if int(tokens[-1]) > 100:
#                 type = "invalid"
            if int(tokens[-1]) == 100:
                type = "combo"
            else:
                type = "invalid"
                
#             else:
#                 type = "inbetween"
# #                 inbetween_for = "{}_100".format("_".join(combination_of))
                

    
    return [type, tokens, combination_of, inbetween_for]
        
        
